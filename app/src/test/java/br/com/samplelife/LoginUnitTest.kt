package br.com.samplelife

import br.com.samplelife.features.login.presenter.LoginContract
import br.com.samplelife.features.login.presenter.LoginPresenter
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito

class LoginUnitTest {

    @Test
    fun validate_withEmptyQuery_callsShowQueryRequiredMessage() {
        val presenter = LoginPresenter()
        val view = Mockito.mock(LoginContract.View::class.java)
        presenter.attachView(view)
        Assert.assertFalse(presenter.validateLogin("", ""));
    }

    @Test
    fun validate_loginQuery_callsShowQueryRequiredMessage() {
        val presenter = LoginPresenter()
        val view = Mockito.mock(LoginContract.View::class.java)
        presenter.attachView(view)
        Assert.assertTrue(presenter.validateLogin("12345678901", "123"));
    }
}