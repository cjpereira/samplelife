package br.com.samplelife.application

import android.app.Application
import br.com.samplelife.BuildConfig
import br.com.samplelife.model.Login
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.Polling
import java.net.URISyntaxException
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.*
import java.security.SecureRandom


class UVApplication : Application() {

    var login: Login? = null
    var socket: Socket? = null
        private set

    fun startSocket(userId: String) {
        try {

            if(socket!=null){
                socket?.disconnect()
            }

            val sc = SSLContext.getInstance("TLS")
            sc.init(null, trustAllCerts, SecureRandom())
            IO.setDefaultSSLContext(sc)
            HttpsURLConnection.setDefaultHostnameVerifier(RelaxedHostNameVerifier())

            val opts = IO.Options()
            opts.secure = true
            opts.sslContext = sc
            opts.transports = arrayOf(Polling.NAME)

            socket = IO.socket("${BuildConfig.SOCKET_URL}?userId=$userId", opts)

        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
    }

    private var trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {

        override fun getAcceptedIssuers(): Array<X509Certificate> {
            return arrayOf()
        }

        @Throws(CertificateException::class)
        override fun checkClientTrusted(
            chain: Array<X509Certificate>,
            authType: String
        ) {
        }

        @Throws(CertificateException::class)
        override fun checkServerTrusted(
            chain: Array<X509Certificate>,
            authType: String
        ) {
        }
    })

    class RelaxedHostNameVerifier : HostnameVerifier {
        override fun verify(hostname: String, session: SSLSession): Boolean {
            return true
        }
    }
}