package br.com.samplelife.api

import br.com.samplelife.utils.Constants
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit


/**
 * Created by carlosjean on 06/04/19.
 */
object ApiManager {

    fun <T> createRetrofit(clazz: Class<T>): T {

        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val okHttpClient = OkHttpClient.Builder().apply {
            connectTimeout(Constants.TIME_OUT, TimeUnit.SECONDS)
            readTimeout(Constants.TIME_OUT, TimeUnit.SECONDS)
            addInterceptor(interceptor)
            addInterceptor{ chain ->

                var request = chain.request()

                //Build new request
                val builder = request.newBuilder()

                request = builder.build() //overwrite old request

                try {
                    chain.proceed(request) //perform request, here original request will be executed
                } catch (e: ConnectException) {
                    throw ApiException(Constants.MSG_CONNECTION_NOT_AVAILABLE)
                }catch (e: UnknownHostException){
                    throw ApiException(Constants.MSG_CONNECTION_NOT_AVAILABLE)
                } catch (e: Exception) {
                    throw ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE)
                }
            }
       }

        val annotations = clazz.annotations

        var url = ""

        for (a in annotations) {
            if (a is URLBase) {
                url = a.value
                break
            }
        }

        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .client(okHttpClient.build())
            .build()

        return retrofit.create(clazz)
    }

}