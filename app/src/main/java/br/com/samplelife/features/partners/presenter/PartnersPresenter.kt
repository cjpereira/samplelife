package br.com.samplelife.features.partners.presenter

import androidx.lifecycle.OnLifecycleEvent
import br.com.samplelife.base.BaseMvpPresenterImpl
import br.com.samplelife.features.partners.service.PartnersService
import br.com.samplelife.model.Generic
import br.com.samplelife.model.Partner
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.BehaviorSubject

open class PartnersPresenter: BaseMvpPresenterImpl<PartnersContract.View>(),
    PartnersContract.Presenter {

    override fun fetchPartners(token: String) {
        var partnersObservable: rx.Observable<ArrayList<Partner>> = PartnersService().fetchPartners(token, null, null)
        var stateObservable : rx.Observable<ArrayList<Generic>> = PartnersService().fetchState(token)
        var categoryObservable: rx.Observable<ArrayList<Generic>> = PartnersService().fetchCategory(token)

        BehaviorSubject.create<OnLifecycleEvent>()
        Observable.zip(partnersObservable, stateObservable,categoryObservable)
        {partners: ArrayList<Partner>, states: ArrayList<Generic>, categories:ArrayList<Generic> ->
            mView?.updateStates(states)
            mView?.updateCategories(categories)
            partners
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({ partners ->
                mView?.success(partners)
            },
            { throwable ->
                mView?.showMessage(throwable.message)
            })

    }

    override fun fetchPartners(token: String, state: String?, category: String?) {

        PartnersService().fetchPartners(token, state, category)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ partners  ->
                mView?.success(partners)
            },
            { throwable ->
                mView?.showMessage(throwable.message)
            })
    }

}