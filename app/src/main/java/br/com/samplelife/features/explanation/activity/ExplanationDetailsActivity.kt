package br.com.samplelife.features.explanation.activity

import android.content.Context
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import br.com.samplelife.R
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.home.presenter.HomeDetailsContract
import br.com.samplelife.features.home.presenter.HomeDetailsPresenter


class ExplanationDetailsActivity : BaseActivity<HomeDetailsContract.View, HomeDetailsContract.Presenter>(),
    HomeDetailsContract.View {

    lateinit var data : HashMap<String, Any>

    override var mPresenter: HomeDetailsContract.Presenter = HomeDetailsPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.explanation_details)

        data = intent.getSerializableExtra("data") as HashMap<String, Any>

        var background: ImageView = findViewById(R.id.background)
        background.setImageResource(data.get("background") as Int)


        var primaryColor = false

        var title: TextView = findViewById(R.id.txtDescription)
        title.setText((data.get("title") as String).replace(" ", "\n"))

        if(title.text.toString().toLowerCase().contains("parceiros")) {
            primaryColor = true
            title.setTextColor(ActivityCompat.getColor(this, R.color.colorPrimary))
        }

        var content: LinearLayout = findViewById(R.id.contentMain)

        val row: ArrayList<HashMap<String, Any>> = data.get("item") as ArrayList<HashMap<String, Any>>

        for (item in row) {
            val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            var row: View = inflater.inflate(R.layout.details_item, null)
            var icon: ImageView = row.findViewById(R.id.rowIcon)
            icon.setImageResource(item.get("icon") as Int)

            var rowTitle: TextView = row.findViewById(R.id.rowTitle)
            rowTitle.setText(item.get("title") as String)


            var rowDetails: TextView = row.findViewById(R.id.rowDetails)
            rowDetails.setText(item.get("text") as String)

            if (primaryColor){
                icon.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
                rowTitle.setTextColor(ActivityCompat.getColor(this, R.color.colorPrimary))
                rowDetails.setTextColor(ActivityCompat.getColor(this, R.color.colorPrimary))
            }


            content.addView(row)
        }

        var back: ImageView = findViewById(R.id.backButton)
        back.setOnClickListener{
          finish()
        }
    }

    override fun success(result: Any) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}