package br.com.samplelife.features.home.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter

class HomePagerViewAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager){

    private val mFragmentList = ArrayList<Fragment>()

    override fun getItem(position: Int): Fragment {
        return mFragmentList.get(position)
    }

    override fun getCount(): Int {
        return 4
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    fun addFragment(fragment: Fragment){
        mFragmentList.add(fragment)
    }

    fun getFragment(index: Int): Fragment {
        return mFragmentList.get(index)
    }
}