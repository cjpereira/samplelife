package br.com.samplelife.features.login.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.webkit.WebStorage
import br.com.samplelife.R
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.login.presenter.LoginContract
import br.com.samplelife.features.login.presenter.LoginPresenter
import br.com.samplelife.model.Login
import br.com.samplelife.preference.PreferenceHelper
import br.com.samplelife.utils.Constants
import kotlinx.android.synthetic.main.login_activity.*


class LoginActivity : BaseActivity<LoginContract.View, LoginContract.Presenter>(),
    LoginContract.View {

    override var mPresenter: LoginContract.Presenter = LoginPresenter()
    private val REQUEST_REQUESTPASSWORD_ITEM = 912
    private val REQUEST_CHANGEPASSWORD_ITEM = 912

    override fun onBackPressed() {
        finishAndRemoveTask()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        inputLayoutUser.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                inputLayoutUser?.isErrorEnabled = false
            }
            override fun afterTextChanged(editable: Editable) {}
        })

        inputLayoutPassword.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
              inputLayoutPassword?.isErrorEnabled = false
            }
            override fun afterTextChanged(editable: Editable) {}
        })

        btnLogin.setOnClickListener {
            doLogin()
        }

//        backButton.setOnClickListener{
//            finish()
//        }

        btnForgotPassword.setOnClickListener{
            val intent = Intent(this@LoginActivity, RecoverPasswordActivity::class.java)
            startActivity(intent)
            inputLayoutPassword.editText?.setText(resources.getString(R.string.empty))
        }

        val window = window
        val winParams = window.attributes
        winParams.flags = winParams.flags and WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS.inv()
        window.attributes = winParams
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

    }

    private fun doLogin(){
        loading()
        mPresenter.doLogin(inputLayoutUser?.editText?.text.toString(),inputLayoutPassword?.editText?.text.toString())
    }

    override fun success(result: Any) {

        hideLoading()
        var login: Login = result as Login

        if(login.user?.needChangePassword!!){

            val intent = Intent(this@LoginActivity, ChangePasswordActivity::class.java)
            intent.putExtra(Constants.PARAM_LOGIN, login)
            startActivityForResult(intent, REQUEST_CHANGEPASSWORD_ITEM)

        } else{

            val resultIntent = Intent()
            resultIntent.putExtra(Constants.PARAM_LOGIN, login)
            setResult(Activity.RESULT_OK, resultIntent)

            var strLogin = PreferenceHelper(this).getValueString(Constants.PARAM_CPF)
            if(!strLogin.equals(inputLayoutUser?.editText?.text.toString(), true)){
                //Limpar histórico usuário diferente
                WebStorage.getInstance().deleteAllData()
            }

            PreferenceHelper(this).save(Constants.PARAM_CPF, inputLayoutUser?.editText?.text.toString())
            PreferenceHelper(this).save(Constants.PARAM_PASSWORD, inputLayoutPassword?.editText?.text.toString())
            finish()
        }
    }

    override fun showErrorLogin(message: Int) {
        hideLoading()
        inputLayoutUser?.error = resources.getString(message)
    }

    override fun showErrorPassword(message: Int) {
        hideLoading()
        inputLayoutPassword?.error = resources.getString(message)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CHANGEPASSWORD_ITEM && resultCode == Activity.RESULT_OK) {
            var login = data?.getSerializableExtra(Constants.PARAM_LOGIN) as Login

            val resultIntent = Intent()
            resultIntent.putExtra(Constants.PARAM_LOGIN, login)
            setResult(Activity.RESULT_OK, resultIntent)

            var strLogin = PreferenceHelper(this).getValueString(Constants.PARAM_CPF)
            if(!strLogin.equals(inputLayoutUser?.editText?.text.toString(), true)){
                //Limpar histórico usuário diferente
                WebStorage.getInstance().deleteAllData()
            }

            PreferenceHelper(this).save(Constants.PARAM_CPF, inputLayoutUser?.editText?.text.toString())
            PreferenceHelper(this).save(Constants.PARAM_PASSWORD, inputLayoutPassword?.editText?.text.toString())
            finish()
        }
    }

}