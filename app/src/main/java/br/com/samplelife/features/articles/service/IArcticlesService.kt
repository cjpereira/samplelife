package br.com.samplelife.features.articles.service

import br.com.samplelife.BuildConfig
import br.com.samplelife.api.URLBase
import br.com.samplelife.payload.ArticlePayload
import br.com.samplelife.payload.GenericPayload
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

@URLBase(BuildConfig.BASE_URL)
interface IArcticlesService {

    @GET("api/publicacao/listar")
    fun fetchNews(@Query("categorias") category:String?): Call<ArrayList<ArticlePayload>>

    @GET("api/categoria-publicacao/listar")
    fun fetchCategory() : Call<ArrayList<GenericPayload>>

    @GET("api/acaoColaborador/infoVida/{idColaborador}")
    fun registerAccessInfoVida(@Path("idColaborador") id: String?): Call<Void>
}