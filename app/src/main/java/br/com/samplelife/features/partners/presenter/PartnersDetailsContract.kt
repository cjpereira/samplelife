package br.com.samplelife.features.partners.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView

object PartnersDetailsContract {

    interface View : BaseMvpView

    interface Presenter : BaseMvpPresenter<View>
}