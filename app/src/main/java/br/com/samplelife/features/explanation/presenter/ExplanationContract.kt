package br.com.samplelife.features.explanation.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView

object ExplanationContract {

    interface View : BaseMvpView

    interface Presenter : BaseMvpPresenter<View>

}
