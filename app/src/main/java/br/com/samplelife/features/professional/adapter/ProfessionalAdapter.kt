package br.com.samplelife.features.professional.adapter


import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.samplelife.R
import br.com.samplelife.model.Professional
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.article_item.view.*
import rx.subjects.PublishSubject

class ProfessionalAdapter(
    var professionalList: ArrayList<Professional>,
    private val context: Context): RecyclerView.Adapter<ProfessionalAdapter.ViewHolder>() {

    private val onClickSubject = PublishSubject.create<Professional>()

    fun getItemClickSignal(): rx.Observable<Professional> {
        return onClickSubject.asObservable()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.professional_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return professionalList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val professional = professionalList[position]
        holder.itemView.cardView.setOnClickListener { v -> onClickSubject.onNext(professional) }
        holder.bindView(professional)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(professional: Professional) {
            val name = itemView.txtDescription
            val specialty = itemView.txtHealthSpecialty
            val city = itemView.txtHour
            val imgPartner = itemView.imgPartner

            name.text = professional.name
            specialty.text = professional.specialty?.name
            city.text = "${professional.city?.name} - ${professional.city?.state?.code}"

            if(professional.linkImage!=null && !professional?.linkImage?.isEmpty()!!){

                Picasso.get()
                    .load(professional.linkImage)
                    .placeholder(R.drawable.rectangle_3)
                    .error(R.drawable.rectangle_3)
                    .into(imgPartner)
            }

        }
    }

}