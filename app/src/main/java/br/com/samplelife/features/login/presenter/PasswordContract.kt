package br.com.samplelife.features.login.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView

object PasswordContract {

    interface View : BaseMvpView {
        fun showErrorUser(message: Int)
        fun showErrorPassword(message: Int)
    }

    interface Presenter : BaseMvpPresenter<View> {
        fun doRecoveryPassword(user: String)
        fun doChangePassword(user: String, password: String)
    }

}
