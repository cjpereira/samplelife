package br.com.samplelife.features.chat.presenter

import br.com.samplelife.application.UVApplication
import br.com.samplelife.base.BaseMvpPresenterImpl
import br.com.samplelife.features.chat.service.ChatService
import br.com.samplelife.model.User
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

open class ChatPresenter: BaseMvpPresenterImpl<ChatContract.View>(),
    ChatContract.Presenter {

    override fun fetchMessages(toUser: User, fromUser: User) {
        ChatService().fetchMessage(toUser, fromUser)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                mView?.success(result)
            },
                { throwable ->
                    mView?.showMessage(throwable.message)
                }
            )
    }

    override fun registerAccessProfessionalChat() {
        ChatService().registerAccessProfessionalChat((mView?.getContext()?.applicationContext as UVApplication).login?.user?.idProfile)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
            }, { })
    }
}