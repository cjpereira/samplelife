package br.com.samplelife.features.partners.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import br.com.samplelife.R
import br.com.samplelife.model.Generic
import kotlinx.android.synthetic.main.filter_item.view.*
import rx.subjects.PublishSubject


class StateAdapter(var stateList: ArrayList<Generic>, private val context: Context, var filterSelected: ArrayList<Generic>?): RecyclerView.Adapter<StateAdapter.ViewHolder>() {

    private val onClickSubject = PublishSubject.create<Generic>()
    fun getItemClickSignal(): rx.Observable<Generic> {
        return onClickSubject.asObservable()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.state_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return stateList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView( stateList[position])

        holder.button.isSelected = false


        for (selected in filterSelected!!){
            if(selected?.genericId == stateList[position].genericId){
                holder.button.isSelected = true
            }
        }

        holder.button.setOnClickListener{
            holder.button.isSelected = !holder.button.isSelected

            var hasSelected = false

            for ((index, selected) in filterSelected!!.withIndex()){
                if(selected?.genericId == stateList[position].genericId){
                    filterSelected!!.removeAt(index)
                    hasSelected = true
                    break
                }
            }

            if(!hasSelected){
                filterSelected!!.add(stateList[position])
            }

            notifyDataSetChanged()
            onClickSubject.onNext(stateList[position])
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var button : Button = itemView.button

        fun bindView(category: Generic) {
            button.text = category.code
        }
    }

}
