package br.com.samplelife.features.chat.activity

import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.samplelife.R
import br.com.samplelife.application.UVApplication
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.chat.adapter.MessageAdapter
import br.com.samplelife.features.chat.presenter.ChatContract
import br.com.samplelife.features.chat.presenter.ChatPresenter
import br.com.samplelife.model.Message
import br.com.samplelife.model.User
import br.com.samplelife.payload.MessagePayload
import br.com.samplelife.utils.Constants
import br.com.samplelife.utils.EventListener
import br.com.samplelife.utils.EventUpdateStatus
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.chat_activity.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ChatActivity : BaseActivity<ChatContract.View, ChatContract.Presenter>(),
    ChatContract.View, View.OnClickListener {

    var toUser: User? = null
    var fromUser: User? = null

    var mSocket: Socket? = null

    var messages: ArrayList<Message> = ArrayList()

    override var mPresenter: ChatContract.Presenter = ChatPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chat_activity)

        if (intent.getSerializableExtra(Constants.PARAM_TOUSER) != null) {
            toUser = intent.getSerializableExtra(Constants.PARAM_TOUSER) as User
        }

        fromUser = (this.application as UVApplication).login?.user

        txtDescription.text = toUser?.name
        txtHealthSpecialty.text = toUser?.health_specialty

        if(toUser?.online == 1){
            photoImageView.setBorderColor(ActivityCompat.getColor(this@ChatActivity,R.color.colorStatusOnLine))
        }else{
            photoImageView.setBorderColor(ActivityCompat.getColor(this@ChatActivity,R.color.colorStatusOffLine))
        }

        if(toUser?.photo!=null && !toUser?.photo?.isEmpty()!!){
            Picasso.get()
                .load("https://s3.us-east-2.amazonaws.com/samplelife/profissional/${toUser?.photo}")
                .placeholder(R.drawable.perfil_placeholder)
                .error(R.drawable.perfil_placeholder)
                .into(photoImageView)
        }

        val app = application as UVApplication
        mSocket = app.socket

        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        recycleView!!.layoutManager = layoutManager
        recycleView!!.adapter = MessageAdapter(this)
        (recycleView!!.adapter as MessageAdapter).toUser = toUser
        (recycleView!!.adapter as MessageAdapter).fromUser = fromUser

        btnSendMessage.setOnClickListener(this)

        app.socket!!.on("add-message-health-professional-response", messageResponse)

        loading()

        mPresenter.fetchMessages(fromUser!!, toUser!!)

        backButton.setOnClickListener{
            finish()
        }

        EventUpdateStatus.getInstance().registerEvent(object : EventListener {
            override fun done(users: ArrayList<User>) {
                for(user in users){
                    if(user.id.equals(toUser?.id)){
                        toUser = user

                        if(user.online == 1){
                            photoImageView.setBorderColor(ActivityCompat.getColor(this@ChatActivity,R.color.colorStatusOnLine))
                        }else{
                            photoImageView.setBorderColor(ActivityCompat.getColor(this@ChatActivity,R.color.colorStatusOffLine))
                        }
                        break
                    }
                }
            }
        })

        mPresenter.registerAccessProfessionalChat()
    }

    private val messageResponse = Emitter.Listener { args ->
        runOnUiThread {
            val messagePayload = Gson().fromJson(args[0].toString(), MessagePayload::class.java)
            var message = messagePayload.toMessage()

            if (message.fromUserId.equals(toUser!!.id, ignoreCase = true)) {
                messages.add(message)
                groupDataIntoHashMap(messages)
            }
        }
    }

    override fun onClick(v: View) {

        if(hasConnectivity(true)){
            val app = application as UVApplication
            if (message.length() > 0 && fromUser != null && toUser != null) {
                val messagePacket = Message(message.text.toString(), fromUser?.id, toUser!!.id, Date(), false)
                app.socket!!.emit("add-message-health-professional", Gson().toJson(messagePacket))
                runOnUiThread {
                    messages.add(messagePacket)
                    groupDataIntoHashMap(messages)
                    message.setText("")
                }
            }
        }
    }

    override fun success(result: Any) {
        hideLoading()
        messages = result as ArrayList<Message>
        groupDataIntoHashMap(messages)
    }

    override fun showMessage(error: String?) {
        hideLoading()
        showMessage(error)
    }

    override fun onDestroy() {
        mSocket!!.off("add-message-health-professional-response")
        EventUpdateStatus.getInstance().unRegisterEvent()
        super.onDestroy()
    }

    private fun groupDataIntoHashMap(chatModelList: ArrayList<Message>) {

        val groupedHashMap = LinkedHashMap<String, ArrayList<Message>>()

        var list: ArrayList<Message>? = null

        for (message in chatModelList) {
            val format = SimpleDateFormat("dd/MM/yyyy")

            val hashMapKey = format.format(message.date)
            if (groupedHashMap.containsKey(hashMapKey)) {
                groupedHashMap.get(hashMapKey)!!.add(message)
            } else {
                list = ArrayList()
                list!!.add(message)
                groupedHashMap.put(hashMapKey, list)
            }
        }
        generateListFromMap(groupedHashMap)
    }

    private fun generateListFromMap(groupedHashMap: LinkedHashMap<String, ArrayList<Message>>){
        val consolidatedList = ArrayList<Message>()
        for (date in groupedHashMap.keys) {
            val message = Message(date, true)
            consolidatedList.add(message)
            consolidatedList.addAll(groupedHashMap[date]!!)
        }

        (recycleView!!.adapter as MessageAdapter).messagens = consolidatedList
        (recycleView!!.adapter as MessageAdapter).notifyItemInserted((recycleView!!.adapter as MessageAdapter).messagens.size-1)
        recycleView.scrollToPosition((recycleView!!.adapter as MessageAdapter).messagens.size-1)

    }
}