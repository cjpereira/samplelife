package br.com.samplelife.features.calendar.service

import br.com.samplelife.BuildConfig
import br.com.samplelife.api.URLBase
import br.com.samplelife.payload.EventPayload
import br.com.samplelife.payload.GenericPayload
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*


@URLBase(BuildConfig.BASE_URL)
interface ICalendarService {

    @GET("api/agenda/listar/{id}")
    fun fetchEvents(@Path("id") id: String?): Call<ArrayList<EventPayload>>

    @GET("/api/categoria-agenda/listar")
    fun fetchCategory(): Call<ArrayList<GenericPayload>>

    @POST("/api/agenda/salvar")
    fun sendCalendar(@Body jsonPayload: JsonObject): Call<JsonObject>

    @POST("/api/agenda/excluir")
    fun removeCalendar(@Header("idAgenda") idCalendar:String, @Header("idColaborador") idPerfil: String): Call<JsonObject>

}