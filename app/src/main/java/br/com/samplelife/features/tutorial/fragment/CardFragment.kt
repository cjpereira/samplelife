package br.com.samplelife.features.tutorial.fragment

import android.content.Intent
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.samplelife.R
import br.com.samplelife.features.explanation.activity.ExplanationDetailsActivity
import kotlinx.android.synthetic.main.card_fragment.view.*


class CardFragment : Fragment(){

    var data = HashMap<String, Any>()
    lateinit  var content: ViewGroup

    fun newInstance(data: HashMap<String, Any>): Fragment {
        val fragment = CardFragment()
        fragment.data = data
        return fragment
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        var view: View = inflater.inflate(R.layout.card_fragment, container, false)

        view.txtDescription.setText(data.get("title") as String)

        val row: ArrayList<HashMap<String, Any>> = data.get("item") as ArrayList<HashMap<String, Any>>

        for (i in 0..3) {
            var item = row.get(i)
            var row: View = inflater.inflate(R.layout.card_item, container, false)
            var icon: ImageView = row.findViewById(R.id.rowIcon)
            icon.setImageResource(item.get("icon") as Int)
            var rowTitle: TextView = row.findViewById(R.id.rowTitle)
            rowTitle.setText(item.get("title") as String)
            view.contentMain.addView(row)
        }

        view.cardView.setOnClickListener {
            val intent = Intent(context, ExplanationDetailsActivity::class.java)
            intent.putExtra("data", data)
            startActivity(intent)
        }

        return view
    }

}