package br.com.samplelife.features.articles.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.samplelife.R
import br.com.samplelife.model.Articles
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.article_item.view.*
import rx.subjects.PublishSubject

class ArticlesAdapter(
    var articlesList: ArrayList<Articles>,
    private val context: Context): RecyclerView.Adapter<ArticlesAdapter.ViewHolder>() {

    private val onClickSubject = PublishSubject.create<Articles>()

    fun getItemClickSignal(): rx.Observable<Articles> {
        return onClickSubject.asObservable()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.article_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return articlesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val news = articlesList[position]
        holder.itemView.cardView.setOnClickListener { v -> onClickSubject.onNext(news) }
        holder.bindView(news)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(articles: Articles) {
            val title = itemView.txtDescription
            val description = itemView.txtHealthSpecialty
            val imgNews = itemView.imgPartner

            title.text = articles.title
            description.text = articles.shortText

            Picasso.get()
                .load(articles.url)
                .placeholder(R.drawable.bg_placeholder)
                .error(R.drawable.bg_placeholder)
                .into(imgNews)
        }
    }

}
