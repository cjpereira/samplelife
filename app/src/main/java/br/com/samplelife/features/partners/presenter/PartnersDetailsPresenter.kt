package br.com.samplelife.features.partners.presenter

import br.com.samplelife.base.BaseMvpPresenterImpl

open class PartnersDetailsPresenter: BaseMvpPresenterImpl<PartnersDetailsContract.View>(),
    PartnersDetailsContract.Presenter {

}