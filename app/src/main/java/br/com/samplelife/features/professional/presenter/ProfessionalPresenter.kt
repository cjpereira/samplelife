package br.com.samplelife.features.professional.presenter


import androidx.lifecycle.OnLifecycleEvent
import br.com.samplelife.base.BaseMvpPresenterImpl
import br.com.samplelife.features.professional.service.ProfessionalService
import br.com.samplelife.model.Generic
import br.com.samplelife.model.Professional
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.BehaviorSubject

open class ProfessionalPresenter: BaseMvpPresenterImpl<ProfessionalContract.View>(),
    ProfessionalContract.Presenter {

    override fun fetchProfessional(token: String) {

        var articleObservable: rx.Observable<ArrayList<Professional>> = ProfessionalService().fetchProfessional(token, null, null)
        var categoryObservable: rx.Observable<ArrayList<Generic>> = ProfessionalService().fetchMedicalSpecialties(token)
        var stateObservable : rx.Observable<ArrayList<Generic>> = ProfessionalService().fetchState(token)

        BehaviorSubject.create<OnLifecycleEvent>()
        Observable.zip(articleObservable, categoryObservable, stateObservable)
        { articles: ArrayList<Professional>, categories:ArrayList<Generic>, states: ArrayList<Generic> ->
            mView?.updateStates(states)
            mView?.updateCategories(categories)
            articles
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({ articles ->
                mView?.success(articles)
            },
                { throwable ->
                    mView?.showMessage(throwable.message)
                })
    }

    override fun fetchProfessional(token: String, state: String?, category: String?) {

        ProfessionalService().fetchProfessional(token, state, category)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ articles  ->
                mView?.success(articles)
            },
                { throwable ->
                    mView?.showMessage(throwable.message)
                })
    }

}