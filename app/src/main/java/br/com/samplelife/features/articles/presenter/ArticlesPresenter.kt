package br.com.samplelife.features.articles.presenter

import androidx.lifecycle.OnLifecycleEvent
import br.com.samplelife.application.UVApplication
import br.com.samplelife.base.BaseMvpPresenterImpl
import br.com.samplelife.features.articles.service.ArcticlesService
import br.com.samplelife.model.Articles
import br.com.samplelife.model.Generic
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.BehaviorSubject

open class ArticlesPresenter: BaseMvpPresenterImpl<ArticlesContract.View>(),
    ArticlesContract.Presenter {

    override fun fetchNews() {

        var articleObservable: rx.Observable<ArrayList<Articles>> = ArcticlesService().fetchNews(null)
        var categoryObservable: rx.Observable<ArrayList<Generic>> = ArcticlesService().fetchCategory()

        BehaviorSubject.create<OnLifecycleEvent>()
        Observable.zip(articleObservable, categoryObservable)
        { articles: ArrayList<Articles>, categories:ArrayList<Generic> ->
            mView?.updateCategories(categories)
            articles
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({ articles ->
                mView?.success(articles)
            },
                { throwable ->
                    mView?.showMessage(throwable.message)
                })
    }

    override fun fetchNews(category: String?) {

        ArcticlesService().fetchNews(category)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ articles  ->
                mView?.success(articles)
            },
                { throwable ->
                    mView?.showMessage(throwable.message)
                })
    }

    override fun registerAccessProfessional() {
        ArcticlesService().registerAccessProfessionalChat((mView?.getContext()?.applicationContext as UVApplication).login?.user?.idProfile)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
            }, { })
    }
}