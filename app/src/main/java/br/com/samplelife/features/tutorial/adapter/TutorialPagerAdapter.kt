package br.com.samplelife.features.tutorial.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import br.com.samplelife.R
import br.com.samplelife.features.tutorial.fragment.CardFragment

class TutorialPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager){

   var data: ArrayList<HashMap<String, Any>> = data()

    override fun getItem(position: Int): Fragment {
            return CardFragment().newInstance(data.get(position))
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    fun data():  ArrayList<HashMap<String, Any>> {

        val card: ArrayList<HashMap<String, Any>> = ArrayList<HashMap<String, Any>>()

        var row: HashMap<String, Any> = HashMap()
        row.put("title", "PARA FUNCIONÁRIOS")
        row.put("item", cardEmployees())
        row.put("background",R.drawable.bg_para_funcionarios)
        card.add(row)

        row = HashMap()
        row.put("title", "PARA PARCEIROS")
        row.put("item", cardPartners())
        row.put("background",R.drawable.bg_para_parceiros)
        card.add(row)

        row = HashMap()
        row.put("title", "PARA EMPRESA")
        row.put("item", cardCompanies())
        row.put("background",R.drawable.bg_para_empresas)
        card.add(row)

        return card
    }


    private fun cardEmployees():  ArrayList<HashMap<String, Any>> {

        val card: ArrayList<HashMap<String, Any>> = ArrayList<HashMap<String, Any>>()

        var row: HashMap<String, Any> = HashMap()
        row.put("title", "Maior controle do nível de qualidade de vida")
        row.put("icon", R.drawable.ic_func_barras)
        row.put("text", "Os funcionários possuem controle dos índices que influenciam na sua qualidade de vida, podendo implementar ações de melhoria")
        card.add(row)


        row = HashMap()
        row.put("title", "Acesso as informações de saúde e qualidade de vida")
        row.put("icon", R.drawable.ic_func_corre)
        row.put("text", "O Use Vida disponibiliza o portal Info Vida com vídeos e artigos relacionados à saúde e qualidade de vida e bem-estar")
        card.add(row)


        row = HashMap()
        row.put("title", "Descontos exclusivos")
        row.put("icon", R.drawable.ic_func_desconto)
        row.put("text", "O usuário poderá economizar, graças aos nossos descontos exclusivos em estabelecimentos com produtos e serviços relacionados à qualidade de vida")
        card.add(row)


        row = HashMap()
        row.put("title", "Busca rápida por profissionais de saúde")
        row.put("icon", R.drawable.ic_func_profissionais)
        row.put("text", "Os usuários poderão encontrar profissionais de saúde especializados e qualificados por localidade, especialidade")
        card.add(row)

        return card
    }

    private fun cardPartners():  ArrayList<HashMap<String, Any>> {

        val card: ArrayList<HashMap<String, Any>> = ArrayList<HashMap<String, Any>>()

        var row: HashMap<String, Any> = HashMap()
        row.put("title", "Aumento de faturamento")
        row.put("icon", R.drawable.ic_parc_aument_fatu)
        row.put("text", "Com os incentivos que proporcionamos aos usuários, nossos parceiros têm uma grande oportunidade de um aumento real no seu faturamento")
        card.add(row)


        row = HashMap()
        row.put("title", "Maior divulgação para seu negócio")
        row.put("icon", R.drawable.ic_icone_parc_divulga_nego)
        row.put("text", "Nossos parceiros são amplamente divulgados em nosso site e redes sociais. Os usuários receberão notificações sobre promoções e lançamentos dos estabelecimentos parceiros")
        card.add(row)


        row = HashMap()
        row.put("title", "Mais relevância para o seu serviço")
        row.put("icon", R.drawable.ic_parc_maior_relevancia)
        row.put("text", "Os profissionais de saúde ganharão relevância na sua atuação, pois os temas fazem parte de uma nova abordagem de prevenção na área da qualidade de vida integrativa")
        card.add(row)

        row = HashMap()
        row.put("title", "Aumento do número de pacientes")
        row.put("icon", R.drawable.ic_parc_num_parcientes)
        row.put("text", "Os profissionais de saúde podem divulgar seus vídeos e artigos em nossa plataforma digital além de serem divulgados e recomendados para os nossos usuários")
        card.add(row)

        return card
    }

    private fun cardCompanies():  ArrayList<HashMap<String, Any>> {

        val card: ArrayList<HashMap<String, Any>> = ArrayList<HashMap<String, Any>>()

        var row: HashMap<String, Any> = HashMap()
        row.put("title", "Redução de custos")
        row.put("icon", R.drawable.ic_empre_custos)
        row.put("text", "Informações e ações de qualidade de vida no trabalho reduzem a sinistralidade dos planos de saúde e o absenteísmo nas organizações")
        card.add(row)


        row = HashMap()
        row.put("title", "Aumento de produtividade")
        row.put("icon", R.drawable.ic_empre_produtividade)
        row.put("text", "Saúde e bem-estar no trabalho exercem grande influência sobre a motivação, criatividade e produtividade dos funcionários")
        card.add(row)


        row = HashMap()
        row.put("title", "Mapeamento da qualidade de vida dos funcionários")
        row.put("icon", R.drawable.ic_empre_mapeamento)
        row.put("text", "Identificar as oportunidades de melhoria na qualidade de vida dos funcionários é a estratégia correta para um investimento em programas de saúde e bem-estar")
        card.add(row)


        row = HashMap()
        row.put("title", "Aumento da satisfação dos funcionários")
        row.put("icon", R.drawable.ic_empre_satisfacao)
        row.put("text", "Funcionários com equilíbrio na qualidade de vida e com hábitos regulares de saúde são funcionários mais satisfeitos")
        card.add(row)


        row = HashMap()
        row.put("title", "Aumento do pacote dos benefícios")
        row.put("icon", R.drawable.ic_empr_beneficios)
        row.put("text", "Somos um grande benefício oferecido aos funcionários. Moderno, eficiente e prático, nos destacamos por proporcionar a maximização do bem-estar")
        card.add(row)


        row = HashMap()
        row.put("title", "Melhora do clima organizacional")
        row.put("icon", R.drawable.ic_empr_clima)
        row.put("text", "Empresas que investem em benefícios para os funcionários tem um melhor clima organizacional e estão no topo das melhores empresas para se trabalhar")
        card.add(row)


        return card
    }

}