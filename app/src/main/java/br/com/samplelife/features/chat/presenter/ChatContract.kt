package br.com.samplelife.features.chat.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView
import br.com.samplelife.model.User

object ChatContract {

    interface View : BaseMvpView

    interface Presenter : BaseMvpPresenter<View> {
        fun fetchMessages(toUser: User, fromUser: User)
        fun registerAccessProfessionalChat()
    }
}
