package br.com.samplelife.features.professional.fragment

import android.content.Intent
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.samplelife.R
import br.com.samplelife.features.home.activity.HomeActivity
import br.com.samplelife.features.partners.activity.PartnersActivity
import br.com.samplelife.features.professional.activity.HealthProfessionalActivity
import kotlinx.android.synthetic.main.home_activity.*
import kotlinx.android.synthetic.main.professional_fragment.view.*

class ProfessionalFragment : Fragment() {

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.professional_fragment, container, false)

        view.cardHealthProfessional.setOnClickListener {
            startActivity(Intent(activity, HealthProfessionalActivity::class.java))
        }

        view.cardPartners.setOnClickListener{
            startActivity(Intent(activity, PartnersActivity::class.java))
        }

        view.toogleMenu.setOnClickListener{
            (activity as HomeActivity).drawer_layout.openDrawer(Gravity.LEFT)
        }

        return view
    }

}
