package br.com.samplelife.features.splash.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView

object SplashContract {

    interface View : BaseMvpView{
        fun error()
    }

    interface Presenter : BaseMvpPresenter<View> {
        fun autoLogin()
        fun logout()
    }

}
