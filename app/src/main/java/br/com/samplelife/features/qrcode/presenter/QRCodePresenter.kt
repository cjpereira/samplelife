package br.com.samplelife.features.qrcode.presenter

import br.com.samplelife.application.UVApplication
import br.com.samplelife.base.BaseMvpPresenterImpl
import br.com.samplelife.features.qrcode.service.QRCodeService
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

open class QRCodePresenter: BaseMvpPresenterImpl<QRCodeContract.View>(),
    QRCodeContract.Presenter {

    override fun validateQRCode(qrcode: String) {

        QRCodeService().fetchPartners((mView?.getContext()?.applicationContext as UVApplication).login?.user!!, qrcode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ partners  ->
                mView?.success(partners)
            },
            { throwable ->
                mView?.showMessage(throwable.message)
            })

    }

}