package br.com.samplelife.features.articles.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import br.com.samplelife.R
import br.com.samplelife.features.partners.adapter.CategoryAdapter
import br.com.samplelife.model.Generic
import br.com.samplelife.utils.Constants
import br.com.samplelife.view.SpacesItemDecoration
import kotlinx.android.synthetic.main.partnersfilter_activity.*


class ArticlesFilterActivity : AppCompatActivity() {

    private var categoryFilter: ArrayList<Generic>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.articlefilter_activity)

        var categories = intent.getSerializableExtra(Constants.PARAM_CATEGORIES) as ArrayList<Generic>
        categoryFilter = intent.getSerializableExtra(Constants.PARAM_CATEGORY_FILTER) as ArrayList<Generic>

        var gridLayoutManager = GridLayoutManager(this, 3)
        recyclerViewCategory.layoutManager = gridLayoutManager
        recyclerViewCategory.addItemDecoration(SpacesItemDecoration(10))

        recyclerViewCategory.adapter = CategoryAdapter(categories, this, categoryFilter!!)
        recyclerViewCategory.setHasFixedSize(true)

        btnConfirm.setOnClickListener{
            var categoryAdapter = recyclerViewCategory.adapter as CategoryAdapter

            val resultIntent = Intent()
            resultIntent.putExtra(Constants.PARAM_CATEGORY_FILTER, categoryAdapter.filterCategory)
            setResult(Activity.RESULT_OK, resultIntent)

            finish()
        }

    }

}