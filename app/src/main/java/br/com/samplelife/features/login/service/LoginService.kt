package br.com.samplelife.features.login.service

import br.com.samplelife.api.ApiException
import br.com.samplelife.api.ApiManager
import br.com.samplelife.features.chat.service.IChatService
import br.com.samplelife.model.Login
import br.com.samplelife.payload.LoginPayload
import br.com.samplelife.payload.LoginResponsePayload
import br.com.samplelife.utils.Constants
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Response
import rx.Observable

class LoginService{

    fun doLogin(login: String, password: String): Observable<Login> {

        return Observable.create {emitter ->

            val service = ApiManager.createRetrofit(ILoginService::class.java)
            try {

                var response: Response<LoginResponsePayload> = service.doLogin(login, password).execute()

                if (response.errorBody() == null){

                    //Atualiza pushid
                    var responseLogin: LoginPayload? = response.body()?.loginPayload

                    if(responseLogin?.userPayload?.profile != "COLABORADOR"){
                        throw ApiException("Acesso apenas para colaborador")
                    }

                    var login = responseLogin?.toLogin()

                    emitter.onNext(login)
                    emitter.onCompleted()
                }else{
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    val resposneLogin = Gson().fromJson<LoginPayload>(errorJson.get("retorno").toString(), LoginPayload::class.java)

                    if(resposneLogin?.mensagem!=null) {
                        throw ApiException(resposneLogin?.mensagem!!)
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                if(e is ApiException){
                    emitter.onError(e)
                }else{
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }
        }
    }

    fun addPushId(userId: String): Observable<Boolean> {

        return Observable.create {emitter ->

            val service = ApiManager.createRetrofit(IChatService::class.java)
            try {
                val token = FirebaseInstanceId.getInstance().token

                var jsonRequest = JsonObject()
                jsonRequest.addProperty("userId", userId)
                jsonRequest.addProperty("pushId", token)
                service.addPushId(jsonRequest).execute()

                emitter.onNext(true)
                emitter.onCompleted()

            }catch (e: Exception){
                if(e is ApiException){
                    emitter.onError(e)
                }else{
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }
        }
    }

    fun doRecoveryPassword(user: String): Observable<String> {

        return Observable.create {emitter ->

            val service = ApiManager.createRetrofit(ILoginService::class.java)
            try {

                var response: Response<LoginResponsePayload> = service.recoveryPassword(user).execute()

                if (response.errorBody() == null){
                    var responseLogin: LoginPayload? = response.body()?.loginPayload

                    emitter.onNext(responseLogin?.mensagem)
                    emitter.onCompleted()
                }else{
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    val resposneLogin = Gson().fromJson<LoginPayload>(errorJson.get("retorno").toString(), LoginPayload::class.java)
                    if(resposneLogin?.mensagem!=null) {
                        throw ApiException(resposneLogin?.mensagem!!)
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                if(e is ApiException){
                    emitter.onError(e)
                }else{
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }
        }
    }

    fun doChangePassword(user: String, password: String): Observable<String> {

        return Observable.create {emitter ->

            val service = ApiManager.createRetrofit(ILoginService::class.java)
            try {

                var response: Response<LoginResponsePayload> = service.changePassword(user, password).execute()

                if (response.errorBody() == null){
                    var responseLogin: LoginPayload? = response.body()?.loginPayload

                    emitter.onNext(responseLogin?.mensagem)
                    emitter.onCompleted()
                }else{
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    val resposneLogin = Gson().fromJson<LoginPayload>(errorJson.get("retorno").toString(), LoginPayload::class.java)
                    if(resposneLogin?.mensagem!=null) {
                        throw ApiException(resposneLogin?.mensagem!!)
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                if(e is ApiException){
                    emitter.onError(e)
                }else{
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }
        }
    }

    fun logout(): Observable<Boolean> {

        return Observable.create {emitter ->

            val service = ApiManager.createRetrofit(IChatService::class.java)
            try {
                var token = FirebaseInstanceId.getInstance().token
                var jsonRequest = JsonObject()
                jsonRequest.addProperty("pushid", token)
                service.logout(jsonRequest).execute()
                emitter.onNext(true)
                emitter.onCompleted()

            }catch (e: Exception){
                if(e is ApiException){
                    emitter.onError(e)
                }else{
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }
        }
    }

}