package br.com.samplelife.features.chat.fragment

import android.content.Intent
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.samplelife.R
import br.com.samplelife.application.UVApplication
import br.com.samplelife.features.chat.activity.ChatActivity
import br.com.samplelife.features.chat.adapter.ChatAdapter
import br.com.samplelife.features.home.activity.HomeActivity
import br.com.samplelife.model.User
import br.com.samplelife.utils.Constants
import br.com.samplelife.utils.EventUpdateStatus
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.socket.client.Socket
import io.socket.emitter.Emitter
import io.socket.engineio.client.EngineIOException
import kotlinx.android.synthetic.main.chat_fragment.*
import kotlinx.android.synthetic.main.chat_fragment.view.*
import kotlinx.android.synthetic.main.home_activity.*
import java.net.ConnectException


class ChatFragment : Fragment() {

    var mSocket: Socket? = null
    var user: User? = null

    var pushFromUser:String? = null

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.chat_fragment, container, false)
        val recyclerView = view.recyclerView

        recyclerView.apply {
            // set a LinearLayoutManager to handle Android
            // RecyclerView behavior
            layoutManager = LinearLayoutManager(activity)
            // set the custom adapter to the RecyclerView
            adapter = ChatAdapter(ArrayList(), this.context!!)
        }

        view.toogleMenu.setOnClickListener{
            (activity as HomeActivity).drawer_layout.openDrawer(Gravity.LEFT)
        }

        return view
    }

    fun startChat(){
        val app = activity?.application as UVApplication

        if(app.login !=null && (app.socket ==null || !app.socket?.connected()!!)){
            app.startSocket((activity?.application as UVApplication).login?.user?.id!!)
            app.socket!!.on(Socket.EVENT_CONNECT, onConnect)
            app.socket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
            app.socket!!.on(Socket.EVENT_CONNECT_ERROR, onDisconnect)

            app.socket!!.connect()
            mSocket = app.socket
            app.socket!!.on("chat-list-update", chatListUpdate)
            mSocket!!.on("chat-list-health-professionals-response", chatResponse)
        }
    }

    fun stopChat(){
        val app = activity?.application as UVApplication
        if(app.socket !=null && app.socket?.connected()!!) {
            mSocket!!.off("chat-list-health-professionals-response")
            mSocket!!.disconnect()
            mSocket!!.close()
        }
    }

    override fun onResume() {
        super.onResume()
        startChat()
    }

    override fun onDestroy() {
        stopChat()
        super.onDestroy()
    }

    private val onDisconnect = Emitter.Listener {

        if(it[0] is EngineIOException && (it[0] as EngineIOException).cause is ConnectException && (recyclerView!!.adapter as ChatAdapter).chatList.size == 0){
            txtStatusChat.text = Constants.MSG_CONNECTION_NOT_AVAILABLE
        }

    }

    private val onConnect = Emitter.Listener {
        if((activity?.application as UVApplication).login?.token!=null){
            mSocket!!.emit("chat-list-health-professionals", (activity?.application as UVApplication).login?.user?.id)
        }else{
            stopChat()
        }
    }

    private val chatListUpdate = Emitter.Listener {
        if((activity?.application as UVApplication).login?.token!=null){
            mSocket!!.emit("chat-list-health-professionals", (activity?.application as UVApplication).login?.user?.id)
        }else{
            stopChat()
        }
    }

    private val chatResponse = Emitter.Listener { args ->

        activity?.runOnUiThread {
            val response = Gson().fromJson(args[0].toString(), JsonObject::class.java)

            val users = ArrayList<User>()

            if (response.has("chatList") && response.getAsJsonArray("chatList") != null && response.getAsJsonArray("chatList").size() > 0) {
                for (json in response.getAsJsonArray("chatList")) {
                    val user = Gson().fromJson(json, User::class.java)
                    users.add(user)
                }

                (recyclerView!!.adapter as ChatAdapter).chatList = users
                EventUpdateStatus.getInstance().emitterDone(users)
                user = Gson().fromJson(response.getAsJsonObject("userinfo"), User::class.java)
                txtStatusChat.visibility = View.GONE
                recyclerView!!.adapter!!.notifyDataSetChanged()

                for (user in users) {
                    if(pushFromUser!=null && pushFromUser.equals(user.id)){
                        val intent = Intent(context, ChatActivity::class.java)
                        intent.putExtra("toUser", user)
                        context?.startActivity(intent)
                        pushFromUser = null
                        break
                    }
                }

            }
        }
    }

}