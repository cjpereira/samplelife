package br.com.samplelife.features.splash.activity

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import br.com.samplelife.R
import br.com.samplelife.application.UVApplication
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.home.activity.HomeActivity
import br.com.samplelife.features.splash.presenter.SplashContract
import br.com.samplelife.features.splash.presenter.SplashPresenter
import br.com.samplelife.model.Login
import br.com.samplelife.utils.Constants
import kotlinx.android.synthetic.main.splash_activity.*

class SplashActivity : BaseActivity<SplashContract.View, SplashContract.Presenter>(),
    SplashContract.View, Animator.AnimatorListener {

    override fun onAnimationRepeat(p0: Animator?) {}

    override fun onAnimationEnd(p0: Animator?) {
        completeAnimation = true
        if (completeService) {
            goHome()
        }
    }

    override fun onAnimationCancel(p0: Animator?) {}

    override fun onAnimationStart(p0: Animator?) {}

    override var mPresenter: SplashContract.Presenter = SplashPresenter()

    private var completeAnimation = false
    private var completeService = false
    private var loginService: Login? = null

    var pageId:String? = null
    var fromUser:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
        splashAnimation.addAnimatorListener(this)

        if(intent.extras!=null && intent.extras!!.get(Constants.PARAM_PAGEID) !=null) {
            pageId = intent.extras!!.get(Constants.PARAM_PAGEID) as String
        }

        if(intent.extras!=null && intent.extras!!.get(Constants.PARAM_FROMUSER) !=null) {
            fromUser = intent.extras!!.get(Constants.PARAM_FROMUSER) as String
        }

        mPresenter.autoLogin()
    }

    override fun success(result:Any) {
        completeService = true
        loginService = result as Login
        if(completeAnimation) {
            goHome()
        }
    }

    private fun goHome(){

        if (loginService is Login){
            (application as UVApplication).login = loginService
        }

        var intent = Intent(this, HomeActivity::class.java)

        if(pageId!=null && pageId!!.isNotEmpty()){
            intent.putExtra(Constants.PARAM_PAGEID, pageId)
        }

        if(fromUser!=null && fromUser!!.isNotEmpty()){
            intent.putExtra(Constants.PARAM_FROMUSER, fromUser)
        }

        startActivity(intent)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

    override fun error() {
        completeService = true
        if(completeAnimation) {
            mPresenter.logout()
            goHome()
        }
    }

}