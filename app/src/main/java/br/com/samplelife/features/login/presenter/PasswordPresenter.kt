package br.com.samplelife.features.login.presenter

import br.com.samplelife.R
import br.com.samplelife.base.BaseMvpPresenterImpl
import br.com.samplelife.features.login.service.LoginService
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

open class PasswordPresenter: BaseMvpPresenterImpl<PasswordContract.View>(),
    PasswordContract.Presenter {

    override fun doRecoveryPassword(user: String){

        if(!user.isEmpty()){
            LoginService().doRecoveryPassword(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    mView?.success(result)
                },
                { throwable ->
                    mView?.showMessage(throwable.message)
                }
            )
        }else{
            mView?.showErrorUser(R.string.msg_error_empty)
        }
    }

    override fun doChangePassword(user: String, password: String){

        if(!password.isEmpty() ){
            LoginService().doChangePassword(user, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    mView?.success(result)
                },
                { throwable ->
                    mView?.showMessage(throwable.message)
                }
            )
        }else{
            mView?.showErrorPassword(R.string.msg_error_empty)
        }
    }

}