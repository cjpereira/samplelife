package br.com.samplelife.features.chat.adapter

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.samplelife.R
import br.com.samplelife.features.chat.activity.ChatActivity
import br.com.samplelife.model.User
import com.squareup.picasso.Picasso
import io.reactivex.annotations.NonNull
import kotlinx.android.synthetic.main.chat_item.view.*

class ChatAdapter(var chatList: ArrayList<User>,
    private val context: Context): RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(this.context).inflate(R.layout.chat_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return chatList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val chat = chatList[position]
        viewHolder.bindView(chat)

        viewHolder.itemView.setOnClickListener {
            val intent = Intent(context, ChatActivity::class.java)
            intent.putExtra("toUser", chat)
            context.startActivity(intent)
        }

    }

    class ViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(user: User) {
            itemView.txtDescription.text = user.name
            itemView.txtHealthSpecialty.text = user.health_specialty
            
            itemView.btnOnline.isSelected = (user.online == 1)
            if(user.online == 1){
                itemView.btnOnline.visibility = View.VISIBLE
                itemView.txtOffline.visibility = View.INVISIBLE
            }else{
                itemView.btnOnline.visibility = View.GONE
                itemView.txtOffline.visibility = View.VISIBLE
            }

            if(user.photo!=null && !user?.photo?.isEmpty()!!){
                Picasso.get()
                    .load("https://s3.us-east-2.amazonaws.com/samplelife/profissional/${user.photo}")
                    .placeholder(R.drawable.rectangle_3)
                    .error(R.drawable.rectangle_3)
                    .into(itemView.imgProfessional)
            }
        }
    }
}