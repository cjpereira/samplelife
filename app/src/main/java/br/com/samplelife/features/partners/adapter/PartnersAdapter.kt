package br.com.samplelife.features.partners.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.samplelife.R
import br.com.samplelife.model.Partner
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.partner_item.view.*
import rx.subjects.PublishSubject

class PartnersAdapter(var articlesList: ArrayList<Partner>, private val context: Context): RecyclerView.Adapter<PartnersAdapter.ViewHolder>() {

    private val onClickSubject = PublishSubject.create<Partner>()

    fun getItemClickSignal(): rx.Observable<Partner> {
        return onClickSubject.asObservable()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.partner_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return articlesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val partners = articlesList[position]

        holder.itemView.cardView.setOnClickListener { v -> onClickSubject.onNext(partners) }
        holder.bindView(partners)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(partner: Partner) {
            val title = itemView.txtDescription
            val description = itemView.txtHealthSpecialty
            val imgPartner = itemView.imgPartner

            title.text = partner.name
            description.text = itemView.context.getString(R.string.partners_description_discount, partner.discount?.percent)

            Picasso.get().load(partner.urlImage)
                .placeholder(R.drawable.bg_placeholder)
                .error(R.drawable.bg_placeholder).into(imgPartner)

        }
    }

}
