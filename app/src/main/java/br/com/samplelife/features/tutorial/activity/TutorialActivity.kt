package br.com.samplelife.features.tutorial.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.samplelife.R
import br.com.samplelife.features.tutorial.adapter.TutorialPagerAdapter
import br.com.samplelife.preference.PreferenceHelper
import br.com.samplelife.utils.Constants
import br.com.samplelife.utils.Utils
import kotlinx.android.synthetic.main.tutorial_activity.*


class TutorialActivity : AppCompatActivity() {


    override fun onBackPressed() {
        PreferenceHelper(this).save(Constants.PARAM_SHOW_TUTORIAL, false)
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tutorial_activity)

        viewPager.offscreenPageLimit = 10
        viewPager.clipToPadding = false
        viewPager.setPageMargin(Utils.dpToPx(this, -45).toInt())
        viewPager.adapter = TutorialPagerAdapter(this.supportFragmentManager)

        pagerIndicator.setViewPager(viewPager)

        btnSkip.setOnClickListener{
            onBackPressed()
        }
    }
}