package br.com.samplelife.features.calendar.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.samplelife.R
import br.com.samplelife.features.home.activity.HomeActivity
import br.com.samplelife.model.Event
import kotlinx.android.synthetic.main.item_event.view.*
import kotlinx.android.synthetic.main.item_event.view.cardView
import kotlinx.android.synthetic.main.item_eventheader.view.txtCategory
import kotlinx.android.synthetic.main.item_eventheader.view.txtDay
import kotlinx.android.synthetic.main.item_eventheader.view.txtDescription
import kotlinx.android.synthetic.main.item_eventheader.view.txtHour
import kotlinx.android.synthetic.main.item_eventheader.view.txtMonth
import rx.subjects.PublishSubject
import java.text.SimpleDateFormat

class CalendarAdapter(var calendarList: ArrayList<Event>, private val context: Context): RecyclerView.Adapter<CalendarAdapter.ViewHolder>() {

    private val VIEW_TYPE_HEADER = 1
    private val VIEW_TYPE_EVENT = 2


    private val onClickSubject = PublishSubject.create<Event>()

    fun getItemClickSignal(): rx.Observable<Event> {
        return onClickSubject.asObservable()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return if (viewType === VIEW_TYPE_HEADER) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_eventheader, parent, false)
            CalendarAdapter.ViewHolder(view)
        } else{
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_event, parent, false)
            CalendarAdapter.ViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return calendarList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val event = calendarList[position]
        holder.bindView(event)

        holder.itemView.cardView.setOnClickListener { v -> onClickSubject.onNext(event) }

        holder.itemView.txtPhone.setOnClickListener{
            if(holder.itemView.txtPhone.text.toString().isNotEmpty()){
                (context as HomeActivity).call(holder.itemView.txtPhone.text.toString())
            }
        }
    }

    // Determines the appropriate ViewType according to the sender of the message.
    override fun getItemViewType(position: Int): Int {
        val event = calendarList[position]
        return if(event.isHeader) {
            VIEW_TYPE_HEADER
        }else{
            VIEW_TYPE_EVENT
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(event: Event) {

            var format = SimpleDateFormat("dd")
            val day = format.format(event.date)
            itemView.txtDay.text = day

            format = SimpleDateFormat("MMM")
            val month = format.format(event.date)
            itemView.txtMonth.text = month.toUpperCase()

            format = SimpleDateFormat("HH:mm")
            val hour = format.format(event.date)
            itemView.txtHour.text = hour

            itemView.txtCategory.text = event.category?.name
            itemView.txtDescription.text = event.name

            itemView.txtPhone.text = event.phone
        }
    }

}