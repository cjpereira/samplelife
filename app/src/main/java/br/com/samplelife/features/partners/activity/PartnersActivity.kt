package br.com.samplelife.features.partners.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import br.com.samplelife.R
import br.com.samplelife.application.UVApplication
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.partners.adapter.PartnersAdapter
import br.com.samplelife.features.partners.presenter.PartnersContract
import br.com.samplelife.features.partners.presenter.PartnersPresenter
import br.com.samplelife.model.Generic
import br.com.samplelife.model.Partner
import br.com.samplelife.utils.Constants
import kotlinx.android.synthetic.main.partners_fragment.*

class PartnersActivity : BaseActivity<PartnersContract.View, PartnersContract.Presenter>(),
    PartnersContract.View {

    override var mPresenter: PartnersContract.Presenter =
        PartnersPresenter()

    var states: ArrayList<Generic>? = null
    var categories: ArrayList<Generic>? = null

    var stateFilter: ArrayList<Generic> =  ArrayList()
    var categoryFilter: ArrayList<Generic> = ArrayList()

    var showQrcode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.partners_fragment)

        showQrcode = intent.getBooleanExtra(Constants.PARAM_QRCODE, false)

        recyclerViewPartners.apply {
            // set a LinearLayoutManager to handle Android
            // RecyclerView behavior
            layoutManager = LinearLayoutManager(this@PartnersActivity)
            // set the custom adapter to the RecyclerView
            adapter = PartnersAdapter(ArrayList(), this.context!!)
        }

        recyclerViewPartners.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (recyclerView.canScrollVertically(-1)) {
                    shadowTopView.visibility = View.VISIBLE
                }else{
                    shadowTopView.visibility = View.GONE
                }
            }
        })

        btnFilter.visibility = View.INVISIBLE

        var adapter = PartnersAdapter(ArrayList(), this)

        adapter.getItemClickSignal().subscribe{partner ->

            if(showQrcode){
                var intent = Intent(this, QRCodeActivity::class.java)
                intent.putExtra(Constants.PARAM_PARTNERS, partner)
                startActivity(intent)
            }else{
                var intent = Intent(this, PartnersDetailsActivity::class.java)
                intent.putExtra(Constants.PARAM_PARTNERS, partner)
                startActivity(intent)
            }
        }

        btnFilter.setOnClickListener{
            var intent = Intent(this, PartnersFilterActivity::class.java)
            intent.putExtra(Constants.PARAM_CATEGORIES, categories)
            intent.putExtra(Constants.PARAM_STATES, states)

            if(stateFilter!=null){
                intent.putExtra(Constants.PARAM_STATE_FILTER, stateFilter)
            }

            if(categoryFilter!=null){
                intent.putExtra(Constants.PARAM_CATEGORY_FILTER, categoryFilter)
            }

            startActivityForResult(intent, Constants.PARAM_REQUEST_CODE_FILTER)
        }

        recyclerViewPartners.adapter = adapter

        backButton.setOnClickListener{
           finish()
        }

        btnTryAgain.setOnClickListener{
            txtNotFound.visibility = View.GONE
            btnTryAgain.visibility = View.GONE

            if(categories!=null && categories?.size!! > 0){
                fetchByFilter()
            }else{
                shimmer_view_container.startShimmer()
                shimmer_view_container.visibility = View.VISIBLE
                mPresenter.fetchPartners((application as UVApplication).login?.token!!)
            }
        }

        if((application as UVApplication).login?.token!=null && adapter.articlesList.size == 0){
            mPresenter.fetchPartners((application as UVApplication).login?.token!!)
        }
    }

    override fun success(result: Any) {
        var adapter: PartnersAdapter = recyclerViewPartners.adapter as PartnersAdapter
        shimmer_view_container.stopShimmer()
        shimmer_view_container.visibility = View.GONE
        btnFilter?.visibility = View.VISIBLE
        adapter.articlesList = result as ArrayList<Partner>
        adapter.notifyDataSetChanged()
    }

    override fun showMessage(error: String?) {
        shimmer_view_container.visibility = View.GONE
        txtNotFound.visibility = View.VISIBLE

        txtNotFound.text = error
        btnTryAgain.visibility = View.VISIBLE

        if(categories!=null && categories?.size!!>0){
            btnFilter?.visibility = View.VISIBLE
        }
    }

    override fun updateCategories(categories: Any) {
        this.categories = categories as ArrayList<Generic>
    }

    override fun updateStates(states: Any) {
        this.states = states as ArrayList<Generic>
    }

    private fun fetchByFilter(){
        var strState : String? = null

        if(stateFilter!=null && stateFilter?.size!! > 0) {

            var strCodeState = StringBuilder()

            for ((index, selected) in stateFilter!!.withIndex()) {
                strCodeState.append(selected.genericId)
                if (index < stateFilter!!.size - 1) {
                    strCodeState.append(",")
                }
            }
            strState = strCodeState.toString()
        }

        var strCategory : String? = null

        if(categoryFilter!=null && categoryFilter?.size!! > 0) {

            var strCodeCategory = StringBuilder()

            for ((index, selected) in categoryFilter!!.withIndex()) {
                strCodeCategory.append(selected.genericId)
                if (index < categoryFilter!!.size - 1) {
                    strCodeCategory.append(",")
                }
            }
            strCategory = strCodeCategory.toString()
        }

        var adapter: PartnersAdapter = recyclerViewPartners.adapter as PartnersAdapter
        adapter.articlesList = ArrayList()
        adapter.notifyDataSetChanged()
        shimmer_view_container.startShimmer()
        shimmer_view_container.visibility = View.VISIBLE

        if((categoryFilter!=null && categoryFilter?.size!! > 0) || (stateFilter!=null && stateFilter.size >0)){
            btnFilter?.setImageResource(R.drawable.ic_filtro_definido)
        }else{
            btnFilter?.setImageResource(R.drawable.ic_filtro_zerado)
        }

        btnFilter?.visibility = View.INVISIBLE

        mPresenter.fetchPartners((application as UVApplication).login?.token!!, strState, strCategory)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == Constants.PARAM_REQUEST_CODE_FILTER && resultCode == Activity.RESULT_OK){
            txtNotFound.visibility = View.GONE
            btnTryAgain.visibility = View.GONE

            var paramCategoryFilter = data?.getSerializableExtra(Constants.PARAM_CATEGORY_FILTER) as ArrayList<Generic>
            var paramStateFilter = data?.getSerializableExtra(Constants.PARAM_STATE_FILTER) as ArrayList<Generic>
            categoryFilter = paramCategoryFilter
            stateFilter = paramStateFilter
            fetchByFilter()

        }
    }

}