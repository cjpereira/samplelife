package br.com.samplelife.features.splash.presenter

import android.os.Handler
import br.com.samplelife.base.BaseMvpPresenterImpl
import br.com.samplelife.features.login.service.LoginService
import br.com.samplelife.preference.PreferenceHelper
import br.com.samplelife.utils.Constants
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

open class SplashPresenter: BaseMvpPresenterImpl<SplashContract.View>(),
    SplashContract.Presenter {

    private var resultService: Any? = null

    companion object {
        var SPLASH_TIME_OUT : Long = 3000
    }

    override fun autoLogin() {

        var login = PreferenceHelper(mView?.getContext()!!).getValueString(Constants.PARAM_CPF)
        var password = PreferenceHelper(mView?.getContext()!!).getValueString(Constants.PARAM_PASSWORD)

        var callAPI = false
        var finishedTimer = false


        Handler().postDelayed({

            if (!callAPI){

                if(resultService != null){
                    mView?.success(resultService!!)
                }else{
                    mView?.error()
                }
            }

            finishedTimer = true
        }, SPLASH_TIME_OUT)

        if(login!=null && password!=null){
            callAPI = true

            LoginService().doLogin(login, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->

                    callAPI = false
                    resultService = result

                    if(finishedTimer){
                        mView?.success(resultService!!)
                    }

                },
                    { throwable ->
                        callAPI = false
                        if(finishedTimer){
                            mView?.error()
                        }
                    }
                )
        }else{

        }

    }

    override fun logout(){

        LoginService().logout()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({}, {}
            )
    }

}