package br.com.samplelife.features.partners.adapter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import br.com.samplelife.R
import br.com.samplelife.model.Generic
import kotlinx.android.synthetic.main.filter_item.view.*


class CategoryAdapter(private var categoryList: ArrayList<Generic>, private val context: Context, var filterCategory: ArrayList<Generic>): RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    var update = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.filter_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView( categoryList[position])

        holder.button.isSelected = false

        for (selected in filterCategory!!){
            if(selected?.genericId == categoryList[position].genericId){
                holder.button.isSelected = true
            }
        }


        holder.button.setOnClickListener{

            var hasSelected = false

            for ((index, selected) in filterCategory!!.withIndex()){
                if(selected?.genericId == categoryList[position].genericId){
                    filterCategory!!.removeAt(index)
                    hasSelected = true
                    break
                }
            }

            if(!hasSelected){
                filterCategory!!.add(categoryList[position])
            }

            notifyDataSetChanged()
        }

        val density = context.resources.displayMetrics.density
        var size =  (45 * density).toInt()
//        val target = object : Target {
//
//            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
//
//                val d = BitmapDrawable(
//                    context.resources,
//                    Bitmap.createScaledBitmap(
//                        bitmap,
//                        size,
//                        size,
//                        true
//                    )
//                )
//
//                holder.button.setCompoundDrawablesWithIntrinsicBounds(null, d, null, null)
//            }
//
//            override fun onBitmapFailed(e: Exception, errorDrawable: Drawable) {
//
//            }
//
//            override fun onPrepareLoad(placeHolderDrawable: Drawable) {
//
//            }
//        }
//
//        Picasso.get().load(categoryList[position].icon).placeholder(R.drawable.btnplaceholder).error(R.drawable.btnplaceholder).into(target)


        if(categoryList[position].bitmap!=null){
            var bmp = BitmapFactory.decodeByteArray(categoryList[position].bitmap, 0, categoryList[position].bitmap!!.size)
            val d = BitmapDrawable(
                context.resources,
                Bitmap.createScaledBitmap(
                    bmp,
                    size,
                    size,
                    true
                )
            )
            holder.button.setCompoundDrawablesWithIntrinsicBounds(null, d, null, null)
        }else{
            holder.button.setCompoundDrawablesWithIntrinsicBounds(null, context.getDrawable(R.drawable.btnplaceholder), null, null)
        }


    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var button : Button = itemView.button

        fun bindView(category: Generic) {
            button.text = category.name
        }
    }

}
