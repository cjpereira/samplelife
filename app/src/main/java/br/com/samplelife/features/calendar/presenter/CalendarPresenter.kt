package br.com.samplelife.features.calendar.presenter

import android.app.AlertDialog
import br.com.samplelife.R
import br.com.samplelife.application.UVApplication
import br.com.samplelife.base.BaseMvpPresenterImpl
import br.com.samplelife.features.calendar.activity.AddCalendarActivity
import br.com.samplelife.features.calendar.service.CalendarService
import br.com.samplelife.model.Event
import br.com.samplelife.model.Generic
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*


open class CalendarPresenter: BaseMvpPresenterImpl<CalendarContract.View>(),
    CalendarContract.Presenter {


    override fun sendCalendar(id:String?, name: String, category: Generic?, date: Calendar?, description: String) {

        if(validate(name, category, date)){

            (mView?.getContext() as AddCalendarActivity).loading()

            CalendarService().sendCalendar(id, name, category?.genericId!!, (mView?.getContext()!!.applicationContext as UVApplication).login?.user?.idProfile!!, date!!, description)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ events  ->
                    mView?.success(events)
                },
                { throwable ->
                    mView?.showMessage(throwable.message)
                })
        }
    }

    override fun removeCalendar(event: Event) {

            (mView?.getContext() as AddCalendarActivity).loading()

            CalendarService().removeCalendar(event?.id!!, (mView?.getContext()!!.applicationContext as UVApplication).login?.user?.idProfile!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result  ->
                    (mView?.getContext() as AddCalendarActivity).hideLoading()

                    val alertDialogBuilder = AlertDialog.Builder(mView?.getContext())
                    alertDialogBuilder.setMessage("Agenda excluida com sucesso.")
                    alertDialogBuilder.setTitle("")
                    alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton(mView?.getContext()?.resources?.getString(R.string.dialog_neutral_button)) { dialog, id ->
                            (mView?.getContext() as AddCalendarActivity).finish()
                        }
                    val alertD = alertDialogBuilder.create()
                    alertD.show()

                },
                { throwable ->
                    mView?.showMessage(throwable.message)
                })

    }


    private fun validate(name: String, speciality: Generic?, date: Calendar?): Boolean{

        var isValid = true

        if (name!=null && name?.isEmpty()!!) {
            isValid = false
            mView?.showErrorDescription(mView?.getContext()!!.getString(R.string.msg_error_empty))
        }

        if (speciality==null) {
            isValid = false
            mView?.showErroCategory(mView?.getContext()!!.getString(R.string.msg_error_empty))
        }

        if (date==null) {
            isValid = false
            mView?.showErroDate(mView?.getContext()!!.getString(R.string.msg_error_empty))
        }

        return isValid
    }

    override fun fetchEvents(id: String?) {
        CalendarService().fetchEvents(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ events  ->
                mView?.success(events)
            },
            { throwable ->
                mView?.showMessage(throwable.message)
            })
    }

    override fun fetchSpecialisties() {


        CalendarService().fetchCategory()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ events ->
                mView?.successProfessional(events)
            },
                { throwable ->
                    mView?.showMessage(throwable.message)
                })
    }

}