package br.com.samplelife.features.login.activity

import android.content.DialogInterface
import android.os.Bundle
import br.com.samplelife.R
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.login.presenter.PasswordContract
import br.com.samplelife.features.login.presenter.PasswordPresenter
import kotlinx.android.synthetic.main.forgotpassword_activity.*

class RecoverPasswordActivity : BaseActivity<PasswordContract.View, PasswordContract.Presenter>(),
    PasswordContract.View{

    override var mPresenter: PasswordContract.Presenter = PasswordPresenter()

    override fun onBackPressed() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forgotpassword_activity)

        btnSend.setOnClickListener{
            loading()
            mPresenter.doRecoveryPassword(inputLayoutPassword.editText?.text.toString())
        }

        backButton.setOnClickListener{
            onBackPressed()
        }

    }

    override fun success(result: Any) {
        showMessage(result as String, DialogInterface.OnClickListener{
            _, _ ->
            finish()
        })
    }

    override fun showErrorUser(message: Int) {
        hideLoading()
        inputLayoutPassword?.error = resources.getString(message)
    }

    override fun showErrorPassword(message: Int) {}

}