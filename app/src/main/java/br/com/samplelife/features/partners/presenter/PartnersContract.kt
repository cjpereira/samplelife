package br.com.samplelife.features.partners.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView


object PartnersContract {

    interface View : BaseMvpView {
        fun updateStates(states: Any)
        fun updateCategories(categories: Any)
    }

    interface Presenter : BaseMvpPresenter<View> {
        fun fetchPartners(token: String)
        fun fetchPartners(token: String, state: String?, category:String?)
    }

}
