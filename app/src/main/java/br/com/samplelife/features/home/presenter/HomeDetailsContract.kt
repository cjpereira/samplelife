package br.com.samplelife.features.home.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView

object HomeDetailsContract {

    interface View : BaseMvpView

    interface Presenter : BaseMvpPresenter<View>

}
