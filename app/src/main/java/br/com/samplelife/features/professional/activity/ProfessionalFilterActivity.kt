package br.com.samplelife.features.professional.activity


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import br.com.samplelife.R
import br.com.samplelife.features.partners.adapter.StateAdapter
import br.com.samplelife.model.Generic
import br.com.samplelife.utils.Constants
import br.com.samplelife.view.DropDownAdapter
import br.com.samplelife.view.SpacesItemDecoration
import kotlinx.android.synthetic.main.professional_filter.*
import android.widget.TextView


class ProfessionalFilterActivity : AppCompatActivity() {

    private var stateFilter: ArrayList<Generic>? = null
    private var medicalSpecialtiesFilter: Generic? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.professional_filter)

        var medicalSpecialties = intent.getSerializableExtra(Constants.PARAM_CATEGORIES) as ArrayList<Generic>

        stateFilter = intent.getSerializableExtra(Constants.PARAM_STATE_FILTER) as ArrayList<Generic>?
        medicalSpecialtiesFilter = intent.getSerializableExtra(Constants.PARAM_CATEGORY_FILTER) as Generic?
        var strNameFilter = intent.getSerializableExtra(Constants.PARAM_NAME_FILTER) as String?

        if(strNameFilter!=null){
            nameFilter.setText(strNameFilter)
            nameFilter.setSelection(nameFilter.text.length)
        }

        val medicalAdapter = DropDownAdapter(this, medicalSpecialties)
        spMedicalSpecialties?.adapter = medicalAdapter

        medicalSpecialties.add(0, Generic("","","Escolha uma especialidade"))
        spMedicalSpecialties.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {

                if(adapterView.getChildAt(0) != null && (adapterView.getChildAt(0) as ViewGroup).getChildAt(0) !=null) {
                    ((adapterView.getChildAt(0) as ViewGroup).getChildAt(0) as TextView).setTextColor(ActivityCompat.getColor(this@ProfessionalFilterActivity, R.color.colorPrimary))

                    if(position >0){
                        medicalSpecialtiesFilter = medicalSpecialties?.get(position)
                    }else{
                        medicalSpecialtiesFilter = null
                    }
                }
            }
            override fun onNothingSelected(adapterView: AdapterView<*>) {}
        }

        if(medicalSpecialties!=null){
            for ((index,selected) in medicalSpecialties.withIndex()) {
                if (selected.name.equals(medicalSpecialtiesFilter?.name)) {
                    spMedicalSpecialties.setSelection(index)
                }
            }
        }

        var states = intent.getSerializableExtra(Constants.PARAM_STATES) as ArrayList<Generic>
        recyclerViewState.layoutManager = GridLayoutManager(this, 4)
        recyclerViewState.addItemDecoration(SpacesItemDecoration(10));

        var stateAdater = StateAdapter(states, this, stateFilter)
        recyclerViewState.adapter = stateAdater
        recyclerViewState.setHasFixedSize(true)

        btnConfirm.setOnClickListener {
            var stateAdapter = recyclerViewState.adapter as StateAdapter

            val resultIntent = Intent()
            resultIntent.putExtra(Constants.PARAM_STATE_FILTER, stateAdapter.filterSelected)
            resultIntent.putExtra(Constants.PARAM_CATEGORY_FILTER, medicalSpecialtiesFilter)
            resultIntent.putExtra(Constants.PARAM_NAME_FILTER, nameFilter.text.toString())

            setResult(Activity.RESULT_OK, resultIntent)

            finish()
        }

    }
}