package br.com.samplelife.features.home.activity

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import android.widget.Toast
import br.com.samplelife.R
import br.com.samplelife.application.UVApplication
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.articles.fragment.ArticlesFragment
import br.com.samplelife.features.calendar.activity.DetailStretchingActivity
import br.com.samplelife.features.calendar.fragment.CalendarFragment
import br.com.samplelife.features.chat.fragment.ChatFragment
import br.com.samplelife.features.explanation.activity.ExplanationActitivy
import br.com.samplelife.features.home.adapter.HomePagerViewAdapter
import br.com.samplelife.features.home.presenter.HomeContract
import br.com.samplelife.features.home.presenter.HomePresenter
import br.com.samplelife.features.login.activity.LoginActivity
import br.com.samplelife.features.professional.fragment.ProfessionalFragment
import br.com.samplelife.features.qrcode.activity.QRCodeScannerActivity
import br.com.samplelife.features.tutorial.activity.TutorialActivity
import br.com.samplelife.model.Login
import br.com.samplelife.preference.PreferenceHelper
import br.com.samplelife.utils.Constants
import br.com.samplelife.utils.ImageSaver
import br.com.samplelife.utils.Utils
import com.luseen.spacenavigation.SpaceItem
import com.luseen.spacenavigation.SpaceOnClickListener
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageOptions
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.home_activity.*
import org.apache.commons.lang3.text.WordUtils


class HomeActivity : BaseActivity<HomeContract.View, HomeContract.Presenter>(),
    HomeContract.View {

    private val REQUEST_LOGIN_MENU = 9129
    private val REQUEST_LOGIN_ITEM = 954
    private val REQUEST_LOGIN_QRCODE = 9130

    var selectItem: Int = R.id.home
    var activeItem: Int = 0
    var pageId: String? = null
    var fromUser:String? = null

    override fun onBackPressed() {

        if(drawer_layout.isDrawerOpen(Gravity.LEFT)) {
            drawer_layout.closeDrawer(Gravity.LEFT)
        }else if(viewPager.currentItem > 0){
            activeItem = 0
            bottomNavigation.changeCurrentItem(activeItem)
            viewPager.setCurrentItem(activeItem, false)
        } else{
            finish()
        }
    }

    override var mPresenter: HomeContract.Presenter = HomePresenter()

    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)


        if(intent.getSerializableExtra(Constants.PARAM_PAGEID)!=null){
            pageId = intent.getSerializableExtra(Constants.PARAM_PAGEID) as  String
        }

        if(intent.getSerializableExtra(Constants.PARAM_FROMUSER)!=null){
            fromUser = intent.getSerializableExtra(Constants.PARAM_FROMUSER) as  String
        }

        supportActionBar?.setDisplayShowTitleEnabled(false)

        btnLogin.setOnClickListener {
            drawer_layout.closeDrawer(Gravity.LEFT)
            startActivityForResult(Intent(this, LoginActivity::class.java),REQUEST_LOGIN_MENU)
        }

        btnExplanationLogin.setOnClickListener{
            drawer_layout.closeDrawer(Gravity.LEFT)
            startActivity(Intent(this, ExplanationActitivy::class.java))
        }

        btnExplanation.setOnClickListener{
            drawer_layout.closeDrawer(Gravity.LEFT)
            startActivity(Intent(this, ExplanationActitivy::class.java))
        }

        var viewPageAdapter = HomePagerViewAdapter(supportFragmentManager)
        viewPageAdapter.addFragment(ArticlesFragment())
        viewPageAdapter.addFragment(CalendarFragment())
        viewPageAdapter.addFragment(ProfessionalFragment())

        var chatFrament = ChatFragment()
        chatFrament.pushFromUser = fromUser
        viewPageAdapter.addFragment(chatFrament)

        fromUser = null
        viewPager.adapter = viewPageAdapter
        viewPager.offscreenPageLimit = 4

        bottomNavigation.showIconOnly()
        bottomNavigation.addSpaceItem(SpaceItem("Info Vida", R.drawable.tab_bar_info_vida))
        bottomNavigation.addSpaceItem(SpaceItem("Parceiros", R.drawable.tab_bar_partners))
        bottomNavigation.addSpaceItem(SpaceItem("Profissionais", R.drawable.tab_bar_professionals))
        bottomNavigation.addSpaceItem(SpaceItem("Chat", R.drawable.tab_bar_chat))

        bottomNavigation.setCentreButtonIcon(R.drawable.icone_tab_bar_scan)
        bottomNavigation.setActiveCentreButtonIconColor(ContextCompat.getColor(this, android.R.color.white))
        bottomNavigation.setInActiveCentreButtonIconColor(ContextCompat.getColor(this, android.R.color.white))

        bottomNavigation.setCentreButtonColor(ContextCompat.getColor(this, R.color.colorPrimary))
        bottomNavigation.setSpaceBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
        bottomNavigation.setActiveSpaceItemColor(ContextCompat.getColor(this, R.color.activeSpace))
        bottomNavigation.setInActiveSpaceItemColor(ContextCompat.getColor(this, R.color.inActiveSpace))

        btnExit.setOnClickListener{

            mPresenter.logout()
            (application as UVApplication).login = null

            PreferenceHelper(this).removeValue(Constants.PARAM_CPF)
            PreferenceHelper(this).removeValue(Constants.PARAM_PASSWORD)
            ((viewPager?.adapter as HomePagerViewAdapter).getFragment(3) as ChatFragment).stopChat()
            ((viewPager?.adapter as HomePagerViewAdapter).getFragment(1) as CalendarFragment).clearData()

            drawer_layout.closeDrawer(Gravity.LEFT)
            selectItem = 0
            activeItem = 0
            bottomNavigation.changeCurrentItem(activeItem)
            Toast.makeText(this@HomeActivity, "Usuário deslogado.", Toast.LENGTH_SHORT).show()
            showMenuPerfil()
            startActivityForResult(Intent(this@HomeActivity, LoginActivity::class.java), REQUEST_LOGIN_ITEM)
        }

        bottomNavigation.setSpaceOnClickListener(object : SpaceOnClickListener {
            override fun onCentreButtonClick() {
                if((application as UVApplication).login==null){
                    selectItem = -1
                    startActivityForResult(Intent(this@HomeActivity, LoginActivity::class.java), REQUEST_LOGIN_QRCODE)
                }else{
                    var intent = Intent(this@HomeActivity, QRCodeScannerActivity::class.java)
                    startActivity(intent)
                }
            }

            override fun onItemClick(itemIndex: Int, itemName: String) {

                when (itemIndex) {
                    0 -> {
                            activeItem = itemIndex
                            viewPager.setCurrentItem(activeItem, false)
                    }

                   1 -> {

                        if((application as UVApplication).login==null){
                            selectItem = 1
                            startActivityForResult(Intent(this@HomeActivity, LoginActivity::class.java), REQUEST_LOGIN_ITEM)
                        }else{
                            activeItem = itemIndex
                            viewPager.setCurrentItem(activeItem, false)
                        }

                    }

                   2 -> {
                       if( (application as UVApplication).login==null){
                           selectItem = 2
                           startActivityForResult(Intent(this@HomeActivity, LoginActivity::class.java), REQUEST_LOGIN_ITEM)
                       }else{
                           activeItem = itemIndex
                           viewPager.setCurrentItem(activeItem, false)
                       }

                   }

                    3 -> {
                        if((application as UVApplication).login==null){
                            selectItem = 3
                            startActivityForResult(Intent(this@HomeActivity, LoginActivity::class.java), REQUEST_LOGIN_ITEM)
                        }else{
                            activeItem = itemIndex
                            viewPager.setCurrentItem(activeItem, false)
                        }
                    }
                }
            }
            override fun onItemReselected(itemIndex: Int, itemName: String) {
            }
        })

        if(PreferenceHelper(this).getValueBoolien(Constants.PARAM_SHOW_TUTORIAL, true)){

            val handler = Handler()

            handler.postDelayed({
                startActivity(Intent(this, TutorialActivity::class.java))
            }, 2000)

        }


        drawer_layout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                showMenuPerfil()
            }

            override fun onDrawerOpened(drawerView: View) {
                showMenuPerfil()
            }

            override fun onDrawerClosed(drawerView: View) {
            }

            override fun onDrawerStateChanged(newState: Int) {
                showMenuPerfil()
            }

        })

        photoImageView.setOnClickListener{
            if (CropImage.isExplicitCameraPermissionRequired(this)) {
                requestPermissions(
                    arrayOf(Manifest.permission.CAMERA),
                    CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE
                )
            } else {
                CropImage.startPickImageActivity(this)
            }
        }

        if((application as UVApplication).login!=null) {

            when {
                pageId.equals(Constants.PARAM_INFOVIDA, true) -> bottomNavigation.changeCurrentItem(0)
                pageId.equals(Constants.PARAM_CHAT, true) -> bottomNavigation.changeCurrentItem(3)
                pageId.equals(Constants.PARAM_AGENDA, true) -> bottomNavigation.changeCurrentItem(1)
                pageId.equals(Constants.PARAM_ALONGAMENTO, true) -> {
                    bottomNavigation.changeCurrentItem(1)

                    var intent = Intent(this@HomeActivity, DetailStretchingActivity::class.java)
                    if(pageId!=null && pageId!!.isNotEmpty()){
                        intent.putExtra(Constants.PARAM_PAGEID, pageId)
                    }
                    startActivity(intent)
                }
                pageId.equals(Constants.PARAM_BEBERAGUA, true) -> {
                    bottomNavigation.changeCurrentItem(1)

                    var intent = Intent(this@HomeActivity, DetailStretchingActivity::class.java)
                    if(pageId!=null && pageId!!.isNotEmpty()){
                        intent.putExtra(Constants.PARAM_PAGEID, pageId)
                    }
                    startActivity(intent)
                }
            }

        }else{
            selectItem = 0
            activeItem = 0
            startActivityForResult(Intent(this@HomeActivity, LoginActivity::class.java), REQUEST_LOGIN_ITEM)
        }

    }

    override fun onResume() {
        super.onResume()

        try {
            var fileName: String? = ""
            var image: Bitmap? = null

            fileName = (application as UVApplication).login?.user?.cpf

            if (fileName != null) {
                if (!fileName!!.isEmpty()) {
                    //Busca a imagem armazenada na pasta privada do aplicativo.
                    image = ImageSaver(getContext()).setFileName("$fileName.jpg").setDirectoryName("images").load()
                    if (image!!.byteCount > 0) {
                        val bit = Bitmap.createScaledBitmap(
                            image!!,
                            Utils.dpToPx(getContext(), 52).toInt(),
                            Utils.dpToPx(getContext(), 52).toInt(),
                            false
                        )

                        val roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(resources, bit)
                        roundedBitmapDrawable.cornerRadius = Utils.dpToPx(getContext(), 10)
                        roundedBitmapDrawable.setAntiAlias(true)

                        photoImageView.setImageDrawable(roundedBitmapDrawable)

                    }
                }
            }
        } catch (e: Exception) {
            e.message.toString()
        }

    }

    private fun showMenuPerfil(){
        if((application as UVApplication).login!=null){
            layoutLogged.visibility = View.VISIBLE
            layoutNoLogged.visibility = View.INVISIBLE
            txtNamePerfil.text = WordUtils.capitalizeFully((application as UVApplication).login?.user?.name)
            txtProfile.text = WordUtils.capitalizeFully((application as UVApplication).login?.user?.profile)
        }else{
            layoutLogged.visibility = View.INVISIBLE
            layoutNoLogged.visibility = View.VISIBLE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_LOGIN_ITEM && resultCode == Activity.RESULT_CANCELED) {
            finish()
        }else  if (requestCode == REQUEST_LOGIN_ITEM && resultCode == Activity.RESULT_OK){
            (application as UVApplication).login = data?.getSerializableExtra(Constants.PARAM_LOGIN) as Login
            showMenuPerfil()
            activeItem = selectItem
            bottomNavigation.changeCurrentItem(activeItem)
            viewPager.setCurrentItem(activeItem, false)
            when (activeItem) {

                1 -> {
                    ((viewPager?.adapter as HomePagerViewAdapter).getFragment(activeItem) as CalendarFragment).loadData()
                }
                3 -> {
                    ((viewPager?.adapter as HomePagerViewAdapter).getFragment(activeItem) as ChatFragment).startChat()
                }
            }

            Toast.makeText(this@HomeActivity, "Usuário logado.", Toast.LENGTH_SHORT).show()
        }else if(requestCode == REQUEST_LOGIN_ITEM){
            bottomNavigation.changeCurrentItem(activeItem)
        }else if(requestCode ==REQUEST_LOGIN_QRCODE && resultCode == Activity.RESULT_OK){
            (application as UVApplication).login = data?.getSerializableExtra(Constants.PARAM_LOGIN) as Login
            var intent = Intent(this@HomeActivity, QRCodeScannerActivity::class.java)
            startActivity(intent)
            Toast.makeText(this@HomeActivity, "Usuário logado.", Toast.LENGTH_SHORT).show()
        }else if(requestCode ==REQUEST_LOGIN_MENU && resultCode == Activity.RESULT_OK){
            (application as UVApplication).login = data?.getSerializableExtra(Constants.PARAM_LOGIN) as Login
            Toast.makeText(this@HomeActivity, "Usuário logado.", Toast.LENGTH_SHORT).show()
            showMenuPerfil()
        } else if (requestCode === CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode === AppCompatActivity.RESULT_OK) {
            val imageUri = CropImage.getPickImageResultUri(this, data)
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                val permissions = ArrayList<String>()
                if (ActivityCompat.checkSelfPermission(
                        this,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    permissions.add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                }
                ActivityCompat.requestPermissions(
                    this,
                    permissions.toTypedArray(), CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE
                )
            } else {
                val mOptions = CropImageOptions()
                mOptions.aspectRatioX = 3
                mOptions.aspectRatioY = 3
                mOptions.fixAspectRatio = true

                val intent = Intent()
                intent.setClass(this, CropImageActivity::class.java!!)
                val bundle = Bundle()
                bundle.putParcelable(CropImage.CROP_IMAGE_EXTRA_SOURCE, imageUri)
                bundle.putParcelable(CropImage.CROP_IMAGE_EXTRA_OPTIONS, mOptions)
                intent.putExtra(CropImage.CROP_IMAGE_EXTRA_BUNDLE, bundle)
                startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
            }
        } else if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)

            if (resultCode === Activity.RESULT_OK) {
                val resultUri = result.uri

                try {
                    var fileName = (application as UVApplication).login?.user?.cpf
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, resultUri)
                    ImageSaver(this).setFileName("$fileName.jpg").setDirectoryName("images").save(bitmap)
                } catch (e: Exception) {
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

}