package br.com.samplelife.features.calendar.activity

import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.samplelife.R
import br.com.samplelife.application.UVApplication
import br.com.samplelife.utils.Constants
import kotlinx.android.synthetic.main.detail_stretching.*


class DetailStretchingActivity: AppCompatActivity(){

    var pageId:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_stretching)

        backButton.setOnClickListener {
            onBackPressed()
        }

        if(intent.getSerializableExtra(Constants.PARAM_PAGEID)!=null){
            pageId = intent.getSerializableExtra(Constants.PARAM_PAGEID) as  String
        }


        if(pageId.equals(Constants.PARAM_BEBERAGUA, true)) {

            imageView6.setOnClickListener{

                val url = "https://samplelife.com.br/publicacao/8-importancia-beber-agua/${(application as UVApplication).login?.token}"

                try {
                    val i = Intent("android.intent.action.MAIN")
                    i.component =
                        ComponentName.unflattenFromString("com.android.chrome/com.android.chrome.Main")
                    i.addCategory("android.intent.category.LAUNCHER")
                    i.data = Uri.parse(url)
                    startActivity(i)
                } catch (e: ActivityNotFoundException) {
                    // Chrome is not installed
                    val i = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    startActivity(i)
                }
            }

            imageView6.setImageResource(R.drawable.drinkwater)
        }else{
            imageView6.setOnClickListener{

                val url = "https://samplelife.com.br/publicacao/7-lista-alongamentos/${(application as UVApplication).login?.token}"

                try {
                    val i = Intent("android.intent.action.MAIN")
                    i.component =
                        ComponentName.unflattenFromString("com.android.chrome/com.android.chrome.Main")
                    i.addCategory("android.intent.category.LAUNCHER")
                    i.data = Uri.parse(url)
                    startActivity(i)
                } catch (e: ActivityNotFoundException) {
                    // Chrome is not installed
                    val i = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    startActivity(i)
                }
            }

            imageView6.setImageResource(R.drawable.stretching)
        }

    }
}
