package br.com.samplelife.features.qrcode.service

import br.com.samplelife.api.ApiException
import br.com.samplelife.api.ApiManager
import br.com.samplelife.model.Partner
import br.com.samplelife.model.User
import br.com.samplelife.payload.QRCodeResponse
import br.com.samplelife.utils.Constants
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Response
import rx.Observable

class QRCodeService {

    fun fetchPartners(user: User, qrCode: String): Observable<Partner>{

        return Observable.create { emitter ->

            val service = ApiManager.createRetrofit(IQRCodeService::class.java)
            try {
                var response: Response<QRCodeResponse> = service.fetchPartners(qrCode, user.idProfile).execute()
                if (response.errorBody() == null) {
                    var qrCodeResponse: QRCodeResponse = response.body()!!
                    emitter.onNext(qrCodeResponse.partners?.toParner())
                    emitter.onCompleted()
                } else {
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if (errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        emitter.onError(ApiException(Constants.MSG_QRCODE_NOT_FOUND))
                    } else {
                        throw Exception()
                    }
                }

            } catch (e: Exception) {
                emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
            }

        }
    }

}