package br.com.samplelife.features.qrcode.activity

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.os.Vibrator
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.widget.Toast
import br.com.samplelife.R
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.partners.activity.PartnersActivity
import br.com.samplelife.features.partners.activity.QRCodeActivity
import br.com.samplelife.features.qrcode.presenter.QRCodeContract
import br.com.samplelife.features.qrcode.presenter.QRCodePresenter
import br.com.samplelife.model.Partner
import br.com.samplelife.utils.Constants
import br.com.samplelife.utils.Utils
import com.google.zxing.Result
import kotlinx.android.synthetic.main.qrcode_activity.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

class QRCodeScannerActivity : BaseActivity<QRCodeContract.View, QRCodeContract.Presenter>(),
    QRCodeContract.View, ZXingScannerView.ResultHandler {

    override var mPresenter: QRCodeContract.Presenter = QRCodePresenter()

    private val ZXING_CAMERA_PERMISSION = 1000
    private var vibrator: Vibrator? = null

    private var mScannerView: ZXingScannerView? = null

    override fun onBackPressed() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.qrcode_activity)

        mScannerView = ZXingScannerView(this)
        mScannerView?.setSquareViewFinder(true)
        mScannerView?.setBorderColor(Color.WHITE)
        mScannerView?.setBorderStrokeWidth(Utils.dpToPx(this, 10).toInt())
        mScannerView?.setBorderCornerRadius(Utils.dpToPx(this, 5).toInt())
        mScannerView?.setBorderLineLength(Utils.dpToPx(this, 60).toInt())

        content_frame.addView(mScannerView)
        mScannerView?.setMaskColor(ActivityCompat.getColor(this, R.color.maskqrcode))

        backButton.setOnClickListener{
            finish()
        }

        btnWebSite.setOnClickListener{
            var intent = Intent(this, PartnersActivity::class.java)
            intent.putExtra(Constants.PARAM_QRCODE, true)
            startActivity(intent)
        }
    }

    override fun handleResult(rawResult: Result?) {

        if (rawResult?.text != null) {
            startVibrate()
            loading(Constants.TITLE_VALIDATING)
            mPresenter.validateQRCode(rawResult?.text)
        }
    }

    override fun onResume() {
        super.onResume()

        if (ContextCompat.checkSelfPermission(
                this@QRCodeScannerActivity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@QRCodeScannerActivity,
                arrayOf(Manifest.permission.CAMERA), ZXING_CAMERA_PERMISSION
            )
        } else {
            mScannerView?.setResultHandler(this)
            mScannerView?.startCamera()

        }
    }

    override fun onPause() {
        super.onPause()
        mScannerView?.stopCamera()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            ZXING_CAMERA_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mScannerView?.setResultHandler(this)
                    mScannerView?.startCamera()
                } else {
                    Toast.makeText(this, "Por favor conceda permissão da camera para usar o scanner de QR Scanner", Toast.LENGTH_SHORT)
                        .show()
                }
                return
            }
        }
    }

    private fun startVibrate() {

        vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        val dot = 200          // Length of a Morse Code "dot" in milliseconds
        val dash = 500         // Length of a Morse Code "dash" in milliseconds
        val short_gap = 200    // Length of Gap Between dots/dashes
        val medium_gap = 500   // Length of Gap Between Letters
        val long_gap = 1000    // Length of Gap Between Words
        val pattern = longArrayOf(
            0, // Start immediately
            dot.toLong(),
            short_gap.toLong()
        )
        vibrator?.vibrate(pattern, -1)
    }

    override fun success(result: Any) {
        hideLoading()
        var intent = Intent(this, QRCodeActivity::class.java)
        intent.putExtra(Constants.PARAM_PARTNERS, result as Partner)
        startActivity(intent)
        finish()
    }

    override fun showMessage(error: String?) {
        showMessage(error as String, DialogInterface.OnClickListener{
                _, _ ->
            mScannerView?.resumeCameraPreview(this)
        })
    }

}