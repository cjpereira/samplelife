package br.com.samplelife.features.calendar.service

import br.com.samplelife.BuildConfig
import br.com.samplelife.api.ApiException
import br.com.samplelife.api.ApiManager
import br.com.samplelife.model.Event
import br.com.samplelife.model.Generic
import br.com.samplelife.payload.EventPayload
import br.com.samplelife.payload.GenericPayload
import br.com.samplelife.utils.Constants
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Response
import rx.Observable
import java.text.SimpleDateFormat
import java.util.*


class CalendarService {


    fun fetchEvents(id: String?): Observable<ArrayList<Event>> {

        return Observable.create { emitter ->
            val service = ApiManager.createRetrofit(ICalendarService::class.java)
            try {

                var response: Response<ArrayList<EventPayload>> = service.fetchEvents(id).execute()

                if (response.errorBody() == null) {
                    var responseEvents: ArrayList<EventPayload> = response.body()!!

                    var events = ArrayList<Event>()

                    for (item in responseEvents) {
                        events.add(item.toEvent())
                    }

                    events.sortWith(Comparator { tk1, tk2 -> tk1.date!!.compareTo(tk2.date) })

                    emitter.onNext(events)
                    emitter.onCompleted()

                } else {

                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()

                    if (response.code() == 404) {
                        throw ApiException(Constants.MSG_NOT_FOUND)
                    }

                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if (errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        throw ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString())
                    } else {
                        throw Exception()
                    }
                }

            } catch (e: Exception) {
                if (e is ApiException) {
                    emitter.onError(e)
                } else {
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }
        }
    }

    fun fetchCategory(): Observable<ArrayList<Generic>> {

        return Observable.create { emitter ->

            val service = ApiManager.createRetrofit(ICalendarService::class.java)

            try {

                var response: Response<ArrayList<GenericPayload>> = service.fetchCategory().execute()

                if (response.errorBody() == null){
                    var responseCategories: ArrayList<GenericPayload> = response.body()!!

                    var categories = ArrayList<Generic>()

                    for (item in responseCategories){
                        var category = item.toGeneric()
                        categories.add(category)
                    }

                    emitter.onNext(categories)
                    emitter.onCompleted()

                }else{
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if(errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        emitter.onError(ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString()))
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
            }

        }
    }



    fun removeCalendar(idCalendar:String, idPerfil:String ): Observable<Boolean> {

        return Observable.create { emitter ->

            val service = ApiManager.createRetrofit(ICalendarService::class.java)

            try {

                var response: Response<JsonObject> = service.removeCalendar(idCalendar, idPerfil).execute()

                if (response.errorBody() == null){
                    var response: JsonObject = response.body()!!
                    emitter.onNext(true)
                    emitter.onCompleted()

                }else{
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if(errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        emitter.onError(ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString()))
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
            }

        }
    }

    fun sendCalendar(id:String?, name:String, category:String, userId:String, date: Calendar, description:String): Observable<String> {

        return Observable.create { emitter ->


            try {


                val formatHour = SimpleDateFormat("HH:mm")
                val format1 = SimpleDateFormat("dd/MM/yyyy")

                var categoryPayload = JsonObject()
                categoryPayload.addProperty("id", category)

                var userPayload = JsonObject()
                userPayload.addProperty("id", userId)

                var calendarPayload = JsonObject()
                calendarPayload.addProperty("atividade", name)
                calendarPayload.addProperty("descricao", description)
                calendarPayload.addProperty("data", format1.format(date.time))
                calendarPayload.addProperty("hora", formatHour.format(date.time))
                calendarPayload.add("categoria", categoryPayload)
                calendarPayload.add("colaborador", userPayload)


                if(id!=null && !id.isEmpty()){
                    calendarPayload.addProperty("id", id)
                }

                val service = ApiManager.createRetrofit(ICalendarService::class.java)
                try {

                    var response: Response<JsonObject> = service.sendCalendar(calendarPayload).execute()

                    if (response.errorBody() == null){

                        //Atualiza pushid
                        var responseLogin: JsonObject? = response.body()

                        if(responseLogin?.has(Constants.PARAM_MESSAGE)!!) {
                            emitter.onNext(responseLogin?.get(Constants.PARAM_MESSAGE)?.asString)
                        }else{
                            emitter.onNext("Agenda salva com sucesso")
                        }
                        emitter.onCompleted()
                    }else{
                        val errorResponse = response.errorBody()
                        val returnError = errorResponse!!.string()
                        val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)

                        if(errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                            emitter.onError(ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString()))
                        }else{
                            throw Exception()
                        }
                    }

                }catch (e: Exception){
                    if(e is ApiException){
                        emitter.onError(e)
                    }else{
                        emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                    }
                }

            }catch (e: Exception){
                emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
            }

        }
    }

}