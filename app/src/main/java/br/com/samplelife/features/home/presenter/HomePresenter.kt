package br.com.samplelife.features.home.presenter

import br.com.samplelife.base.BaseMvpPresenterImpl
import br.com.samplelife.features.login.service.LoginService
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

open class HomePresenter: BaseMvpPresenterImpl<HomeContract.View>(),
    HomeContract.Presenter {


    override fun logout(){

        LoginService().logout()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({}, {}
            )
    }
}