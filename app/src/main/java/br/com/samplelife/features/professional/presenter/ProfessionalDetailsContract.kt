package br.com.samplelife.features.professional.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView

object ProfessionalDetailsContract {

    interface View : BaseMvpView

    interface Presenter : BaseMvpPresenter<View>{
        fun registerAccessProfessional()
    }

}