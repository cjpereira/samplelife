package br.com.samplelife.features.chat.service

import br.com.samplelife.api.ApiException
import br.com.samplelife.api.ApiManager
import br.com.samplelife.model.Message
import br.com.samplelife.model.User
import br.com.samplelife.payload.MessageResponsePayload
import br.com.samplelife.payload.UserResponsePayload
import br.com.samplelife.utils.Constants
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Response
import rx.Observable
import java.util.*


class ChatService {


    fun fetchMessage(fromUser: User, toUser: User): Observable<ArrayList<Message>> {

        return Observable.create { emitter ->

            val service = ApiManager.createRetrofit(IChatService::class.java)
            try {

                var jsonRequest = JsonObject()
                jsonRequest.addProperty("fromUserId", fromUser.id)
                jsonRequest.addProperty("toUserId", toUser.id)

                var response: Response<MessageResponsePayload> = service.fetchMessages(jsonRequest).execute()

                if (response.errorBody() == null) {
                    var messageResponsePayload: MessageResponsePayload? = response.body()

                    var messages = ArrayList<Message>()

                    for (messagePayload in messageResponsePayload?.messages!!) {
                        messages.add(messagePayload.toMessage())
                    }

                    emitter.onNext(messages)
                    emitter.onCompleted()
                } else {
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)

                    val response = Gson().fromJson<UserResponsePayload>(
                        errorJson.get("retorno").toString(),
                        UserResponsePayload::class.java
                    )

                    if (response?.message != null) {
                        throw ApiException(response?.message!!)
                    } else {
                        throw Exception()
                    }
                }

            } catch (e: Exception) {
                if(e is ApiException){
                    emitter.onError(e)
                }else{
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }
        }
    }

    fun registerAccessProfessionalChat(id: String?): Observable<Boolean> {
        return Observable.create { emitter ->
            val service = ApiManager.createRetrofit(IRegisterChat::class.java)
            try {
                service.registerAccessProfessionalChat(id).execute()
                emitter.onNext(true)
                emitter.onCompleted()
            } catch (e: Exception) {
                emitter.onNext(false)
                emitter.onCompleted()
            }
        }
    }

}