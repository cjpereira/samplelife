package br.com.samplelife.features.chat.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.samplelife.R
import br.com.samplelife.model.Message
import br.com.samplelife.model.User
import io.reactivex.annotations.NonNull
import kotlinx.android.synthetic.main.item_message_header.view.*
import kotlinx.android.synthetic.main.item_message_received.view.*
import java.text.SimpleDateFormat
import java.util.*


class MessageAdapter(private val context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_MESSAGE_SENT = 1
    private val VIEW_TYPE_MESSAGE_RECEIVED = 2
    private val VIEW_TYPE_HEADER = 3

    var messagens = ArrayList<Message>()
    var toUser: User? = null
    var fromUser: User? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType === VIEW_TYPE_MESSAGE_SENT) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_message_sent, parent, false)
            SentMessageHolder(view)
        } else if (viewType === VIEW_TYPE_MESSAGE_RECEIVED){
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_message_received, parent, false)
            ReceivedMessageHolder(view)
        }else{
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_message_header, parent, false)
            HeaderMessageHolder(view)
        }
    }

    // Determines the appropriate ViewType according to the sender of the message.
    override fun getItemViewType(position: Int): Int {

        return if(messagens[position].isHeader!!){
            VIEW_TYPE_HEADER
        } else if (messagens[position].fromUserId.equals(fromUser?.id)) {
            // If the current user is the sender of the message
            VIEW_TYPE_MESSAGE_SENT
        } else {
            // If some other user sent the message
            VIEW_TYPE_MESSAGE_RECEIVED
        }
    }

    override fun getItemCount(): Int {
        return messagens.size
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val message = messagens[position]

        when (viewHolder.itemViewType) {
            VIEW_TYPE_MESSAGE_SENT -> (viewHolder as SentMessageHolder).bind(message)
            VIEW_TYPE_MESSAGE_RECEIVED -> (viewHolder as ReceivedMessageHolder).bind(message)
            VIEW_TYPE_HEADER -> (viewHolder as HeaderMessageHolder).bind(message)

        }
    }

    internal inner class ReceivedMessageHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(message: Message) {
            itemView.text_message_body.text = message.message
            val format = SimpleDateFormat("HH:mm")
            itemView.text_message_time.text = format.format(message.date)
        }
    }

    internal inner class SentMessageHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(message: Message) {
            itemView.text_message_body.text = message.message

            val format = SimpleDateFormat("HH:mm")
            itemView.text_message_time.text = format.format(message.date)
        }
    }

    internal inner class HeaderMessageHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(message: Message) {
            itemView.txt_header.text = message.message
        }
    }

}