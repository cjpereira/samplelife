package br.com.samplelife.features.professional.presenter

import br.com.samplelife.application.UVApplication
import br.com.samplelife.base.BaseMvpPresenterImpl
import br.com.samplelife.features.professional.service.ProfessionalService
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

open class ProfessionalDetailsPresenter: BaseMvpPresenterImpl<ProfessionalDetailsContract.View>(),
    ProfessionalDetailsContract.Presenter {

    override fun registerAccessProfessional() {
        ProfessionalService().registerAccessProfessional((mView?.getContext()?.applicationContext as UVApplication).login?.user?.idProfile)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
            }, { })
    }
}