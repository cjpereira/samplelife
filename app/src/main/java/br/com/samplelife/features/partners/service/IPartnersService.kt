package br.com.samplelife.features.partners.service

import br.com.samplelife.BuildConfig
import br.com.samplelife.api.URLBase
import br.com.samplelife.payload.GenericPayload
import br.com.samplelife.payload.PartnersResponse
import br.com.samplelife.payload.StateResponse
import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

@URLBase(BuildConfig.BASE_URL)
interface IPartnersService {

    @POST("api/parceiro/listar")
    fun fetchPartners(@Header("token") token: String,
                      @Query("uf") uf: String?,
                      @Query("categorias") category: String?) : Call<PartnersResponse>

    @POST("api/parceiro/uf/listar")
    fun fetchState(@Header("token") token: String) : Call<StateResponse>

    @POST("api/categoria-parceiro/listar")
    fun fetchCategory(@Header("token") token: String) : Call<ArrayList<GenericPayload>>
}