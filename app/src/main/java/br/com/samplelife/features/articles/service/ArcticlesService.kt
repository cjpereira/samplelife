package br.com.samplelife.features.articles.service

import br.com.samplelife.api.ApiException
import br.com.samplelife.api.ApiManager
import br.com.samplelife.model.Articles
import br.com.samplelife.model.Generic
import br.com.samplelife.payload.ArticlePayload
import br.com.samplelife.payload.GenericPayload
import br.com.samplelife.utils.Constants
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Response
import rx.Observable

class ArcticlesService {

    fun fetchNews(category:String?): Observable<ArrayList<Articles>> {

        return Observable.create { emitter ->
            val service = ApiManager.createRetrofit(IArcticlesService::class.java)
            try {

                var response: Response<ArrayList<ArticlePayload>> = service.fetchNews(category).execute()

                if (response.errorBody() == null){
                    var responseNews: ArrayList<ArticlePayload> = response.body()!!

                    var hArticles = ArrayList<Articles>()

                    for (item in responseNews){
                        hArticles.add(item.toArticle())
                    }
                    hArticles.sortByDescending{it.date}
                    emitter.onNext(hArticles)
                    emitter.onCompleted()

                }else{

                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()

                    if (response.code() == 404){
                        throw ApiException(Constants.MSG_NOT_FOUND)
                    }

                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if(errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        throw ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString())
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                if(e is ApiException){
                    emitter.onError(e)
                }else{
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }
        }
    }


    fun fetchCategory(): Observable<ArrayList<Generic>> {

        return Observable.create { emitter ->

            val service = ApiManager.createRetrofit(IArcticlesService::class.java)

            try {

                var response: Response<ArrayList<GenericPayload>> = service.fetchCategory().execute()

                if (response.errorBody() == null){
                    var responseCategories: ArrayList<GenericPayload> = response.body()!!

                    var categories = ArrayList<Generic>()

                    for (item in responseCategories){
                        categories.add(item.toGeneric())
                    }

                    emitter.onNext(categories)
                    emitter.onCompleted()

                }else{
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if(errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        emitter.onError(ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString()))
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                if(e is ApiException){
                    emitter.onError(e)
                }else{
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }

        }
    }

    fun registerAccessProfessionalChat(id: String?): Observable<Boolean> {
        return Observable.create { emitter ->
            val service = ApiManager.createRetrofit(IArcticlesService::class.java)
            try {
                service.registerAccessInfoVida(id).execute()
                emitter.onNext(true)
                emitter.onCompleted()
            } catch (e: Exception) {
                emitter.onNext(false)
                emitter.onCompleted()
            }
        }
    }
}