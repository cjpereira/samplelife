package br.com.samplelife.features.calendar.fragment

import android.content.Intent
import android.os.Bundle
import android.text.format.DateUtils
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.samplelife.R
import br.com.samplelife.application.UVApplication
import br.com.samplelife.base.BaseFragment
import br.com.samplelife.features.calendar.activity.AddCalendarActivity
import br.com.samplelife.features.calendar.activity.DetailStretchingActivity
import br.com.samplelife.features.calendar.adapter.CalendarAdapter
import br.com.samplelife.features.calendar.presenter.CalendarContract
import br.com.samplelife.features.calendar.presenter.CalendarPresenter
import br.com.samplelife.features.home.activity.HomeActivity
import br.com.samplelife.model.Event
import br.com.samplelife.model.Generic
import br.com.samplelife.utils.Constants
import kotlinx.android.synthetic.main.calendar_fragment.*
import kotlinx.android.synthetic.main.calendar_fragment.view.*
import kotlinx.android.synthetic.main.home_activity.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CalendarFragment : BaseFragment<CalendarContract.View, CalendarContract.Presenter>(),
    CalendarContract.View {

    var event: Event? = null

    override var mPresenter: CalendarContract.Presenter =
        CalendarPresenter()


    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.calendar_fragment, container, false)

        val recyclerView = view.recyclerView

        recyclerView.apply {
            // set a LinearLayoutManager to handle Android
            // RecyclerView behavior
            layoutManager = LinearLayoutManager(activity)
            // set the custom adapter to the RecyclerView
            adapter = CalendarAdapter(ArrayList(), this.context!!)
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (recyclerView.canScrollVertically(-1)) {
                    shadowTopView.visibility = View.VISIBLE
                }else{
                    shadowTopView.visibility = View.GONE
                }
            }
        })

        view.btnAddCalendar.setOnClickListener{
            (activity as HomeActivity).loading()
            mPresenter.fetchSpecialisties()
        }

        view.toogleMenu.setOnClickListener{
            (activity as HomeActivity).drawer_layout.openDrawer(Gravity.LEFT)
        }

        view.btnTryAgain.setOnClickListener {
            txtNotFound.visibility = View.GONE
            btnTryAgain.visibility = View.GONE
            mPresenter.fetchEvents((context.applicationContext as UVApplication).login?.user?.idProfile)
        }

        if((context.applicationContext as UVApplication).login!=null){
            loadData()
        }

        (recyclerView.adapter as CalendarAdapter).getItemClickSignal().subscribe{

            if(it.name.equals("Alongamento", true)){
                var intent = Intent(activity, DetailStretchingActivity::class.java)
                intent.putExtra(Constants.PARAM_PAGEID, Constants.PARAM_ALONGAMENTO)
                activity?.startActivity(intent)

            }else if(it.name.equals("Beber Água", true) || it.name.equals("Beber Agua", true)){
                var intent = Intent(activity, DetailStretchingActivity::class.java)
                intent.putExtra(Constants.PARAM_PAGEID, Constants.PARAM_BEBERAGUA)
                activity?.startActivity(intent)

            }else{
                event = it
                (activity as HomeActivity).loading()
                mPresenter.fetchSpecialisties()
            }
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    fun clearData(){
        (recyclerView.adapter as CalendarAdapter).calendarList = ArrayList()
    }

    fun loadData() {
        val app = activity?.application as UVApplication

        if(app.login !=null){
            mPresenter.fetchEvents((context.applicationContext as UVApplication).login?.user?.idProfile)
        }
    }

    override fun successProfessional(result: Any) {
        (activity as HomeActivity).hideLoading()
        var intent = Intent(activity, AddCalendarActivity::class.java)
        intent.putExtra(Constants.PARAM_CATEGORIES, result as ArrayList<Generic>)

        if(event!=null){
            intent.putExtra(Constants.PARAM_EVENT, event)
        }
        activity!!.startActivity(intent)
        event = null
    }

    override fun success(result: Any) {

        btnTryAgain.visibility = View.GONE
        txtNotFound.visibility = View.GONE

        if((result as ArrayList<Event>).size > 0){
            groupDataIntoHashMap(result)
        } else{
            txtNotFound.visibility = View.VISIBLE
            txtNotFound.text = context.getString(R.string.message_event_empty)

            (recyclerView.adapter as CalendarAdapter).calendarList = ArrayList()
            (recyclerView.adapter as CalendarAdapter).notifyDataSetChanged()


        }
    }

    override fun showMessage(error: String?) {
        (activity as HomeActivity).hideLoading()
        txtNotFound.visibility = View.VISIBLE
        txtNotFound.text = error
        btnTryAgain.visibility = View.VISIBLE
    }

    private fun groupDataIntoHashMap(eventModelList: ArrayList<Event>) {

        val groupedHashMap = LinkedHashMap<String, ArrayList<Event>>()

        var list: ArrayList<Event>? = null

        for (event in eventModelList) {
            val format = SimpleDateFormat("dd/MM/yyyy")

            val hashMapKey = format.format(event.date)
            if (groupedHashMap.containsKey(hashMapKey)) {
                groupedHashMap.get(hashMapKey)!!.add(event)
            } else {
                list = ArrayList()
                list!!.add(event)
                groupedHashMap.put(hashMapKey, list)
            }
        }
        generateListFromMap(groupedHashMap)
    }

    private fun generateListFromMap(groupedHashMap: LinkedHashMap<String, ArrayList<Event>>){

        val consolidatedList = ArrayList<Event>()

        for (date in groupedHashMap.keys) {
            var agroup = groupedHashMap[date]!!
            agroup[0].isHeader = true
            consolidatedList.addAll(groupedHashMap[date]!!)
        }

        var scrollPosition = true
        if((recyclerView.adapter as CalendarAdapter).calendarList.size > 0){
            scrollPosition = false
        }

        (recyclerView.adapter as CalendarAdapter).calendarList = consolidatedList
        (recyclerView.adapter as CalendarAdapter).notifyDataSetChanged()

        if(scrollPosition){

            var position = consolidatedList.size - 1

            for((index, event) in consolidatedList.withIndex()){
                if(event.isHeader && (DateUtils.isToday(event.date?.time!!) || event.date?.time!! > Date().time) ){
                    position = index
                    break
                }
            }

            recyclerView.scrollToPosition(position)
        }

    }

    override fun showErrorDescription(message: String) {}

    override fun showErroCategory(message: String) {}

    override fun showErroDate(message: String) {}

}