package br.com.samplelife.features.chat.service

import br.com.samplelife.BuildConfig
import br.com.samplelife.api.URLBase
import br.com.samplelife.payload.MessageResponsePayload
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

@URLBase(BuildConfig.SOCKET_URL)
interface IChatService {

    @POST("getMessages")
    fun fetchMessages(@Body jsonObject: JsonObject): Call<MessageResponsePayload>


    @POST("addPushId")
    fun addPushId(@Body jsonObject: JsonObject): Call<JsonObject>

    @POST("logout")
    fun logout(@Body jsonObject: JsonObject): Call<JsonObject>

}