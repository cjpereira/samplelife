package br.com.samplelife.features.qrcode.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView

object QRCodeContract {

    interface View : BaseMvpView

    interface Presenter : BaseMvpPresenter<View> {
        fun validateQRCode(qrcode: String)
    }

}
