package br.com.samplelife.features.articles.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.samplelife.R
import br.com.samplelife.base.BaseFragment
import br.com.samplelife.features.articles.activity.ArticlesActivity
import br.com.samplelife.features.articles.activity.ArticlesFilterActivity
import br.com.samplelife.features.articles.adapter.ArticlesAdapter
import br.com.samplelife.features.articles.presenter.ArticlesContract
import br.com.samplelife.features.articles.presenter.ArticlesPresenter
import br.com.samplelife.features.home.activity.HomeActivity
import br.com.samplelife.model.Articles
import br.com.samplelife.model.Generic
import br.com.samplelife.utils.Constants
import kotlinx.android.synthetic.main.articles_fragment.*
import kotlinx.android.synthetic.main.articles_fragment.view.*
import kotlinx.android.synthetic.main.home_activity.*
import java.util.*

class ArticlesFragment : BaseFragment<ArticlesContract.View, ArticlesContract.Presenter>(),
    ArticlesContract.View {

    var categories: ArrayList<Generic>? = null
    var categoryFilter: ArrayList<Generic>? = ArrayList()

    override var mPresenter: ArticlesContract.Presenter =
        ArticlesPresenter()

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.articles_fragment, container, false)
        val recyclerView = view.recyclerView

        recyclerView.apply {
            // set a LinearLayoutManager to handle Android
            // RecyclerView behavior
            layoutManager = LinearLayoutManager(activity)
            // set the custom adapter to the RecyclerView
            adapter = ArticlesAdapter(ArrayList(), this.context!!)
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (recyclerView.canScrollVertically(-1)) {
                    shadowTopView.visibility = View.VISIBLE
                }else{
                    shadowTopView.visibility = View.GONE
                }
            }

        })

        var adapter = ArticlesAdapter(ArrayList(), this.context)
        adapter.getItemClickSignal().subscribe{news ->
            var intent = Intent(context, ArticlesActivity::class.java)
            intent.putExtra(Constants.PARAM_NEWS, news)
            startActivity(intent)
        }

        recyclerView.adapter = adapter
        mPresenter.fetchNews()


        view.btnFilter.setOnClickListener{
            var intent = Intent(context, ArticlesFilterActivity::class.java)
            intent.putExtra(Constants.PARAM_CATEGORIES, categories)

            if(categoryFilter!=null){
                intent.putExtra(Constants.PARAM_CATEGORY_FILTER, categoryFilter)
            }

            startActivityForResult(intent, Constants.PARAM_REQUEST_CODE_FILTER)
        }

        view?.btnFilter?.visibility = View.INVISIBLE

        view.toogleMenu.setOnClickListener{
            (activity as HomeActivity).drawer_layout.openDrawer(Gravity.LEFT)
        }

        view.btnTryAgain.setOnClickListener{
            txtNotFound.visibility = View.GONE
            btnTryAgain.visibility = View.GONE

            if(categories!=null && categories?.size!! > 0){
                fetchByFilter()
            }else{
                shimmer_view_container.startShimmer()
                shimmer_view_container.visibility = View.VISIBLE
                mPresenter.fetchNews()
            }

        }

        return view
    }

    override fun updateCategories(categories: Any) {
        this.categories = categories as ArrayList<Generic>
    }

    override fun success(result: Any) {
        var adapter: ArticlesAdapter = recyclerView.adapter as ArticlesAdapter
        shimmer_view_container.stopShimmer()
        shimmer_view_container.visibility = View.GONE
        txtNotFound.visibility = View.GONE
        btnTryAgain.visibility = View.GONE
        view?.btnFilter?.visibility = View.VISIBLE
        adapter.articlesList = result as ArrayList<Articles>
        adapter.notifyDataSetChanged()
    }

    override fun showMessage(error: String?) {
        shimmer_view_container.visibility = View.GONE
        txtNotFound.visibility = View.VISIBLE
        txtNotFound.text = error
        btnTryAgain.visibility = View.VISIBLE

        if(categories!=null && categories?.size!!>0){
            view?.btnFilter?.visibility = View.VISIBLE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == Constants.PARAM_REQUEST_CODE_FILTER && resultCode == Activity.RESULT_OK){
            txtNotFound.visibility = View.GONE
            btnTryAgain.visibility = View.GONE

            var paramCategoryFilter = data?.getSerializableExtra(Constants.PARAM_CATEGORY_FILTER) as ArrayList<Generic>
            categoryFilter = paramCategoryFilter
            fetchByFilter()

        }
    }

    private fun fetchByFilter(){

        var strCategory : String? = null

        if(categoryFilter!=null && categoryFilter?.size!! > 0) {

            var strCodeCategory = StringBuilder()

            for ((index, selected) in categoryFilter!!.withIndex()) {
                strCodeCategory.append(selected.genericId)
                if (index < categoryFilter!!.size - 1) {
                    strCodeCategory.append(",")
                }
            }

            strCategory = strCodeCategory.toString()
        }

        var adapter: ArticlesAdapter = recyclerView.adapter as ArticlesAdapter
        adapter.articlesList = ArrayList()
        adapter.notifyDataSetChanged()
        shimmer_view_container.startShimmer()
        shimmer_view_container.visibility = View.VISIBLE

        if(categoryFilter?.size!! > 0){
            view?.btnFilter?.setImageResource(R.drawable.ic_filtro_definido)
        }else{
            view?.btnFilter?.setImageResource(R.drawable.ic_filtro_zerado)
        }


        view?.btnFilter?.visibility = View.INVISIBLE

        mPresenter.fetchNews(strCategory)

    }

}
