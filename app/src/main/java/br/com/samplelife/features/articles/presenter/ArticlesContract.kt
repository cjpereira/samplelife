package br.com.samplelife.features.articles.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView


object ArticlesContract {

    interface View : BaseMvpView{
        fun updateCategories(categories: Any)
    }

    interface Presenter : BaseMvpPresenter<View>{
        fun fetchNews(categrory: String?)
        fun fetchNews()
        fun registerAccessProfessional()
    }

}
