package br.com.samplelife.features.home.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView

object HomeContract {

    interface View : BaseMvpView

    interface Presenter : BaseMvpPresenter<View>{
        fun logout()
    }

}
