package br.com.samplelife.features.professional.service

import br.com.samplelife.api.ApiException
import br.com.samplelife.api.ApiManager
import br.com.samplelife.model.Generic
import br.com.samplelife.model.Professional
import br.com.samplelife.payload.GenericPayload
import br.com.samplelife.payload.ProfessionalResponse
import br.com.samplelife.payload.StateResponse
import br.com.samplelife.utils.Constants
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Response
import rx.Observable


class ProfessionalService {

    fun fetchProfessional(token: String, state: String?, category: String?): Observable<ArrayList<Professional>> {

        return Observable.create { emitter ->
            val service = ApiManager.createRetrofit(IProfessionalService::class.java)
            try {

                var response: Response<ProfessionalResponse> = service.fetchProfessional(token, state, category).execute()

                if (response.errorBody() == null) {

                    var professionalResponse: ProfessionalResponse = response.body()!!

                    var professionals = ArrayList<Professional>()

                    for (item in professionalResponse.professionals!!) {
                        professionals.add(item.toProfessional())
                    }
                    emitter.onNext(professionals)
                    emitter.onCompleted()

                } else {
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()

                    if (response.code() == 404){
                        throw ApiException(Constants.MSG_NOT_FOUND)
                    }

                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if (errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        throw ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString())
                    } else {
                        throw Exception()
                    }
                }

            } catch (e: Exception) {
                if(e is ApiException){
                    emitter.onError(e)
                }else{
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }
        }
    }


    fun fetchMedicalSpecialties(token: String): Observable<ArrayList<Generic>> {

        return Observable.create { emitter ->

            val service = ApiManager.createRetrofit(IProfessionalService::class.java)

            try {

                var response: Response<ArrayList<GenericPayload>> = service.fetchMedicalSpecialties(token).execute()

                if (response.errorBody() == null){
                    var responseCategories: ArrayList<GenericPayload> = response.body()!!

                    var medicalSpecialities = ArrayList<Generic>()

                    for (item in responseCategories){
                        var category = item.toGeneric()
                        medicalSpecialities.add(category)
                    }

                    emitter.onNext(medicalSpecialities)
                    emitter.onCompleted()

                }else{
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if(errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        emitter.onError(ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString()))
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
            }

        }
    }

    fun fetchState(token: String): Observable<ArrayList<Generic>> {

        return Observable.create { emitter ->

            val service = ApiManager.createRetrofit(IProfessionalService::class.java)

            try {

                var response: Response<StateResponse> = service.fetchState(token).execute()

                if (response.errorBody() == null){
                    var responseStates: StateResponse = response.body()!!

                    var states = ArrayList<Generic>()

                    for (item in responseStates.states!!){
                        states.add(item.toGeneric())
                    }

                    emitter.onNext(states)
                    emitter.onCompleted()

                }else{
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if(errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        emitter.onError(ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString()))
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
            }

        }
    }

    fun registerAccessProfessional(id: String?): Observable<Boolean> {
        return Observable.create { emitter ->
            val service = ApiManager.createRetrofit(IProfessionalService::class.java)
            try {
                service.registerAccessProfessional(id).execute()
                emitter.onNext(true)
                emitter.onCompleted()
            } catch (e: Exception) {
                emitter.onNext(false)
                emitter.onCompleted()
            }
        }
    }
}