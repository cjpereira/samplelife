package br.com.samplelife.features.partners.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Html
import br.com.samplelife.R
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.partners.presenter.PartnersDetailsContract
import br.com.samplelife.features.partners.presenter.PartnersDetailsPresenter
import br.com.samplelife.model.Partner
import br.com.samplelife.utils.Constants
import br.com.samplelife.utils.Utils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.partners_details.*




class PartnersDetailsActivity : BaseActivity<PartnersDetailsContract.View, PartnersDetailsContract.Presenter>(),
    PartnersDetailsContract.View {

    override var mPresenter: PartnersDetailsContract.Presenter = PartnersDetailsPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.partners_details)

        var partner = intent.getSerializableExtra(Constants.PARAM_PARTNERS) as Partner

        Picasso.get().load(partner.urlImage)
            .placeholder(R.drawable.placeholder)
            .error(R.drawable.placeholder)
            .into(imgPartner)

        txtDescription.text = partner.name
        txtHealthSpecialty.text = Html.fromHtml(partner.text)

        txtAddress.text = "${partner?.address}, ${partner?.district} - ${partner?.city?.name} - ${partner?.state?.code}\n${partner?.phone} - ${partner?.cellPhone}"

        btnQRCode.text = getString(R.string.partners_description_discount, partner.discount?.percent)

        backButton.setOnClickListener{
            finish()
        }

        btnQRCode.setOnClickListener{
            var intent = Intent(this, QRCodeActivity::class.java)
            intent.putExtra(Constants.PARAM_PARTNERS, partner)
            intent.putExtra(Constants.PARAM_QRCODE, true)
            startActivity(intent)
        }

        btnCall.setOnClickListener{
            call(partner?.phone!!)
        }

        btnSendMail.setOnClickListener{
            sendMail(partner?.mail!!)
        }

        btnShowMap.setOnClickListener{
            if(partner?.address!=null){
                var address = "${partner?.address} - ${partner?.district} - ${partner?.city?.name} / ${partner?.state?.code}"
                Utils.viewOnMap(this, address)
            }
        }

        btnWebSite.setOnClickListener{
            if(partner?.webSite!=null){

                if (!partner?.webSite!!.startsWith("http://") && !partner?.webSite!!.startsWith("https://")){
                    partner?.webSite= "http://" + partner?.webSite;
                }

                val browse = Intent(Intent.ACTION_VIEW, Uri.parse(partner?.webSite))
                startActivity(browse)
            }

        }
    }
}