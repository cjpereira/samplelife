package br.com.samplelife.features.professional.service

import br.com.samplelife.BuildConfig
import br.com.samplelife.api.URLBase
import br.com.samplelife.payload.GenericPayload
import br.com.samplelife.payload.ProfessionalResponse
import br.com.samplelife.payload.StateResponse
import retrofit2.Call
import retrofit2.http.*

@URLBase(BuildConfig.BASE_URL)
interface IProfessionalService {

    @GET("api/profissionalSaude/listar")
    fun fetchProfessional(@Header("token") token: String, @Query("uf") uf: String?, @Query("categorias") category:String?): Call<ProfessionalResponse>

    @POST("api/especialidade-saude/listar")
    fun fetchMedicalSpecialties(@Header("token") token: String): Call<ArrayList<GenericPayload>>

    @POST("api/dominio/uf/listar")
    fun fetchState(@Header("token") token: String) : Call<StateResponse>

    @GET("api/acaoColaborador/profissional/{idColaborador}")
    fun registerAccessProfessional(@Path("idColaborador") id: String?): Call<Void>
}