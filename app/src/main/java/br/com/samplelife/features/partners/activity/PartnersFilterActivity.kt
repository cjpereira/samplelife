package br.com.samplelife.features.partners.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import br.com.samplelife.R
import br.com.samplelife.features.partners.adapter.CategoryAdapter
import br.com.samplelife.features.partners.adapter.StateAdapter
import br.com.samplelife.model.Generic
import br.com.samplelife.utils.Constants
import br.com.samplelife.view.SpacesItemDecoration
import kotlinx.android.synthetic.main.partnersfilter_activity.*

class PartnersFilterActivity : AppCompatActivity() {

    private var stateFilter: ArrayList<Generic>? = null
    private var categoryFilter: ArrayList<Generic>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.partnersfilter_activity)

        var categories = intent.getSerializableExtra(Constants.PARAM_CATEGORIES) as ArrayList<Generic>

        stateFilter = intent.getSerializableExtra(Constants.PARAM_STATE_FILTER) as ArrayList<Generic>?
        categoryFilter = intent.getSerializableExtra(Constants.PARAM_CATEGORY_FILTER) as ArrayList<Generic>

        var gridLayoutManager = GridLayoutManager(this, 3)
        recyclerViewCategory.layoutManager = gridLayoutManager
        recyclerViewCategory.addItemDecoration(SpacesItemDecoration(10))

        recyclerViewCategory.adapter = CategoryAdapter(categories, this, categoryFilter!!)
        recyclerViewCategory.setHasFixedSize(true)

        var states = intent.getSerializableExtra(Constants.PARAM_STATES) as ArrayList<Generic>
        recyclerViewState.layoutManager = GridLayoutManager(this, 4)
        recyclerViewState.addItemDecoration(SpacesItemDecoration(10));

        var stateAdater = StateAdapter(states, this, stateFilter)
//        stateAdater.getItemClickSignal().subscribe{
//            nearBy.isSelected = false
//        }

        recyclerViewState.adapter = stateAdater
        recyclerViewState.setHasFixedSize(true)

        btnConfirm.setOnClickListener{
            var stateAdapter = recyclerViewState.adapter as StateAdapter
            var categoryAdapter = recyclerViewCategory.adapter as CategoryAdapter

            val resultIntent = Intent()

            resultIntent.putExtra(Constants.PARAM_STATE_FILTER, stateAdapter.filterSelected)
            resultIntent.putExtra(Constants.PARAM_CATEGORY_FILTER, categoryAdapter.filterCategory)
            setResult(Activity.RESULT_OK, resultIntent)

            finish()
        }

//        nearBy.setOnClickListener{
//            nearBy.isSelected = !nearBy.isSelected;
//            var adapter = recyclerViewCity.adapter as StateAdapter
//            adapter.filterSelected = null
//            adapter.notifyDataSetChanged()
//        }
    }

}