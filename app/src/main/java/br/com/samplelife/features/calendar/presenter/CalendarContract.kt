package br.com.samplelife.features.calendar.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView
import br.com.samplelife.model.Event
import br.com.samplelife.model.Generic
import java.util.*


object CalendarContract {

    interface View : BaseMvpView{
        fun successProfessional(result: Any)
        fun showErrorDescription(message:String)
        fun showErroCategory(message:String)
        fun showErroDate(message:String)
    }

    interface Presenter : BaseMvpPresenter<View>{
        fun sendCalendar(id: String?, name: String, category: Generic?, date: Calendar?, description: String)
        fun fetchEvents(id: String?)
        fun fetchSpecialisties()
        fun removeCalendar(event: Event)
    }

}
