package br.com.samplelife.features.professional.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import br.com.samplelife.R
import br.com.samplelife.application.UVApplication
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.professional.adapter.ProfessionalAdapter
import br.com.samplelife.features.professional.presenter.ProfessionalContract
import br.com.samplelife.features.professional.presenter.ProfessionalPresenter
import br.com.samplelife.model.Generic
import br.com.samplelife.model.Professional
import br.com.samplelife.utils.Constants
import br.com.samplelife.utils.Utils
import kotlinx.android.synthetic.main.health_professionals.*

class HealthProfessionalActivity : BaseActivity<ProfessionalContract.View, ProfessionalContract.Presenter>(),
    ProfessionalContract.View {

    var states: ArrayList<Generic>? = null
    var categories: ArrayList<Generic>? = null

    var stateFilter: ArrayList<Generic> = ArrayList()
    var medicalSpecialtiesFilter: Generic? = null
    var nameFilter: String? = null

    override var mPresenter: ProfessionalContract.Presenter = ProfessionalPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.health_professionals)

        recyclerView.apply {
            // set a LinearLayoutManager to handle Android
            // RecyclerView behavior
            layoutManager = LinearLayoutManager(this@HealthProfessionalActivity)
            // set the custom adapter to the RecyclerView
            adapter = ProfessionalAdapter(ArrayList(), this.context!!)
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (recyclerView.canScrollVertically(-1)) {
                    shadowTopView.visibility = View.VISIBLE
                }else{
                    shadowTopView.visibility = View.GONE
                }
            }

        })

        var adapter = ProfessionalAdapter(ArrayList(), this)
        adapter.getItemClickSignal().subscribe{professional ->
            var intent = Intent(this,ProfessionalDetailsActivity::class.java)
            intent.putExtra(Constants.PARAM_PROFESSIONAL, professional)
            startActivity(intent)
        }

        recyclerView.adapter = adapter

        btnFilter.setOnClickListener{
            var intent = Intent(this, ProfessionalFilterActivity::class.java)
            intent.putExtra(Constants.PARAM_CATEGORIES, categories)
            intent.putExtra(Constants.PARAM_STATES, states)

            if(stateFilter!=null){
                intent.putExtra(Constants.PARAM_STATE_FILTER, stateFilter)
            }

            if(medicalSpecialtiesFilter!=null){
                intent.putExtra(Constants.PARAM_CATEGORY_FILTER, medicalSpecialtiesFilter)
            }

            if(nameFilter!=null){
                intent.putExtra(Constants.PARAM_NAME_FILTER, nameFilter)
            }

            startActivityForResult(intent, Constants.PARAM_REQUEST_CODE_FILTER)
        }

        btnFilter?.visibility = View.INVISIBLE

        btnTryAgain.setOnClickListener{
            txtNotFound.visibility = View.GONE
            btnTryAgain.visibility = View.GONE

            if(categories!=null && categories?.size!! > 0){
                fetchByFilter()
            }else{
                shimmer_view_container.startShimmer()
                shimmer_view_container.visibility = View.VISIBLE
                mPresenter.fetchProfessional((application as UVApplication).login?.token!!)
            }
        }

        backButton.setOnClickListener{
            onBackPressed()
        }

        if((application as UVApplication).login?.token!=null && adapter.professionalList.size == 0){
            mPresenter.fetchProfessional((application as UVApplication).login?.token!!)
        }

    }

    override fun updateCategories(categories: Any) {
        this.categories = categories as ArrayList<Generic>
    }

    override fun updateStates(states: Any) {
        this.states = states as ArrayList<Generic>
    }

    override fun success(result: Any) {
        var adapter: ProfessionalAdapter = recyclerView.adapter as ProfessionalAdapter
        shimmer_view_container.stopShimmer()
        shimmer_view_container.visibility = View.GONE
        txtNotFound.visibility = View.GONE
        btnTryAgain.visibility = View.GONE
        btnFilter?.visibility = View.VISIBLE

        var professionalList = ArrayList<Professional>()

        if(nameFilter!=null && !nameFilter!!.isEmpty()){
            for(professional in result as ArrayList<Professional>){
                if(Utils.stripAccents(professional.name!!).contains(Utils.stripAccents(nameFilter!!), true)){
                    professionalList.add(professional)
                }
            }
        }else{
            professionalList = result as ArrayList<Professional>
        }

        adapter.professionalList = professionalList
        adapter.notifyDataSetChanged()
    }

    override fun showMessage(error: String?) {
        shimmer_view_container.visibility = View.GONE
        txtNotFound.visibility = View.VISIBLE
        txtNotFound.text = error
        btnTryAgain.visibility = View.VISIBLE

        if(categories!=null && categories?.size!!>0){
            btnFilter?.visibility = View.VISIBLE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == Constants.PARAM_REQUEST_CODE_FILTER && resultCode == Activity.RESULT_OK){


            txtNotFound.visibility = View.GONE
            btnTryAgain.visibility = View.GONE

            var paramStateFilter = data?.getSerializableExtra(Constants.PARAM_STATE_FILTER) as ArrayList<Generic>
            var paraMedicalSpecialtiesFilter =  data?.getSerializableExtra(Constants.PARAM_CATEGORY_FILTER) as Generic?
            var paraNameFilter =  data?.getSerializableExtra(Constants.PARAM_NAME_FILTER) as String?

            stateFilter = paramStateFilter
            nameFilter = paraNameFilter
            medicalSpecialtiesFilter = paraMedicalSpecialtiesFilter
            fetchByFilter()

        }
    }

    private fun fetchByFilter(){

        var strState : String? = null

        if(stateFilter!=null && stateFilter?.size!! > 0) {

            var strCodeState = StringBuilder()

            for ((index, selected) in stateFilter!!.withIndex()) {
                strCodeState.append(selected.genericId)
                if (index < stateFilter!!.size - 1) {
                    strCodeState.append(",")
                }
            }
            strState = strCodeState.toString()
        }

        var strCategory : String? = null

        if(medicalSpecialtiesFilter!=null) {
            strCategory = medicalSpecialtiesFilter?.genericId
        }

        var adapter: ProfessionalAdapter = recyclerView.adapter as ProfessionalAdapter
        adapter.professionalList = ArrayList()
        adapter.notifyDataSetChanged()
        shimmer_view_container.startShimmer()
        shimmer_view_container.visibility = View.VISIBLE

        if(medicalSpecialtiesFilter!=null || (stateFilter!=null && stateFilter.size >0) || (nameFilter!=null && !nameFilter!!.isEmpty())){
            btnFilter?.setImageResource(R.drawable.ic_filtro_definido)
        }else{
            btnFilter?.setImageResource(R.drawable.ic_filtro_zerado)
        }

        btnFilter?.visibility = View.INVISIBLE

        mPresenter.fetchProfessional((application as UVApplication).login?.token!!, strState, strCategory)

    }


}