package br.com.samplelife.features.professional.activity

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.widget.ImageView
import br.com.samplelife.R
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.professional.presenter.ProfessionalDetailsContract
import br.com.samplelife.features.professional.presenter.ProfessionalDetailsPresenter
import br.com.samplelife.model.Professional
import br.com.samplelife.utils.Constants
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.professionaldetails_activity.*


class ProfessionalDetailsActivity : BaseActivity<ProfessionalDetailsContract.View, ProfessionalDetailsContract.Presenter>(),
    ProfessionalDetailsContract.View {

    override var mPresenter: ProfessionalDetailsContract.Presenter = ProfessionalDetailsPresenter()


    private var professional: Professional? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.professionaldetails_activity)

        professional = intent.getSerializableExtra(Constants.PARAM_PROFESSIONAL) as Professional

        txtNameProfessional.text = professional?.name
        txtNameSpecialty.text = "${professional?.specialty?.name} EM ${professional?.city?.name} - ${professional?.state?.code}"
        txtHealthPlans.text = professional?.helthPlan
        txtViewPhone.text = professional?.phone
        txtViewAddress.text = "${professional?.address}, ${professional?.district} - ${professional?.city?.name} - ${professional?.state?.code}"

        backButton.setOnClickListener{
            onBackPressed()
        }

        btnCall.setOnClickListener{
            call(professional?.phone!!)
        }

        btnSendMail.setOnClickListener{
            sendMail(professional?.mail!!)
        }

        if(professional?.linkImage!=null && !professional?.linkImage?.isEmpty()!!){


            val target = object : Target {

            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {

                val d = BitmapDrawable(
                    this@ProfessionalDetailsActivity.resources,
                    Bitmap.createScaledBitmap(
                        bitmap,
                        bitmap.width,
                        bitmap.height,
                        true
                    )
                )

                imgPartner.scaleType = ImageView.ScaleType.CENTER_CROP
                imgPartner.setImageDrawable(d)
            }

            override fun onBitmapFailed(e: Exception, errorDrawable: Drawable) {

            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable) {

            }
        }

        mPresenter.registerAccessProfessional()

        Picasso.get().load(professional?.linkImage).placeholder(R.drawable.ic_placeholder_img_detalhe_prof).error(R.drawable.ic_placeholder_img_detalhe_prof).into(target)
        }
    }
}
