package br.com.samplelife.features.chat.service

import br.com.samplelife.BuildConfig
import br.com.samplelife.api.URLBase
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

@URLBase(BuildConfig.BASE_URL)
interface IRegisterChat {

    @GET("api/acaoColaborador/profissionalChat/{idColaborador}")
    fun registerAccessProfessionalChat(@Path("idColaborador") id: String?): Call<Void>
}