package br.com.samplelife.features.home.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import br.com.samplelife.R
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageOptions
import com.theartofdev.edmodo.cropper.CropImageView
import java.io.File
import java.io.IOException


class CropImageActivity : AppCompatActivity(), CropImageView.OnSetImageUriCompleteListener,
    CropImageView.OnCropImageCompleteListener{

    var mCropImageUri: Uri? = null
    var mOptions: CropImageOptions? = null
    var cropImageView: CropImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.crop_image)

        cropImageView = findViewById(R.id.cropImageView)
        val bundle = intent.getBundleExtra(CropImage.CROP_IMAGE_EXTRA_BUNDLE)

        mCropImageUri = bundle.getParcelable(CropImage.CROP_IMAGE_EXTRA_SOURCE)
        mOptions = bundle.getParcelable(CropImage.CROP_IMAGE_EXTRA_OPTIONS)

        cropImageView?.setImageUriAsync(mCropImageUri)
        setSupportActionBar(findViewById(R.id.toolbar))

        val bar = supportActionBar
        bar!!.setDisplayHomeAsUpEnabled(true)
        bar.title = ""

    }
    //----------------------------------------------

    override fun onResume() {
        super.onResume()
        cropImageView?.setOnSetImageUriCompleteListener(this)
        cropImageView?.setOnCropImageCompleteListener(this)
    }
    //----------------------------------------------

    override fun onStop() {
        super.onStop()
        cropImageView?.setOnSetImageUriCompleteListener(null)
        cropImageView?.setOnCropImageCompleteListener(null)
    }
    //----------------------------------------------

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_crop, menu)
        return true
    }
    //----------------------------------------------

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()

        if (id == R.id.crop_image_menu_crop) {
            cropImage()
        }

        return super.onOptionsItemSelected(item)
    }
    //----------------------------------------------


    /** Execute crop image and save the result tou output uri.  */
    protected fun cropImage() {
        if (mOptions!!.noOutputImage) {
            setResult(null, null, 1)
        } else {
            val outputUri = getOutputUri()
            cropImageView?.saveCroppedImageAsync(
                outputUri,
                mOptions!!.outputCompressFormat,
                mOptions!!.outputCompressQuality,
                mOptions!!.outputRequestWidth,
                mOptions!!.outputRequestHeight,
                mOptions!!.outputRequestSizeOptions
            )
        }
    }
    //----------------------------------------------

    /**
     * Get Android uri to save the cropped image into.<br></br>
     * Use the given in options or create a temp file.
     */
    private fun getOutputUri(): Uri? {
        var outputUri = mOptions!!.outputUri
        if (outputUri == null || outputUri == Uri.EMPTY) {
            try {
                val ext = if (mOptions?.outputCompressFormat === Bitmap.CompressFormat.JPEG)
                    ".jpg"
                else if (mOptions?.outputCompressFormat === Bitmap.CompressFormat.PNG) ".png" else ".webp"
                outputUri = Uri.fromFile(File.createTempFile("cropped", ext, cacheDir))
            } catch (e: IOException) {
                throw RuntimeException("Failed to create temp file for output image", e)
            }

        }
        return outputUri
    }
    //----------------------------------------------

    /** Result with cropped image data or error if failed.  */
    private fun setResult(uri: Uri?, error: Exception?, sampleSize: Int) {
        val resultCode = if (error == null) Activity.RESULT_OK else CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE
        setResult(resultCode, getResultIntent(uri, error, sampleSize))
        finish()
    }
    //----------------------------------------------


    /** Get intent instance to be used for the result of this activity.  */
    private fun getResultIntent(uri: Uri?, error: Exception?, sampleSize: Int): Intent {
        val result = CropImage.ActivityResult(
            cropImageView?.imageUri,
            uri,
            error,
            cropImageView?.cropPoints!!,
            cropImageView?.cropRect!!,
            cropImageView?.rotatedDegrees!!,
            cropImageView?.wholeImageRect!!,
            sampleSize
        )
        val intent = Intent()
        intent.putExtras(getIntent())
        intent.putExtra(CropImage.CROP_IMAGE_EXTRA_RESULT, result)
        return intent
    }

    //----------------------------------------------
    override fun onSetImageUriComplete(view: CropImageView?, uri: Uri?, error: Exception?) {

    }

    override fun onCropImageComplete(view: CropImageView?, result: CropImageView.CropResult?) {
        setResult(result?.uri, result?.error, result!!.sampleSize)
    }

}