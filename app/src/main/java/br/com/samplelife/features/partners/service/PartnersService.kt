package br.com.samplelife.features.partners.service

import br.com.samplelife.api.ApiException
import br.com.samplelife.api.ApiManager
import br.com.samplelife.model.Partner
import br.com.samplelife.model.Generic
import br.com.samplelife.payload.PartnersResponse
import br.com.samplelife.payload.GenericPayload
import br.com.samplelife.payload.StateResponse
import br.com.samplelife.utils.Constants
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Response
import rx.Observable

class PartnersService {

    fun fetchPartners(token: String, state: String?, category: String?): Observable<ArrayList<Partner>> {

        return Observable.create { emitter ->

            val service = ApiManager.createRetrofit(IPartnersService::class.java)
            try {

                var response: Response<PartnersResponse> = service.fetchPartners(token, state, category).execute()

                if (response.errorBody() == null){
                    var partnersResponse: PartnersResponse = response.body()!!

                    var partners = ArrayList<Partner>()
                    for (item in partnersResponse.partners!!){

                        var partner = item.toParner()
                        partners.add(partner)
                    }

                    emitter.onNext(partners)
                    emitter.onCompleted()

                }else{
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()

                    if (response.code() == 404){
                        throw ApiException(Constants.MSG_NOT_FOUND)
                    }

                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if(errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        throw ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString())
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                if(e is ApiException){
                    emitter.onError(e)
                }else{
                    emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
                }
            }
        }
    }


    fun fetchState(token: String): Observable<ArrayList<Generic>> {

        return Observable.create { emitter ->

            val service = ApiManager.createRetrofit(IPartnersService::class.java)

            try {

                var response: Response<StateResponse> = service.fetchState(token).execute()

                if (response.errorBody() == null){
                    var responseStates: StateResponse = response.body()!!

                    var states = ArrayList<Generic>()

                    for (item in responseStates.states!!){
                        states.add(item.toGeneric())
                    }

                    emitter.onNext(states)
                    emitter.onCompleted()

                }else{
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if(errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        emitter.onError(ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString()))
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
            }

        }
    }

    fun fetchCategory(token: String): Observable<ArrayList<Generic>> {

        return Observable.create { emitter ->

            val service = ApiManager.createRetrofit(IPartnersService::class.java)

            try {

                var response: Response<ArrayList<GenericPayload>> = service.fetchCategory(token).execute()

                if (response.errorBody() == null){
                    var responseCategories: ArrayList<GenericPayload> = response.body()!!

                    var categories = ArrayList<Generic>()

                    for (item in responseCategories){

                        var category = item.toGeneric()
                        categories.add(category)
                    }

                    emitter.onNext(categories)
                    emitter.onCompleted()

                }else{
                    val errorResponse = response.errorBody()
                    val returnError = errorResponse!!.string()
                    val errorJson = Gson().fromJson<JsonObject>(returnError, JsonObject::class.java)
                    if(errorJson.has(Constants.PARAM_CODE) && errorJson.has(Constants.PARAM_MSG_RROR)) {
                        emitter.onError(ApiException(errorJson.get(Constants.PARAM_MSG_RROR).toString()))
                    }else{
                        throw Exception()
                    }
                }

            }catch (e: Exception){
                emitter.onError(ApiException(Constants.MSG_SYSTEM_NOT_AVAILABLE))
            }

        }
    }

}
