package br.com.samplelife.features.partners.activity

import android.graphics.Bitmap
import android.os.Bundle
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import br.com.samplelife.R
import br.com.samplelife.application.UVApplication
import br.com.samplelife.model.Partner
import br.com.samplelife.utils.Constants
import br.com.samplelife.utils.ImageSaver
import br.com.samplelife.utils.Utils
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import kotlinx.android.synthetic.main.qr_code.*
import java.util.*

class QRCodeActivity : AppCompatActivity(){

    var showQrcode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.qr_code)

        showQrcode = intent.getBooleanExtra(Constants.PARAM_QRCODE, false)

        var partner = intent.getSerializableExtra(Constants.PARAM_PARTNERS) as Partner

        txtNamePartners.text = partner.name
        txtDiscount.text = getString(R.string.partners_discount_qrcode, partner.discount?.percent)

        if(showQrcode){
            cardViewQrCode.visibility = View.VISIBLE
            cardViewUser.visibility = View.GONE
            txtTitle.text = "QR Code Gerado"
            txtCode.text = partner.qrCode?.toUpperCase()
            generate(txtCode.text.toString())
            txtDiscount.text = getString(R.string.partners_discount_qrcode, partner.discount?.percent)
        }else{
            cardViewQrCode.visibility = View.GONE
            cardViewUser.visibility = View.VISIBLE
            txtTitle.text = "Verificado"
            txtUser.text = (application as UVApplication).login?.user?.name
            txtDiscount.text = getString(R.string.discount_qrcode, partner.discount?.percent)
        }

        backButton.setOnClickListener{
            onBackPressed()
        }

    }

    override fun onResume() {
        super.onResume()

        if(!showQrcode) {

            try {
                var fileName: String? = ""
                var image: Bitmap? = null

                fileName = (application as UVApplication).login?.user?.cpf

                if (fileName != null) {
                    if (!fileName!!.isEmpty()) {
                        //Busca a imagem armazenada na pasta privada do aplicativo.
                        image = ImageSaver(this).setFileName("$fileName.jpg").setDirectoryName("images").load()
                        if (image!!.byteCount > 0) {
                            val bit = Bitmap.createScaledBitmap(
                                image!!,
                                Utils.dpToPx(this, 52).toInt(),
                                Utils.dpToPx(this, 52).toInt(),
                                false
                            )

                            val roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(resources, bit)
                            roundedBitmapDrawable.cornerRadius = Utils.dpToPx(this, 10)
                            roundedBitmapDrawable.setAntiAlias(true)

                            photoImageView.setImageDrawable(roundedBitmapDrawable)

                        }
                    }
                }
            } catch (e: Exception) {
                e.message.toString()
            }
        }
    }

    private fun generate(uniqueID: String) {
        // TODO Auto-generated method stub
        val barcodeFormat = BarcodeFormat.QR_CODE

        val colorBack = -0x1000000
        val colorFront = -0x1

        val writer = QRCodeWriter()
        try {
            val hint = EnumMap<EncodeHintType, Any>(EncodeHintType::class.java)
            hint.put(EncodeHintType.CHARACTER_SET, "UTF-8")
            val bitMatrix = writer.encode(uniqueID, barcodeFormat, resources.getDimensionPixelSize(R.dimen.qrcode_size), resources.getDimensionPixelSize(R.dimen.qrcode_size), hint)
            val width = bitMatrix.width
            val height = bitMatrix.height
            val pixels = IntArray(width * height)
            for (y in 0 until height) {
                val offset = y * width
                for (x in 0 until width) {

                    pixels[offset + x] = if (bitMatrix.get(x, y)) colorBack else colorFront
                }
            }

            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
            imgQRCode.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
    }

}