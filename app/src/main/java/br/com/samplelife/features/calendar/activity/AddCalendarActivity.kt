package br.com.samplelife.features.calendar.activity

import android.annotation.TargetApi
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.appcompat.widget.AppCompatTextView
import br.com.samplelife.R
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.calendar.presenter.CalendarContract
import br.com.samplelife.features.calendar.presenter.CalendarPresenter
import br.com.samplelife.model.Event
import br.com.samplelife.model.Generic
import br.com.samplelife.utils.Constants
import br.com.samplelife.utils.Utils
import br.com.samplelife.view.DropDownAdapter
import kotlinx.android.synthetic.main.add_calendar.*
import java.text.SimpleDateFormat
import java.util.*


class AddCalendarActivity: BaseActivity<CalendarContract.View, CalendarContract.Presenter>(),
    CalendarContract.View {

    private var occurrenceDate: Calendar? = Calendar.getInstance()
    private var category: Generic? = null
    private var event: Event? = null

    override var mPresenter: CalendarContract.Presenter =
        CalendarPresenter()

    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_calendar)

        if (intent.getSerializableExtra(Constants.PARAM_EVENT) != null) {
            event = intent.getSerializableExtra(Constants.PARAM_EVENT) as Event
        }

        var medicalSpecialties = intent.getSerializableExtra(Constants.PARAM_CATEGORIES) as ArrayList<Generic>

        btnDate.setOnClickListener {

            var tempCalendar = Calendar.getInstance()
            val mYear = tempCalendar.get(Calendar.YEAR)
            val mMonth = tempCalendar.get(Calendar.MONTH)
            val mDay = tempCalendar.get(Calendar.DAY_OF_MONTH)


            val datePickerDialog = DatePickerDialog(getContext(),
                { view, year, monthOfYear, dayOfMonth ->
                    tempCalendar.set(Calendar.YEAR, year)
                    tempCalendar.set(Calendar.MONTH, monthOfYear)
                    tempCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                    showTime(tempCalendar)
                }, mYear, mMonth, mDay
            )

            datePickerDialog.show()
        }

        val medicalAdapter = DropDownAdapter(this, medicalSpecialties)
        spMedicalSpecialties?.adapter = medicalAdapter
        medicalSpecialties.add(0, Generic("", "", ""))

        inputLayoutName.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                inputLayoutName.isErrorEnabled = false
            }
            override fun afterTextChanged(editable: Editable) {}
        })


        inputLayoutCategories.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                inputLayoutCategories.isErrorEnabled = false
            }
            override fun afterTextChanged(editable: Editable) {}
        })

        spMedicalSpecialties.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                if (adapterView.getChildAt(0) != null && (adapterView.getChildAt(0) as ViewGroup).getChildAt(0) != null) {

                    inputLayoutCategories.isErrorEnabled = false
                    inputLayoutCategories.editText?.setText("")
                    category = null
                    if (position > 0) {
                        inputLayoutCategories.editText?.setText(medicalAdapter.listItemsTxt[position].name)
                        category = medicalAdapter.listItemsTxt[position]
                        ((spMedicalSpecialties.getChildAt(0) as ViewGroup).getChildAt(0) as AppCompatTextView).text = ""
                    }
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {}
        }

        btnSend.setOnClickListener{

            var idEvent:String? = ""
            if(event != null){
                idEvent = event?.id!!
            }

            mPresenter.sendCalendar(idEvent, inputLayoutName.editText?.text.toString(), category, occurrenceDate, inputLayoutDescription.editText?.text.toString())
        }

        btnDelete.setOnClickListener{

            val alertDialogBuilder = AlertDialog.Builder(this)
            alertDialogBuilder.setMessage("Deseja apagar este agendamento?")
            alertDialogBuilder.setTitle("")
            alertDialogBuilder
                .setCancelable(false)
                .setNegativeButton(resources.getString(R.string.dialog_nao_button)) { dialog, id ->
                }
                .setPositiveButton(resources.getString(R.string.dialog_sim_button)) { dialog, id ->
                    mPresenter.removeCalendar(event!!)
                }
            val alertD = alertDialogBuilder.create()
            alertD.show()

        }

        inputLayoutDate.editText?.setText(Utils.parseDateToString(occurrenceDate?.time, "dd/MM/yyyy HH:mm"))

        backButton.setOnClickListener {
            onBackPressed()
        }

        if(event!=null){
            inputLayoutName.editText?.setText(event?.name!!)

            for((index, category) in medicalSpecialties.withIndex()){
                if(category.name.equals(event?.category?.name, true)){
                    spMedicalSpecialties.setSelection(index)
                }
            }

            var format = SimpleDateFormat("dd/MM/yyyy HH:mm")
            inputLayoutDate.editText?.setText(format.format(event?.date))
            inputLayoutDescription.editText?.setText(event?.description)

            txtTitle.text = "Compromisso agendado"
            category = event?.category
            occurrenceDate?.time = event?.date!!

            btnDelete.visibility = View.VISIBLE
            if(event?.name!!.equals("Beber Água", true)){
                inputLayoutName.editText?.isEnabled = false
                inputLayoutCategories.editText?.isEnabled = false
                spMedicalSpecialties.isEnabled = false
                inputLayoutDate.editText?.isEnabled = false
                inputLayoutDescription.editText?.isEnabled = false
                btnSend.visibility = View.INVISIBLE
                btnDelete.visibility = View.INVISIBLE
                btnDate.isEnabled = false
                inputLayoutCategories.editText?.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null)
                inputLayoutDate.editText?.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,null,null)
            }
        }

    }

    private fun showTime(tempCalendar: Calendar){

        // Get Current Time
        val mHour = tempCalendar.get(Calendar.HOUR_OF_DAY)
        val mMinute = tempCalendar.get(Calendar.MINUTE)

        // Launch Time Picker Dialog
        val timePickerDialog = TimePickerDialog(this,
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                tempCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                tempCalendar.set(Calendar.MINUTE, minute)
                inputLayoutDate.editText?.setText(Utils.parseDateToString(tempCalendar.time, "dd/MM/yyyy HH:mm"))
                inputLayoutDate.isErrorEnabled = false
                occurrenceDate = tempCalendar

            }, mHour, mMinute, false
        )
        timePickerDialog.show()
    }

    override fun success(result: Any) {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage(result as String)
        alertDialogBuilder.setTitle("")
        alertDialogBuilder
            .setCancelable(false)
            .setNegativeButton(resources.getString(R.string.dialog_neutral_button)) { dialog, id ->
                finish()
            }

        val alertD = alertDialogBuilder.create()
        alertD.show()
    }

    override fun successProfessional(result: Any) {}


    override fun showErroCategory(message: String) {
        inputLayoutCategories.isErrorEnabled = true
        inputLayoutCategories.error = message
    }

    override fun showErroDate(message: String) {
        inputLayoutDate.isErrorEnabled = true
        inputLayoutDate.error = message
    }

    override fun showErrorDescription(message: String) {
        inputLayoutName.isErrorEnabled = true
        inputLayoutName.error = message
    }


}
