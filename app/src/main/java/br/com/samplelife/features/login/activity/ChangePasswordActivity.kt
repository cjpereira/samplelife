package br.com.samplelife.features.login.activity

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import br.com.samplelife.R
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.login.presenter.PasswordContract
import br.com.samplelife.features.login.presenter.PasswordPresenter
import br.com.samplelife.model.Login
import br.com.samplelife.utils.Constants
import kotlinx.android.synthetic.main.resetpassword_activity.*

class ChangePasswordActivity : BaseActivity<PasswordContract.View, PasswordContract.Presenter>(),
    PasswordContract.View{

    override var mPresenter: PasswordContract.Presenter = PasswordPresenter()
    var login: Login? = null

    override fun onBackPressed() {
        val resultIntent = Intent()
        setResult(Activity.RESULT_CANCELED, resultIntent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.resetpassword_activity)

        if(intent.getSerializableExtra(Constants.PARAM_LOGIN)!=null){
            login = intent.getSerializableExtra(Constants.PARAM_LOGIN) as Login
        }

        btnSend.setOnClickListener{
            loading()
            mPresenter.doChangePassword(login?.user?.id!!, inputLayoutPassword.editText?.text.toString())
        }

        backButton.setOnClickListener{
            onBackPressed()
        }
    }

    override fun success(result: Any) {

        showMessage(result as String, DialogInterface.OnClickListener{
                _, _ ->
            hideLoading()
            val resultIntent = Intent()
            resultIntent.putExtra(Constants.PARAM_LOGIN, login)
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        })
    }

    override fun showErrorUser(message: Int) {}

    override fun showErrorPassword(message: Int) {
        hideLoading()
        inputLayoutPassword?.error = resources.getString(message)
    }

}