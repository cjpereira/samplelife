package br.com.samplelife.features.qrcode.service

import br.com.samplelife.BuildConfig
import br.com.samplelife.api.URLBase
import br.com.samplelife.payload.QRCodeResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header


@URLBase(BuildConfig.BASE_URL)
interface IQRCodeService {

    @GET("api/parceiro/obterPorQrCode")
    fun fetchPartners(@Header("qrCode") qrCode:String?, @Header("colaborador") userId:String?): Call<QRCodeResponse>
}