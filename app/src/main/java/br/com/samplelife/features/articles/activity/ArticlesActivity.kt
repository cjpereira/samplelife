package br.com.samplelife.features.articles.activity

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import br.com.samplelife.R
import br.com.samplelife.base.BaseActivity
import br.com.samplelife.features.articles.presenter.ArticlesContract
import br.com.samplelife.features.articles.presenter.ArticlesPresenter
import br.com.samplelife.model.Articles
import br.com.samplelife.utils.Constants
import kotlinx.android.synthetic.main.article_details.*
import java.lang.Exception


class ArticlesActivity : BaseActivity<ArticlesContract.View, ArticlesContract.Presenter>(),
    ArticlesContract.View{

    override var mPresenter: ArticlesContract.Presenter = ArticlesPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.article_details)

        var news = intent.getSerializableExtra(Constants.PARAM_NEWS) as Articles

        txtDescription.text = news.title

        var img = ""
        if(news.url!=null){
            img = "<img src="+news.url+ " height='auto' width='90%'/>"
        }

        //Font must be placed in assets/fonts folder
        val text =
            (
                    "<html>"+

                            "<style type='text/css'>" +
                            "@font-face { font-family: opensansregular; src: url('fonts/opensansregular.ttf'); }"+
                            "body div {font-family: opensansregular; margin: 5%;}"+
                            "body img {margin-right: 5%;margin-left: 5%;}"+
                            "body iframe {width: 100%; height=auto}"+

                            "</style>"+
                            "<body >" + img+
                            "<div style=\"font-size: 13px; font-family: opensansregular; color:#505050\">" +
                            news.text +
                            "</div> " +
                            "</body>"+
                            "</html>"
                    )

        webViewDescripton.loadDataWithBaseURL("file:///android_asset/",text,"text/html","utf-8",null)

        // Get the web view settings instance
        val settings = webViewDescripton.settings
        settings.javaScriptEnabled = true
        settings.setTextSize(WebSettings.TextSize.NORMAL);

        // WebView settings
        webViewDescripton.fitsSystemWindows = true

        webViewDescripton.webViewClient = object: WebViewClient(){
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
            }

            override fun onPageFinished(view: WebView, url: String) {
                view.height
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                try{
                    var intent = Intent(Intent.ACTION_VIEW, request?.url)
                    view?.context?.startActivity(intent)
                }catch (e: Exception){

                }

                return true
            }
        }

        updateDate.text = news.strDate
        backButton.setOnClickListener{
            finish()
        }

        mPresenter.registerAccessProfessional()
    }

    override fun updateCategories(categories: Any) {}

}