package br.com.samplelife.features.login.service

import br.com.samplelife.BuildConfig
import br.com.samplelife.api.URLBase
import br.com.samplelife.payload.LoginResponsePayload
import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST

@URLBase(BuildConfig.BASE_URL)
interface ILoginService {

    @POST("api/seguranca/login")
    fun doLogin(@Header("usuario") login: String, @Header("senha") password: String): Call<LoginResponsePayload>

    @POST("api/seguranca/recuperarSenha")
    fun recoveryPassword(@Header("usuario") login: String): Call<LoginResponsePayload>

    @POST("api/seguranca/alterarSenha")
    fun changePassword(@Header("idUsuario") login: String, @Header("senha") password: String): Call<LoginResponsePayload>

}