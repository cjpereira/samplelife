package br.com.samplelife.features.login.presenter

import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView

object LoginContract {

    interface View : BaseMvpView {
        fun showErrorLogin(message: Int)
        fun showErrorPassword(message: Int)
    }

    interface Presenter : BaseMvpPresenter<View> {
        fun doLogin(login: String, password: String)
    }

}
