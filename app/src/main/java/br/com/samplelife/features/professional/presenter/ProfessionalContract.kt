package br.com.samplelife.features.professional.presenter


import br.com.samplelife.base.BaseMvpPresenter
import br.com.samplelife.base.BaseMvpView


object ProfessionalContract {

    interface View : BaseMvpView{
        fun updateStates(states: Any)
        fun updateCategories(categories: Any)

    }

    interface Presenter : BaseMvpPresenter<View>{
        fun fetchProfessional(token: String, state: String?, categrory: String?)
        fun fetchProfessional(token: String)
    }

}
