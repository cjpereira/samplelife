package br.com.samplelife.features.login.presenter

import br.com.samplelife.R
import br.com.samplelife.base.BaseMvpPresenterImpl
import br.com.samplelife.features.login.service.LoginService
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

open class LoginPresenter: BaseMvpPresenterImpl<LoginContract.View>(),
    LoginContract.Presenter {

    override fun doLogin(login: String, password: String){

        if(validateLogin(login, password)){

            LoginService().doLogin(login, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->

                    LoginService().addPushId(result.user?.id!!)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({},{})

                    mView?.success(result)

                },
                { throwable ->
                    mView?.showMessage(throwable.message)
                }
            )
        }
    }

    fun validateLogin(login: String, password: String): Boolean{

        var isValid = true

        if (login.isEmpty()) {
            isValid = false
            mView?.showErrorLogin(R.string.msg_error_empty)
        }

        if (password.isEmpty()) {
            isValid = false
            mView?.showErrorPassword(R.string.msg_error_empty)
        }

        return isValid
    }

}