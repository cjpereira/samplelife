package br.com.samplelife.view

import android.view.ViewGroup


/**
 * Interface for providing common API for observable and scrollable widgets.
 */
interface Scrollable {

    /**
     * Return the current Y of the scrollable view.
     *
     * @return Current Y pixel.
     */
    val currentScrollY: Int

    /**
     * Set a callback listener.<br></br>
     * Developers should use [.addScrollViewCallbacks]
     * and [.removeScrollViewCallbacks].
     *
     * @param listener Listener to set.
     */
    @Deprecated("")
    fun setScrollViewCallbacks(listener: ObservableScrollViewCallbacks)

    /**
     * Add a callback listener.
     *
     * @param listener Listener to add.
     * @since 1.7.0
     */
    fun addScrollViewCallbacks(listener: ObservableScrollViewCallbacks)

    /**
     * Remove a callback listener.
     *
     * @param listener Listener to remove.
     * @since 1.7.0
     */
    fun removeScrollViewCallbacks(listener: ObservableScrollViewCallbacks)

    /**
     * Clear callback listeners.
     *
     * @since 1.7.0
     */
    fun clearScrollViewCallbacks()

    /**
     * Scroll vertically to the absolute Y.<br></br>
     * Implemented classes are expected to scroll to the exact Y pixels from the top,
     * but it depends on the type of the widget.
     *
     * @param y Vertical position to scroll to.
     */
    fun scrollVerticallyTo(y: Int)

    /**
     * Set a touch motion event delegation ViewGroup.<br></br>
     * This is used to pass motion events back to parent view.
     * It's up to the implementation classes whether or not it works.
     *
     * @param viewGroup ViewGroup object to dispatch motion events.
     */
    fun setTouchInterceptionViewGroup(viewGroup: ViewGroup)
}