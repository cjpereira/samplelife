package br.com.samplelife.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.webkit.WebView
import android.os.Parcel
import android.os.Parcelable
import android.view.ViewGroup
import android.view.MotionEvent
import android.view.View


class ObservableWebView : WebView, Scrollable {

    // Fields that should be saved onSaveInstanceState
    private var mPrevScrollY: Int = 0
    override var currentScrollY: Int = 0
        private set

    // Fields that don't need to be saved onSaveInstanceState
    private var mCallbacks: ObservableScrollViewCallbacks? = null
    private var mCallbackCollection: MutableList<ObservableScrollViewCallbacks>? = null
    private var mScrollState: ScrollState? = null
    private var mFirstScroll: Boolean = false
    private var mDragging: Boolean = false
    private var mIntercepted: Boolean = false
    private var mPrevMoveEvent: MotionEvent? = null
    private var mTouchInterceptionViewGroup: ViewGroup? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {}

    public override fun onRestoreInstanceState(state: Parcelable) {
        val ss = state as SavedState
        mPrevScrollY = ss.prevScrollY
        currentScrollY = ss.scrollY
        super.onRestoreInstanceState(ss.getSuperState())
    }

    public override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        val ss = SavedState(superState)
        ss.prevScrollY = mPrevScrollY
        ss.scrollY = currentScrollY
        return ss
    }

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)
        if (hasNoCallbacks()) {
            return
        }
        currentScrollY = t

        dispatchOnScrollChanged(currentScrollY, mFirstScroll, mDragging)
        if (mFirstScroll) {
            mFirstScroll = false
        }

        if (mPrevScrollY < t) {
            mScrollState = ScrollState.UP
        } else if (t < mPrevScrollY) {
            mScrollState = ScrollState.DOWN
        } else {
            mScrollState = ScrollState.STOP
        }
        mPrevScrollY = t
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        if (hasNoCallbacks()) {
            return super.onInterceptTouchEvent(ev)
        }
        when (ev.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                // Whether or not motion events are consumed by children,
                // flag initializations which are related to ACTION_DOWN events should be executed.
                // Because if the ACTION_DOWN is consumed by children and only ACTION_MOVEs are
                // passed to parent (this view), the flags will be invalid.
                // Also, applications might implement initialization codes to onDownMotionEvent,
                // so call it here.
                mDragging = true
                mFirstScroll = mDragging
                dispatchOnDownMotionEvent()
            }
        }
        return super.onInterceptTouchEvent(ev)
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        if (hasNoCallbacks()) {
            return super.onTouchEvent(ev)
        }
        when (ev.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                mIntercepted = false
                mDragging = false
                dispatchOnUpOrCancelMotionEvent(mScrollState)
            }
            MotionEvent.ACTION_MOVE -> {
                if (mPrevMoveEvent == null) {
                    mPrevMoveEvent = ev
                }
                val diffY = ev.y - mPrevMoveEvent!!.y
                mPrevMoveEvent = MotionEvent.obtainNoHistory(ev)
                if (currentScrollY - diffY <= 0) {
                    // Can't scroll anymore.

                    if (mIntercepted) {
                        // Already dispatched ACTION_DOWN event to parents, so stop here.
                        return false
                    }

                    // Apps can set the interception target other than the direct parent.
                    val parent: ViewGroup
                    if (mTouchInterceptionViewGroup == null) {
                        parent = getParent() as ViewGroup
                    } else {
                        parent = mTouchInterceptionViewGroup as ViewGroup
                    }

                    // Get offset to parents. If the parent is not the direct parent,
                    // we should aggregate offsets from all of the parents.
                    var offsetX = 0f
                    var offsetY = 0f
                    var v: View? = this
                    while (v != null && v !== parent) {
                        offsetX += v!!.getLeft() - v!!.getScrollX()
                        offsetY += v!!.getTop() - v!!.getScrollY()
                        v = v!!.getParent() as View
                    }
                    val event = MotionEvent.obtainNoHistory(ev)
                    event.offsetLocation(offsetX, offsetY)

                    if (parent.onInterceptTouchEvent(event)) {
                        mIntercepted = true

                        // If the parent wants to intercept ACTION_MOVE events,
                        // we pass ACTION_DOWN event to the parent
                        // as if these touch events just have began now.
                        event.action = MotionEvent.ACTION_DOWN

                        // Return this onTouchEvent() first and set ACTION_DOWN event for parent
                        // to the queue, to keep events sequence.
                        post { parent.dispatchTouchEvent(event) }
                        return false
                    }
                    // Even when this can't be scrolled anymore,
                    // simply returning false here may cause subView's click,
                    // so delegate it to super.
                    return super.onTouchEvent(ev)
                }
            }
        }
        return super.onTouchEvent(ev)
    }

    override fun setScrollViewCallbacks(listener: ObservableScrollViewCallbacks) {
        mCallbacks = listener
    }

    override fun addScrollViewCallbacks(listener: ObservableScrollViewCallbacks) {
        if (mCallbackCollection == null) {
            mCallbackCollection = ArrayList()
        }
        mCallbackCollection!!.add(listener)
    }

    override fun removeScrollViewCallbacks(listener: ObservableScrollViewCallbacks) {
        if (mCallbackCollection != null) {
            mCallbackCollection!!.remove(listener)
        }
    }

    override fun clearScrollViewCallbacks() {
        if (mCallbackCollection != null) {
            mCallbackCollection!!.clear()
        }
    }

    override fun setTouchInterceptionViewGroup(viewGroup: ViewGroup) {
        mTouchInterceptionViewGroup = viewGroup
    }

    override fun scrollVerticallyTo(y: Int) {
        scrollTo(0, y)
    }

    private fun dispatchOnDownMotionEvent() {
        if (mCallbacks != null) {
            mCallbacks!!.onDownMotionEvent()
        }
        if (mCallbackCollection != null) {
            for (i in mCallbackCollection!!.indices) {
                val callbacks = mCallbackCollection!![i]
                callbacks.onDownMotionEvent()
            }
        }
    }

    private fun dispatchOnScrollChanged(scrollY: Int, firstScroll: Boolean, dragging: Boolean) {
        if (mCallbacks != null) {
            mCallbacks!!.onScrollChanged(scrollY, firstScroll, dragging)
        }
        if (mCallbackCollection != null) {
            for (i in mCallbackCollection!!.indices) {
                val callbacks = mCallbackCollection!![i]
                callbacks.onScrollChanged(scrollY, firstScroll, dragging)
            }
        }
    }

    private fun dispatchOnUpOrCancelMotionEvent(scrollState: ScrollState?) {
        if (mCallbacks != null) {
            mCallbacks!!.onUpOrCancelMotionEvent(scrollState!!)
        }
        if (mCallbackCollection != null) {
            for (i in mCallbackCollection!!.indices) {
                val callbacks = mCallbackCollection!![i]
                callbacks.onUpOrCancelMotionEvent(scrollState!!)
            }
        }
    }

    private fun hasNoCallbacks(): Boolean {
        return mCallbacks == null && mCallbackCollection == null
    }

    internal class SavedState : View.BaseSavedState {
        var prevScrollY: Int = 0
        var scrollY: Int = 0

        /**
         * Called by onSaveInstanceState.
         */
        constructor(superState: Parcelable) : super(superState) {}

        /**
         * Called by CREATOR.
         */
        private constructor(`in`: Parcel) : super(`in`) {
            prevScrollY = `in`.readInt()
            scrollY = `in`.readInt()
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeInt(prevScrollY)
            out.writeInt(scrollY)
        }

        companion object {

            @SuppressLint("ParcelCreator")
            val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(`in`: Parcel): SavedState {
                    return SavedState(`in`)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }
}