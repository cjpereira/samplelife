package br.com.samplelife.view

import android.graphics.Point
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import android.view.View
import android.view.ViewGroup


class LessonsLayoutManager : StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.HORIZONTAL) {

    private val mMeasuredDimension = Point()

    override fun onMeasure(
        recycler: RecyclerView.Recycler, state: RecyclerView.State,
        widthSpec: Int, heightSpec: Int
    ) {

        val widthSize = View.MeasureSpec.getSize(widthSpec) - (paddingRight + paddingLeft)

        var width = 0
        var height = 0
        var row = 0

        for (i in 0 until itemCount) {

            if (!measureScrapChild(
                    recycler, i,
                    View.MeasureSpec.makeMeasureSpec(i, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(i, View.MeasureSpec.UNSPECIFIED),
                    mMeasuredDimension
                )
            )
                continue

            if (width + mMeasuredDimension.x > widthSize || mMeasuredDimension.x > widthSize) {
                row++
                width = mMeasuredDimension.x
            } else {
                width += mMeasuredDimension.x
            }

            height += mMeasuredDimension.y
        }

        spanCount = row
        setMeasuredDimension(View.MeasureSpec.getSize(widthSpec), height)
    }

    override fun canScrollHorizontally(): Boolean {
        return false
    }

    override fun canScrollVertically(): Boolean {
        return false
    }

    private fun measureScrapChild(
        recycler: RecyclerView.Recycler,
        position: Int,
        widthSpec: Int,
        heightSpec: Int,
        measuredDimension: Point
    ): Boolean {

        var view: View? = null
        try {
            view = recycler.getViewForPosition(position)
        } catch (ex: Exception) {
            // try - catch is needed since support library version 24
        }

        if (view != null) {

            val p = view!!.getLayoutParams() as RecyclerView.LayoutParams

            val childWidthSpec = ViewGroup.getChildMeasureSpec(
                widthSpec,
                paddingLeft + paddingRight, p.width
            )
            val childHeightSpec = ViewGroup.getChildMeasureSpec(
                heightSpec,
                paddingTop + paddingBottom, p.height
            )

            view!!.measure(childWidthSpec, childHeightSpec)

            measuredDimension.set(
                view!!.getMeasuredWidth() + p.leftMargin + p.rightMargin,
                view!!.getMeasuredHeight() + p.bottomMargin + p.topMargin
            )

            recycler.recycleView(view!!)

            return true
        }

        return false
    }

}