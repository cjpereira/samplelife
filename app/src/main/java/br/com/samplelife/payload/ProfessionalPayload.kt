package br.com.samplelife.payload


import br.com.samplelife.model.Professional
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProfessionalPayload: Serializable {

    @SerializedName("nome") internal var name: String? = null
    @SerializedName("cpf") internal var cpf: String? = null
    @SerializedName("cnpj") internal var cnpj: String? = null
    @SerializedName("telefone") internal var phone: String? = null
    @SerializedName("email") internal var mail: String? = null
    @SerializedName("linkImagem") internal var linkImage: String? = null
    @SerializedName("tipoPessoaFisicaJuridica") internal var kindPerson: String? = null
    @SerializedName("especialidadeSaude") internal var specialty: GenericPayload? = null
    @SerializedName("cidade") internal var city: CityPayload? = null
    @SerializedName("planoSaude") internal var helthPlan: String? = null
    @SerializedName("endereco") internal var address: String? = null
    @SerializedName("bairro") internal var district: String? = null
    @SerializedName("estadoFederacao") internal var state: GenericPayload? = null


    fun toProfessional(): Professional {
        return Professional(name, cpf, cnpj, phone, mail, linkImage, kindPerson, specialty?.toGeneric(), city?.toCity(), helthPlan, address, district, state?.toGeneric())
    }
}