package br.com.samplelife.payload

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class MessageResponsePayload : Serializable {

    @SerializedName("error") internal var error: Boolean = false
    @SerializedName("message") internal var message: String? = null
    @SerializedName("messages") internal var messages: ArrayList<MessagePayload>? = null

}