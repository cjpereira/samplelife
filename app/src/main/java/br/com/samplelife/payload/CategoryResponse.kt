package br.com.samplelife.payload

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CategoryResponse: Serializable {

    @SerializedName("retorno")
    var categories: ArrayList<GenericPayload>? = null
}