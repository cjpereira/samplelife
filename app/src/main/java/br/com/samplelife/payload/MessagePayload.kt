package br.com.samplelife.payload

import br.com.samplelife.model.Message
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.text.SimpleDateFormat

class MessagePayload : Serializable {

    @SerializedName("message") internal var id: String? = null
    @SerializedName("fromUserId") internal var username: String? = null
    @SerializedName("toUserId") internal var online: String? = null
    @SerializedName("createdDate") internal var date: String? = null

    fun toMessage(): Message {
        val format = SimpleDateFormat("dd/MM/yyyy HH:mm")
        return Message(id, username, online, format.parse(date), false)
    }
}