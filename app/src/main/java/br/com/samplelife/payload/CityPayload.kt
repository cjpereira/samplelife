package br.com.samplelife.payload

import br.com.samplelife.model.City
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class CityPayload: Serializable {

    @SerializedName("id") internal var idCity: String? = null
    @SerializedName("nome") internal var name: String? = null
    @SerializedName("estadoFederacao") internal var state: GenericPayload? = null

    fun toCity():City{
        return City(idCity, name, state?.toGeneric())
    }
}