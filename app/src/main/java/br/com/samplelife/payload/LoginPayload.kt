package br.com.samplelife.payload

import br.com.samplelife.model.Login
import com.google.gson.annotations.SerializedName

class LoginPayload {

    @SerializedName("token") internal var token: String? = null
    @SerializedName("usuario") internal var userPayload: UserPayload? = null
    @SerializedName("mensagem") internal var mensagem: String? = null

    fun toLogin():Login{
        return Login(token, userPayload?.toUser())
    }

}