package br.com.samplelife.payload

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*


class UserResponsePayload() : Serializable {

    @SerializedName("error") internal var error: Boolean = false
    @SerializedName("message") internal var message: String? = null
    @SerializedName("users") internal var users: ArrayList<UserPayload>? = null

}
