package br.com.samplelife.payload

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class QRCodeResponse: Serializable {

    @SerializedName("retorno")
    var partners: PartnerPayload? = null
}