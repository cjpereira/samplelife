package br.com.samplelife.payload

import br.com.samplelife.model.Generic
import br.com.samplelife.utils.Utils
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GenericPayload: Serializable {

    @SerializedName("id") internal var idState: String? = null
    @SerializedName("codigo") internal var code: String? = null
    @SerializedName("nome") internal var name: String? = null
    @SerializedName("icone") internal var icon: String? = null

    fun toGeneric():Generic{
        return Generic(idState, code, name, icon, Utils.getBitmapFromURL(icon))
    }
}