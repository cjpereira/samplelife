package br.com.samplelife.payload

import br.com.samplelife.model.Articles
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class ArticlePayload: Serializable {

    @SerializedName("id") internal var id: String? = null
    @SerializedName("titulo") internal var title: String? = null
    @SerializedName("texto") internal var text: String? = null
    @SerializedName("dataPublicacao") internal var date: Date? = null
    @SerializedName("urlImagemDestaque") internal var url: String? = null
    @SerializedName("tipo") internal var type: String? = null
    @SerializedName("textoResumido") internal var shortText: String? = null
    @SerializedName("indicadorDestaque") internal var highlight: Boolean = false
    @SerializedName("indicadorAtivo") internal var active: Boolean = false
    @SerializedName("tipoVideo") internal var videoIndicator: Boolean? = false
    @SerializedName("tipoImagem") internal var imageIndicator: Boolean? = false
    @SerializedName("categoria") internal var category: ArticleCategoryPayload? = null
    @SerializedName("dataPublicacaoFormatada") internal var strDate: String? = null

    fun toArticle(): Articles {
        return Articles(id, title, text, shortText, date, url, highlight, active, strDate)
    }
}