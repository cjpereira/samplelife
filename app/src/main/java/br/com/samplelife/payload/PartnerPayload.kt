package br.com.samplelife.payload

import br.com.samplelife.model.Partner
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PartnerPayload: Serializable {

    @SerializedName("nome") internal var name: String? = null
    @SerializedName("endereco") internal var address: String? = null
    @SerializedName("enderecoComplemento") internal var complement: String? = null
    @SerializedName("bairro") internal var district: String? = null
    @SerializedName("cidade") internal var city: CityPayload? = null
    @SerializedName("estadoFederacao") internal var state: GenericPayload? = null
    @SerializedName("cep") internal var zipcode: String? = null
    @SerializedName("telefone") internal var phone: String? = null
    @SerializedName("celular") internal var cellPhone: String? = null
    @SerializedName("email") internal var mail: String? = null
    @SerializedName("linkImagem") internal var urlImage: String? = null
    @SerializedName("texto") internal var text: String? = null
    @SerializedName("textoResumido") internal var shortText: String? = null
    @SerializedName("desconto") internal var discount: DiscountPayload? = null
    @SerializedName("codigo") internal var qrCode: String? = null
    @SerializedName("site") internal var website: String? = null


    fun toParner():Partner{
        return Partner(name, address, complement, district, city?.toCity(), state?.toGeneric(), zipcode, phone, mail, urlImage, text, shortText, discount?.toDiscount(),qrCode, website, cellPhone)
    }
}