package br.com.samplelife.payload

import br.com.samplelife.model.Event
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class EventPayload: Serializable {

    @SerializedName("atividade") internal var name: String? = null
    @SerializedName("descricao") internal var description: String? = null
    @SerializedName("dataCompleta") internal var date: Date? = null
    @SerializedName("telefone") internal var phone: String? = null
    @SerializedName("categoria") internal var category: GenericPayload? = null
    @SerializedName("id") internal var id: String? = null


    fun toEvent(): Event {
        return Event(id, name, description, date, phone, category?.toGeneric()!!)
    }
}