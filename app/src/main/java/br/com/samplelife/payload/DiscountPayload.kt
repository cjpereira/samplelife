package br.com.samplelife.payload

import br.com.samplelife.model.Discount
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class DiscountPayload: Serializable {

    @SerializedName("percentual") internal var percent: Int? = null

    fun toDiscount(): Discount {
        return Discount(percent)
    }
}