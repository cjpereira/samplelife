package br.com.samplelife.payload

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ArticleCategoryPayload: Serializable {

    @SerializedName("id") internal var idCategory: String? = null
    @SerializedName("nome") internal var name: String? = null
    @SerializedName("icone") internal var icon: String? = null

}