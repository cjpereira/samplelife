package br.com.samplelife.payload

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class StateResponse: Serializable {

    @SerializedName("retorno")
    var states: ArrayList<GenericPayload>? = null
}