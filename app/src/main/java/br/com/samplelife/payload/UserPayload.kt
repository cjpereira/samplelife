package br.com.samplelife.payload

import br.com.samplelife.model.User
import com.google.gson.annotations.SerializedName
import java.util.*

class UserPayload {
    @SerializedName("id") internal var id: String? = null
    @SerializedName("codigo") internal var code: String? = null
    @SerializedName("nome") internal var name: String? = null
    @SerializedName("cpf") internal var cpf: String? = null
    @SerializedName("email") internal var mail: String? = null
    @SerializedName("dataAlteracao") internal var dateUpdate: Date? = null
    @SerializedName("indicadorAtivo") internal var active: Boolean = false
    @SerializedName("tipoPerfil") internal var profile: String? = null
    @SerializedName("idPerfil") internal var idProfile: String? = null
    @SerializedName("indicadorTrocarSenha") internal var needChangePassword: Boolean = false

    fun toUser(): User{
        return User(id, code, name, cpf, mail, dateUpdate, active, profile, needChangePassword, idProfile)
    }
}