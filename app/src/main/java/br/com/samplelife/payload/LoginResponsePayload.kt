package br.com.samplelife.payload

import com.google.gson.annotations.SerializedName

class LoginResponsePayload {

    @SerializedName("retorno")
    var loginPayload: LoginPayload? = null
}