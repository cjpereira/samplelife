package br.com.samplelife.payload

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PartnersResponse: Serializable {

    @SerializedName("retorno")
    var partners: ArrayList<PartnerPayload>? = null
}