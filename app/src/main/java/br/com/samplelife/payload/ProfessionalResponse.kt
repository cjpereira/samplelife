package br.com.samplelife.payload

import com.google.gson.annotations.SerializedName

class ProfessionalResponse {

    @SerializedName("retorno")
    var professionals: ArrayList<ProfessionalPayload>? = null
}