package br.com.samplelife.model

import java.io.Serializable
import java.util.*

class Articles: Serializable {

    var id: String? = null
    var title: String? = null
    var text: String? = null
    var shortText: String? = null
    var date: Date? = null
    var url: String? = null
    var highlight: Boolean = false
    var active: Boolean = false
    var strDate: String? = null

    constructor(
        id: String?,
        title: String?,
        text: String?,
        shortText: String?,
        date: Date?,
        url: String?,
        highlight: Boolean,
        active: Boolean,
        strDate: String?
    ) {
        this.id = id
        this.title = title
        this.text = text
        this.shortText = shortText
        this.date = date
        this.url = url
        this.highlight = highlight
        this.active = active
        this.strDate = strDate
    }
}