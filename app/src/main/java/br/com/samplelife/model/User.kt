package br.com.samplelife.model

import java.io.Serializable
import java.util.*

class User: Serializable {
    var id: String? = null
    var code: String? = null
    var name: String? = null
    var health_specialty: String? = null
    var cpf: String? = null
    var mail: String? = null
    var dateUpdate: Date? = null
    var active: Boolean = false
    var profile: String? = null
    var online: Int? = null
    var needChangePassword: Boolean = false
    var photo: String? = null
    var idProfile: String? = null

    constructor(id: String?, code: String?, name: String?, cpf: String?, mail: String?, dateUpdate: Date?, active: Boolean, profile: String?, needChangePassword: Boolean, idProfile: String?) {
        this.id = id
        this.code = code
        this.name = name
        this.cpf = cpf
        this.mail = mail
        this.dateUpdate = dateUpdate
        this.active = active
        this.profile = profile
        this.needChangePassword = needChangePassword
        this.idProfile = idProfile
    }
}