package br.com.samplelife.model

enum class ArticleType {
    IMAGE,
    VIDEOS
}