package br.com.samplelife.model

import java.io.Serializable

class Partner: Serializable {

    var name: String? = null
    var address: String? = null
    var complement: String? = null
    var district: String? = null
    var city: City? = null
    var state: Generic? = null
    var zipcode: String? = null
    var phone: String? = null
    var mail: String? = null
    var urlImage: String? = null
    var text: String? = null
    var shortText: String? = null
    var discount: Discount? = null
    var qrCode: String? = null
    var webSite: String? = null
    var cellPhone: String? = null

    constructor(
        name: String?,
        address: String?,
        complement: String?,
        district: String?,
        city: City?,
        state: Generic?,
        zipcode: String?,
        phone: String?,
        mail: String?,
        urlImage: String?,
        text: String?,
        shortText: String?,
        discount: Discount?,
        qrCode: String?,
        webSite: String?,
        cellPhone: String?

        ) {
        this.name = name
        this.address = address
        this.complement = complement
        this.district = district
        this.city = city
        this.state = state
        this.zipcode = zipcode
        this.phone = phone
        this.mail = mail
        this.urlImage = urlImage
        this.text = text
        this.shortText = shortText
        this.discount = discount
        this.qrCode = qrCode
        this.webSite = webSite
        this.cellPhone = cellPhone
    }
}