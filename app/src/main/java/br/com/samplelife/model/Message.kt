package br.com.samplelife.model

import java.util.*

class Message{

    var message: String? = null
    var fromUserId: String? = null
    var toUserId: String?= null
    var date: Date? = null
    var isHeader: Boolean = false

    constructor(message: String?, fromUserId: String?, toUserId: String?, date: Date?, isHeader: Boolean) {
        this.message = message
        this.fromUserId = fromUserId
        this.toUserId = toUserId
        this.date = date
        this.isHeader = isHeader
    }

    constructor(message: String?, isHeader: Boolean) {
        this.message = message
        this.isHeader = isHeader
    }


}