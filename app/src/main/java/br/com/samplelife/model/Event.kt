package br.com.samplelife.model

import java.io.Serializable
import java.util.*

class Event : Serializable {

    var id: String? = null
    var date: Date? = null
    var name: String? = null
    var description: String? = null
    var isHeader: Boolean = false
    var phone: String? = null
    var category: Generic? = null

    constructor(id:String?, name: String?, description: String?, date: Date?, phone: String?, category: Generic) {
        this.id = id
        this.name = name
        this.description = description
        this.date = date
        this.phone = phone
        this.category = category
    }
}