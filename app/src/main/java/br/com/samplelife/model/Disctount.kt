package br.com.samplelife.model

import java.io.Serializable


class Discount : Serializable {

    var percent: Int? = null

    constructor(percent: Int?) {
        this.percent = percent
    }
}