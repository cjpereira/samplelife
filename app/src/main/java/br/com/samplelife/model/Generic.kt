package br.com.samplelife.model

import java.io.Serializable


class Generic: Serializable {

    var genericId: String? = null
    var code: String? = null
    var name: String? = null
    var icon: String? = null
    var bitmap: ByteArray? = null

    constructor(idState: String?, code: String?, name: String?) {
        this.genericId = idState
        this.code = code
        this.name = name
    }

    constructor(idState: String?, code: String?, name: String?, icon: String?, bitmap: ByteArray?) {
        this.genericId = idState
        this.code = code
        this.name = name
        this.icon = icon
        this.bitmap = bitmap
    }
}
