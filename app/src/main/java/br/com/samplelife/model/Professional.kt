package br.com.samplelife.model

import java.io.Serializable


class Professional: Serializable {

    var name: String? = null
    var cnpj: String? = null
    var cpf: String? = null
    var phone: String? = null
    var mail: String? = null
    var linkImage: String? = null
    var kindPerson: String? = null
    var specialty: Generic? = null
    var city: City? = null
    var helthPlan: String? = ""
    var address: String? = null
    var district: String? = null
    var state: Generic? = null

    constructor(
        name: String?,
        cnpj: String?,
        cpf: String?,
        phone: String?,
        mail: String?,
        linkImage: String?,
        kindPerson: String?,
        specialty: Generic?,
        city: City?,
        helthPlan: String?,
        address: String?,
        district: String?,
        state: Generic?
    ) {
        this.name = name
        this.cnpj = cnpj
        this.cpf = cpf
        this.phone = phone
        this.mail = mail
        this.linkImage = linkImage
        this.kindPerson = kindPerson
        this.specialty = specialty
        this.city = city
        this.helthPlan= helthPlan
        this.address= address
        this.district = district
        this.state = state

    }
}