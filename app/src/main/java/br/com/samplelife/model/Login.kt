package br.com.samplelife.model

import java.io.Serializable

class Login:Serializable {

    var token: String? = null
    var user: User? = null

    constructor(token: String?, user: User?) {
        this.token = token
        this.user = user
    }
}