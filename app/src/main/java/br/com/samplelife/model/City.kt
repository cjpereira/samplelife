package br.com.samplelife.model

import java.io.Serializable

class City : Serializable {

    var idCity: String? = null
    var name: String? = null
    var state: Generic? = null

    constructor(idCity: String?, name: String?, state: Generic?) {
        this.idCity = idCity
        this.name = name
        this.state = state
    }
}