package br.com.samplelife.base

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.util.Log

abstract class BaseFragment<in V : BaseMvpView, T : BaseMvpPresenter<V>>
    : Fragment(), BaseMvpView {

    private val TAG = this::class.java.simpleName

    override fun getContext(): Context = activity!!.applicationContext
    protected abstract var mPresenter: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attachView(this as V)
        Log.d(TAG, "onCreate")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
        mPresenter.detachView()
    }

    override fun showMessage(error: String?) {
    }

    override fun showMessage(error: String?, listener: DialogInterface.OnClickListener) {
    }

    override fun success(result: Any) {
    }
}