package br.com.samplelife.base

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import br.com.samplelife.R
import br.com.samplelife.utils.Constants
import br.com.samplelife.utils.Utils

abstract class BaseActivity<in V : BaseMvpView, T : BaseMvpPresenter<V>>
    : AppCompatActivity(), BaseMvpView {

    private val TAG = this::class.java.simpleName

    override fun getContext(): Context = this
    protected abstract var mPresenter: T
    var progress: ProgressDialog? = null
    internal var telefone: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.attachView(this as V)
        Log.d(TAG, "onCreate")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")
    }

    override fun onPostResume() {
        super.onPostResume()
        Log.d(TAG, "onPostResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
        mPresenter.detachView()
    }

    override fun showMessage(error: String?) {
        hideLoading()
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage(error)
        alertDialogBuilder.setTitle("")
        alertDialogBuilder
            .setCancelable(false)
            .setNegativeButton(resources.getString(R.string.dialog_neutral_button)) { dialog, id ->

            }

        val alertD = alertDialogBuilder.create()
        alertD.show()
    }

    override fun showMessage(error: String?, listener: DialogInterface.OnClickListener) {
        hideLoading()
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage(error)
        alertDialogBuilder.setTitle("")
        alertDialogBuilder
            .setCancelable(false)
            .setNegativeButton(resources.getString(R.string.dialog_neutral_button)) { dialog, id ->
                listener.onClick(dialog, id)
            }

        val alertD = alertDialogBuilder.create()
        alertD.show()
    }

    fun loading() {

        progress = ProgressDialog(this)
        progress?.setTitle("")
        progress?.setMessage("Carregando...")
        progress?.show()

    }

    fun loading(message: String) {

        progress = ProgressDialog(this)
        progress?.setTitle("")
        progress?.setMessage(message)
        progress?.show()

    }

    fun hideLoading() {
        if (progress != null) {
            progress?.dismiss()
            progress?.cancel()
        }
    }

    override fun success(result: Any) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun hasConnectivity(showError: Boolean): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        if (netInfo == null || !cm.activeNetworkInfo.isAvailable
            || !cm.activeNetworkInfo.isConnected
        ) {
            if (showError) {
                showMessage(Constants.MSG_CONNECTION_NOT_AVAILABLE)
            }
            return false
        }
        return true
    }

    fun sendMail(email: String) {
        val mailIntent = Intent(Intent.ACTION_SEND)
        mailIntent.type = "message/rfc822"
        mailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        val chooserIntent = Intent.createChooser(mailIntent, getString(R.string.send_mail))
        startActivity(chooserIntent)

    }

    fun call(telefone: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val hasAccessCallPhone = ContextCompat.checkSelfPermission(this@BaseActivity, Manifest.permission.CALL_PHONE)
            if (hasAccessCallPhone != PackageManager.PERMISSION_GRANTED) {
                this.telefone = telefone
                // marcou pra nao mais exibir o dialog de permissao
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {

                    val alertDialogBuilder = AlertDialog.Builder(this)
                    alertDialogBuilder.setMessage(getString(R.string.error_permission_call))
                    alertDialogBuilder.setTitle("")
                    alertDialogBuilder
                        .setCancelable(false)
                        .setNegativeButton(resources.getString(R.string.dialog_nao_button)) { dialog, id ->
                        }
                        .setPositiveButton(resources.getString(R.string.dialog_sim_button)) { dialog, id ->

                            val i = Intent()
                            i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            i.addCategory(Intent.CATEGORY_DEFAULT)
                            i.data = Uri.parse("package:$packageName")
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                            this@BaseActivity.startActivity(i)
                            ActivityCompat.requestPermissions(
                                this@BaseActivity,
                                arrayOf(Manifest.permission.CALL_PHONE),
                                Constants.REQUEST_CODE_ASK_PERMISSIONS
                            )

                        }
                    val alertD = alertDialogBuilder.create()
                    alertD.show()


                } else {
                    requestPermissions(
                        arrayOf(Manifest.permission.CALL_PHONE),
                        Constants.REQUEST_CODE_ASK_PERMISSIONS
                    )
                }
            } else {
                startActivity(Utils.callTo(telefone))
            }
        } else {
            startActivity(Utils.callTo(telefone))
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            Constants.REQUEST_CODE_ASK_PERMISSIONS -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivity(Utils.callTo(telefone))
                this.telefone = null
            }
        }
    }
}
