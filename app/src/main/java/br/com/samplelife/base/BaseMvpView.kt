package br.com.samplelife.base

import android.content.Context
import android.content.DialogInterface

interface BaseMvpView {
    fun getContext(): Context
    fun showMessage(error: String?)
    fun showMessage(error: String?, listener: DialogInterface.OnClickListener)
    fun success(result: Any)
}