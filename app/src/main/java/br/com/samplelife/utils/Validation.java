package br.com.samplelife.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class Validation {

    private static final List<String> BLACKLIST_CPF = new ArrayList<String>() {{
        add("00000000000");
        add("11111111111");
        add("22222222222");
        add("33333333333");
        add("44444444444");
        add("55555555555");
        add("66666666666");
        add("77777777777");
        add("88888888888");
        add("99999999999");
        add("12345678909");
    }};

    public static boolean cpf(String candidate) {
        if(candidate==null){
            return false;
        }
        String stripped = candidate.replaceAll("[^0-9]", "");
        if (stripped.length() != 11) {
            return false;
        }

        if (BLACKLIST_CPF.contains(stripped)) {
            return false;
        }

        String numbers = stripped.substring(0, 9);
        numbers = numbers.concat(generateDigit(numbers));
        numbers = numbers.concat(generateDigit(numbers));

        return stripped.equals(numbers);
    }

    private static String generateDigit(String numbers) {
        int modulus = numbers.length() + 1;
        int multiplied = 0;
        for (int i = 0; i < numbers.length(); i++) {
            int number = Integer.parseInt(String.valueOf(numbers.charAt(i)));
            multiplied += number * (modulus - i);
        }
        int mod = multiplied % 11;
        return String.valueOf(mod < 2 ? 0 : 11 - mod);
    }

    /**
     *
     * @param date
     * @return
     */
    public static boolean date(String date) {

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        try {
            df.parse(date);
            return true;
        } catch (ParseException ex) {
            return false;
        }
    }

    /**
     *
     * @param date
     * @return
     */
    public static boolean dateGreaterThanCurrent(String date) {

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        try {

            Calendar currentDate = new GregorianCalendar();

            Calendar brithday = new GregorianCalendar();
            brithday.setTime(df.parse(date));

            if ((currentDate.getTimeInMillis() < brithday.getTimeInMillis())) {
                return true;
            }

            return false;
        } catch (ParseException ex) {
            return true;
        }
    }


}
