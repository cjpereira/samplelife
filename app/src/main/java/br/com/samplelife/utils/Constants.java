package br.com.samplelife.utils;

public class Constants {

    public static final String MSG_SYSTEM_NOT_AVAILABLE = "Sistema indisponível no momento, tente mais tarde!";
    public static final String MSG_CONNECTION_NOT_AVAILABLE = "Não conectado à Internet. Verifique sua conexão e tente novamente.";
    public static final String MSG_NOT_FOUND = "Nenhum resultado encontrado\npara o filtro selecionado.";
    public static final String MSG_QRCODE_NOT_FOUND = "Não foi possível validar o QRCode, por favor tente novamente.";

    public static final long TIME_OUT = 60;
    public static final int REQUEST_CODE_ASK_PERMISSIONS = 123;


    public static final String TITLE_VALIDATING = "Validando...";

    public static final String PARAM_MESSAGE= "mensagem";
    public static final String PARAM_MSG_RROR = "mensagem";
    public static final String PARAM_CODE = "codigo";
    public static final String PARAM_SHOW_TUTORIAL= "tutorial";
    public static final String PARAM_CPF = "cpf";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_LOGIN = "login";
    public static final String PARAM_NEWS = "news";
    public static final String PARAM_PARTNERS = "partners";
    public static final String PARAM_CATEGORIES = "categories";
    public static final String PARAM_PROFESSIONAL = "professional";
    public static final String PARAM_STATES = "states";
    public static final String PARAM_EVENT = "event";

    public static final String PARAM_PAGEID = "pageId";
    public static final String PARAM_FROMUSER = "fromUser";
    public static final String PARAM_CHAT = "CHAT";
    public static final String PARAM_AGENDA = "AGENDA";
    public static final String PARAM_INFOVIDA = "INFOVIDA";
    public static final String PARAM_ALONGAMENTO = "ALONGAMENTO";
    public static final String PARAM_BEBERAGUA = "BEBER_AGUA";

    public static final String PARAM_STATE_FILTER = "state_filter";
    public static final String PARAM_CATEGORY_FILTER = "category_filter";
    public static final String PARAM_NAME_FILTER = "name_filter";
    public static final int PARAM_REQUEST_CODE_FILTER = 9875;


    public static final String PARAM_TOUSER = "toUser";
    public static final String PARAM_QRCODE = "showqrcode";

}
