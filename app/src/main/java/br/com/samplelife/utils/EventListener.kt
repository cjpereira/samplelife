package br.com.samplelife.utils

import br.com.samplelife.model.User

interface EventListener {

    fun done(user: ArrayList<User>)
}