package br.com.samplelife.utils

import br.com.samplelife.model.User

class EventUpdateStatus {

    private var eventListener: EventListener? = null

    fun registerEvent(eventListener: EventListener) {
        this.eventListener = eventListener
    }

    fun unRegisterEvent() {
        this.eventListener = null
    }

    fun emitterDone(users: ArrayList<User>) {
        if(this.eventListener!=null){
            this.eventListener!!.done(users)
        }
    }

    companion object {
        private val instance: EventUpdateStatus = EventUpdateStatus()

        fun getInstance(): EventUpdateStatus {
            return instance
        }
    }

}